#pragma once
#include "RuFileId.h"
#include <boost/filesystem.hpp>
namespace RU
{
    using boost::filesystem::path;
    path path_from_FileId(const FileId& aId)
    {
        return path(aId);
    }
}