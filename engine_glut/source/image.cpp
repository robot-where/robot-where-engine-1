#include "file.h"
#include "ExtRuImage.h"
#include "SOIL.h"
#include "ExtRuGl.h"


static GLfloat image_points[4][5] = {
										-0.5f,0.5f,0,	0,0,
										0.5f,0.5f,0,	0,1,
										-0.5f,-0.5f,0,	1,0,
										0.5f,-0.5f,0,	1,1
									};

namespace RU
{

	

    class ResourceImage::__ImageData
    {
        friend class ResourceImage;
		FileId path;
		GLuint tex;
	public:
        __ImageData(){}
        ~__ImageData(){unload_image();} //TODO do I need to do this check or will the destroy function do it for me???
		FileId get_FileId(){return path;}
        void load_image(const FileId& aPath)
        {
            path = aPath;   
			tex = SOIL_load_OGL_texture
			(
				aPath.string().c_str(),
				SOIL_LOAD_AUTO,
				SOIL_CREATE_NEW_ID,
				SOIL_FLAG_MIPMAPS 
			);
        }
        void unload_image()
        {
            glDeleteTextures(1,&tex);
			tex = 0;
        }
        bool is_loaded(){return tex;}
        
    };

    ResourceImage::ResourceImage():data(new __ImageData())
    {
    }
    ResourceImage::ResourceImage(const FileId& aFile):data(new __ImageData())
    {
        load_image(aFile);
    }
    ResourceImage::~ResourceImage()
    {
		unload();
    }

	bool ResourceImage::is_loaded()
	{
		return data->is_loaded();
	}
	FileId ResourceImage::get_FileId()
	{
		return data->get_FileId();
	}

	void ResourceImage::unload()
	{
	}

    void ResourceImage::load_image(const FileId& aFile)
    {
        data->load_image(aFile);
    }
    void ResourceImage::render_image(int x, int y)
    {
		render_image(vec2(x,y),vec2(1,1),0);
    }
	void ResourceImage::render_image(vec2 aPos, vec2 aScale, float angle)
	{
		glPushMatrix();
		glLoadIdentity();
		glEnable(GL_TEXTURE_2D);
        glDisable(GL_LIGHTING);
		glBindTexture(GL_TEXTURE_2D,data->tex);
		GLRenderVBO render;
		render.set_mode(GL_TRIANGLE_STRIP);
		render.set_data(image_points,4,sizeof(GLfloat)*5*4);
		render.set_array_attributes(RUGL_ARRAY_VERTEX,3,GL_FLOAT,sizeof(GLfloat)*5);
		render.set_array_attributes(RUGL_ARRAY_TEXTURE,2,GL_FLOAT,sizeof(GLfloat)*5,sizeof(GLfloat)*3);
		render.draw();
		glPopMatrix();
	}

}