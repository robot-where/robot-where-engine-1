#include "main.h"
#include "RuMain.h"
RU::shared_ptr<RU::Primary> primary;
bool quit = false;
int mainWindowId;
int framePeriod = 30;
bool buttons[3] = {false,false,false};
RU::shared_ptr<shader_prog> prog;

namespace RU
{

    RU::vec2 EngineRunner::get_screen_dimensions()
    {
        return RU::vec2(glutGet(GLUT_WINDOW_WIDTH),glutGet(GLUT_WINDOW_HEIGHT));
    }
    void EngineRunner::keyPressed(unsigned char key, int x, int y)
    {
        primary->key_pressed(RU::KeyCode(key));
    }
    void EngineRunner::keyReleased(unsigned char key, int x, int y)
    {
        primary->key_released(RU::KeyCode(key));
        //exit(0);
    }
    void EngineRunner::specialKeyPressed(int key, int x, int y)
    {
        primary->key_pressed(RU::KeyCode(key));
    }
    void EngineRunner::specialKeyReleased(int key, int x, int y)
    {
        primary->key_released(RU::KeyCode(key));
    }

    void EngineRunner::mousePassiveMotion(int x, int y)
    {

        RU::MousePosition mp;
        mp.mScreenDim = get_screen_dimensions();
        mp.mAbsolute = RU::vec2(x,y);

        for(int i =	0; i < 3; i++)
            if(buttons[i])
                primary->mouse_position_update(i,mp);
        primary->mouse_position_update(-1,mp);
    }
    void EngineRunner::mouseAction(int button, int state, int x, int y)
    {
        RU::MousePosition mp;
        mp.mScreenDim = get_screen_dimensions();
        mp.mAbsolute = RU::vec2(x,y);

        buttons[button] = state;
        if(state)
            primary->mouse_pressed(button,mp);
        else 
            primary->mouse_released(button,mp);
    }	
    void EngineRunner::drawScene()
    {
        glClearColor(0.5f,0.5f,0.5f,1);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        //drawAFrigginTriangle();
        //glutSolidTeapot(0.5f);
        glMatrixMode(GL_PROJECTION);	
        glLoadIdentity();
        //gluPerspective(60, 3/4.0f, 0, 100);
        glMatrixMode(GL_MODELVIEW);	
        glLoadIdentity();
        //drawAFrigginTriangle();
        primary->update(framePeriod/1000.0f);
        glFlush();
        glutSwapBuffers();
        if(primary->does_request_quit())
            quit = true;
    }

    void EngineRunner::onResize(GLsizei width, GLsizei height)
    {
        
    }

    void EngineRunner::idleFunc()
    {
        //glutPostRedisplay();
        if(quit)
        {
            primary->destroy();
            glutDestroyWindow(mainWindowId);
            //glutLeaveMainLoop(); freeglut only
            exit(0);
        }
    }


    void EngineRunner::initGl(GLsizei width, GLsizei height)
    {
        glDepthFunc(GL_LESS);
        glEnable(GL_DEPTH_TEST);
        glShadeModel(GL_SMOOTH);
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glClearColor(0.0f,0.0f,0.0f,0.0f);
        glClearDepth(1.0);
        onResize(width,height);
    }

    void EngineRunner::timerCallback (int value)
    {
        glutSetWindow(mainWindowId);
        glutTimerFunc(framePeriod, timerCallback, value);
        glutPostRedisplay();
    }

	shared_ptr<Primary> create_engine(const EngineParameters& aParam)
	{
		//prog = shared_ptr<shader_prog>(new shader_prog(vertex_shader, color_shader)); does not work don't know why
        //see http://stackoverflow.com/questions/2795044/easy-framework-for-opengl-shaders-in-c-c
		std::cout << "initialize glut" << std::endl;
		int argc = 0;
		char** argv = NULL;
		//glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE); freeglut only
		glutInit(&argc, argv);  
		glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_ALPHA | GLUT_DEPTH);  
		glutInitWindowSize(640, 480);  
		glutInitWindowPosition(0, 0);  
		mainWindowId = glutCreateWindow("gamo");

		initialize_gl_extras_basic();
       
		glutKeyboardFunc(EngineRunner::keyPressed);
		glutKeyboardUpFunc(EngineRunner::keyReleased);
		glutSpecialFunc(EngineRunner::specialKeyPressed);
		glutSpecialUpFunc(&EngineRunner::specialKeyReleased);
		glutMouseFunc(&EngineRunner::mouseAction);
		glutPassiveMotionFunc(&EngineRunner::mousePassiveMotion);
		glutReshapeFunc(&EngineRunner::onResize);
		glutDisplayFunc(&EngineRunner::drawScene);  
		glutIdleFunc(&EngineRunner::idleFunc);
		glutTimerFunc (framePeriod, &EngineRunner::timerCallback, 0); //NOT WORKING
        primary = shared_ptr<Primary>(new Primary(aParam));
		primary->set_screen_size(RU::vec2(640,480));
		primary->initialize();
		return primary;
	}
	OperationStatus run_engine(shared_ptr<Primary> aGame)
	{
		
		glutMainLoop();
		return STATUS_OK;
	}
}