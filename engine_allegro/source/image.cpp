#include "file.h"
#include "ExtRuImage.h"
#include "allegro5/allegro.h"
#include "allegro5/allegro_image.h"
#include "ExtRuImage.h"
#include "RuMathTypes.h"
namespace RU
{
    class ResourceImage::__ImageData
    {
        friend class ResourceImage;
		FileId path;
        ALLEGRO_BITMAP* image;
	public:
        __ImageData():image(NULL){}
        ~__ImageData(){unload_image();} //TODO do I need to do this check or will the destroy function do it for me???
		FileId get_FileId(){return path;}
        void load_image(const FileId& aPath)
        {
			ALLEGRO_PATH *temp = al_get_standard_path(ALLEGRO_RESOURCES_PATH);
			const char * s = al_path_cstr(temp,ALLEGRO_NATIVE_PATH_SEP);
			al_change_directory(s);
			al_destroy_path(temp);
			path = aPath;
            image = al_load_bitmap(aPath.c_str());
        }
        void unload_image()
        {
            if(image) al_destroy_bitmap(image);
            image = NULL;
        }
        bool is_loaded(){return image!=NULL;}
        
    };

    ResourceImage::ResourceImage():data(new __ImageData())
    {
    }
    ResourceImage::ResourceImage(const FileId& aFile):data(new __ImageData())
    {
        load_image(aFile);
    }
    ResourceImage::~ResourceImage()
    {
    }

	bool ResourceImage::is_loaded()
	{
		return data->is_loaded();
	}
	FileId ResourceImage::get_FileId()
	{
		return data->get_FileId();
	}

	void ResourceImage::unload()
	{
		al_destroy_bitmap(data->image);
	}

    void ResourceImage::load_image(const FileId& aFile)
    {
        data->load_image(aFile);
    }
    void ResourceImage::render_image(int x, int y)
    {
		render_image(vec2(x,y),vec2(1,1),0);
    }
	void ResourceImage::render_image(vec2 aPos, vec2 aScale, float angle)
	{
		al_draw_scaled_rotated_bitmap(data->image,0,0,aPos.x,aPos.y,aScale.x,aScale.y,angle,0);
	}

}