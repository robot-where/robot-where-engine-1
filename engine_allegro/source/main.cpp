//this implementation uses allegro
#include "RuMain.h"
#include "RuException.h"
#include "RuKeys.h"
#include "allegro5/allegro.h"
#include "allegro5/allegro_image.h"
#include "allegro5/allegro_primitives.h"
#include "allegro5/allegro_audio.h"
#include "allegro5/allegro_acodec.h"
#include "allegro5/allegro_opengl.h"
#include <boost/assign/std/vector.hpp>

namespace RU
{
	
    struct AllegroState
    {
        ALLEGRO_DISPLAY* display;
        ALLEGRO_EVENT_QUEUE* event_queue;
        ALLEGRO_TIMER *draw_timer;
        AllegroState():display(NULL),event_queue(NULL),draw_timer(NULL){}
        void destroy()
        {
			al_destroy_event_queue(event_queue);
            al_destroy_timer(draw_timer);
            al_destroy_display(display);
            
        }
    };


	AllegroState state; 
    std::vector<KeyCode> keyMap;
    void generate_key_map()
    {
        keyMap.resize(228);
        for(int i = 0; i < 26; i++)
            keyMap[i+1] = KeyCode('a' + i);
        for(int i = 0; i < 10; i++)
            keyMap[i+27] = KeyCode('0' + i);
        
        keyMap[ALLEGRO_KEY_ESCAPE] = KEY_BACKSPACE;
        keyMap[ALLEGRO_KEY_SPACE] = KEY_SPACE;
        //keyMap[ALLEGRO_KEY_RETURN] = KEY_RETURN;
        
    }

    //template<typename T> shared_ptr<Primary> create_engine_allegro(const EngineParameters& aParam)
	shared_ptr<Primary> create_engine(const EngineParameters& aParam)
    {
		
        generate_key_map();
		if(!al_init()) throw SimpleException("failed to initialize allegro");
        
        al_init_image_addon();
        al_init_primitives_addon();
        if(!al_install_audio()) throw SimpleException("failed to create allegro audio");
        if(!al_init_acodec_addon()) throw SimpleException("failed to create allegro audio codecs");
		ALLEGRO_DISPLAY_MODE disp_data;
        al_get_display_mode(al_get_num_display_modes() - 1, &disp_data);
        //al_set_new_display_flags(ALLEGRO_FULLSCREEN);
        //state.display = al_create_display(disp_data.width, disp_data.height);
        state.display = al_create_display(1024,768);
        if(!state.display) throw SimpleException("failed to create allegro display");
        al_set_current_opengl_context(state.display);
        state.event_queue = al_create_event_queue();
        if(!state.event_queue) throw SimpleException("failed to create allegro event queue");
        state.draw_timer = al_create_timer(1.0 / 60.0);
        if(!al_install_mouse()) throw SimpleException("failed to create allegro mouse"); 
        if(!al_install_keyboard()) throw SimpleException("failed to create allegro keyboard"); 
        
        al_register_event_source(state.event_queue, al_get_display_event_source(state.display));
        al_register_event_source(state.event_queue, al_get_timer_event_source(state.draw_timer));
        al_register_event_source(state.event_queue, al_get_keyboard_event_source());
        al_register_event_source(state.event_queue, al_get_mouse_event_source());
        al_start_timer(state.draw_timer);




        shared_ptr<Primary> p(new Primary(aParam));
		p->initialize();
		return p;
    }
    //template<typename T> OperationStatus run_engine_allegro(shared_ptr<Primary> aGame)
	OperationStatus run_engine(shared_ptr<Primary> aGame)
    {      
        bool redraw = false;
        while(1)
        {
            double last_update = al_get_time();
            ALLEGRO_EVENT ev;
            ALLEGRO_TIMEOUT timeout;
            al_init_timeout(&timeout, 0.06);
            bool get_event = al_wait_for_event_until(state.event_queue, &ev, &timeout);
            //if(get_event && ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) break;
            if(ev.type == ALLEGRO_EVENT_KEY_DOWN)
            {
                aGame->key_pressed(RU::KeyCode(keyMap[ev.keyboard.keycode]));
            }
            else if(ev.type == ALLEGRO_EVENT_MOUSE_AXES ||
                    ev.type == ALLEGRO_EVENT_MOUSE_ENTER_DISPLAY)
            {
                
            }
            else if(ev.type == ALLEGRO_EVENT_TIMER)
                redraw = true;
            if(redraw && al_is_event_queue_empty(state.event_queue))
            {
                al_clear_to_color(al_map_rgb(0,0,0));
                aGame->update(al_get_time()-last_update);
              
                al_flip_display();
                redraw = false;
                if(aGame->does_request_quit())
				{
					aGame->destroy();
					state.destroy();
                    return STATUS_OK;
				}
            }
        }
		state.destroy();
        return STATUS_OK;
    }

}