#include "RuTypesSerialization.h"
#include "RuPhysicsBase.h"
#include "RuTypes.h"
#include "RuComponentGLVectorField.h"
#include "ExtRu.h"
#include "RuComponentRenderImage.h"
#include "RuComponentCollidable.h"
#include "RuEntityBase.h"
#include "RuMain.h"
#include "algebra.h"
#include "RuMathTypesStream.h"
#include <iostream>
#include <cmath>
#include <fstream>
#include <glm/glm.hpp>
#include <glm/gtx/random.hpp>
#include <boost/foreach.hpp>

namespace RU
{
namespace MATH
{

	template<typename T>
	class Array2d
	{
		unsigned M,N;
		T* data;
		unsigned stride;
		Array2d(unsigned aM, unsigned aN, T* aData, unsigned aStride = 1):M(aM),N(aN),data(aData),stride(aStride)
		{
		}
		T& operator()(int x, int y){return data[x*N + y];}

	};
	//contains interpolation functions as wel...
	template<typename T>
	struct MacGrid
	{
		//MxN grid, i.e. (M-1)x(N-1) cells
		//by convention, x is left and right of the BOX, and y is top and bottom of the BOX
		unsigned M,N;
		std::vector<T> x;
		std::vector<T> y;
		std::vector<T> p;
		//shared_ptr<Array2d> arr;
		T interpolate(T& v1, T& v2, T& x1, T& x2, T& x)
		{
			T lambda = (x2-x)/(x2-x1);
			return v1*(lambda)+v2*(1-lambda);
		}
		unsigned nearest_x(const T& x)
		{
			//TODO
			return 0;
		}
		unsigned nearest_y(const T& y)
		{
			//TODO
			return 0;
		}
		unsigned left_x(const T& x)
		{
			//returns nearest to the left
			return floor(x*N); //TODO check me
		}
		unsigned bot_y(const T& y)
		{
			//returns nearest to the bottom
			return floor(y*M); //TODO check me
		}
		//do any transformations here
		glm::detail::tvec2<T> transform_position(const glm::detail::tvec2<T>& pos)
		{
			return pos;
		}
		void get_lambdas(const glm::detail::tvec2<T>& pos, glm::detail::tvec2<T>& outLambda, glm::detail::tvec2<unsigned>& outIndex)
		{
			unsigned xtier = nearest_y(pos.y);
			unsigned ytier = nearest_x(pos.x);
			outIndex.x = ytier*N + left_x(pos.x); //TODO check me
			outIndex.y = xtier*M + bot_y(pos.y); //TODO check me
			
			//TODO need to normalize here, need to decide on grid size...
			//lambda is distance from left to position
			outLambda.x = outIndex.x-pos.x;
			outLambda.y = outIndex.y-pos.y;
		}
		void zero_out_grid()
		{
			BOOST_FOREACH(T xval, x)
			{
				xval = 0;
			}
			BOOST_FOREACH(T yval, y)
			{
				yval = 0;
			}
		}
	public:
		MacGrid(){}
		MacGrid(unsigned aM, unsigned aN):M(aM),N(aN)
		{
			p.resize((aM-1)*(aN-1));
			x.resize((aM)*(aN));
			y.resize((aM)*(aN));
		}
		T get_x(unsigned xindex, unsigned yindex){return x[xindex+yindex*N];}
		void set_grid_from_points(const std::vector<glm::detail::tvec2<T> >& positions, const std::vector<glm::detail::tvec2<T> >& values)
		{
			zero_out_grid();
			std::vector<T> countx,county;
			countx.resize((M)*(N),0);
			county.resize((M)*(N),0);
			for(int i = 0; i < positions.size(); i++)
			{
				//TODO this is wrong... need to normalize each row by something... I can't remember what...
				glm::detail::tvec2<unsigned> index;
				glm::detail::tvec2<T> lambda;
				get_lambdas(positions[i],lambda,index);
				countx[index.x] += (1-lambda.x);
				x[index.x] += (1-lambda.x)*values[i].x;
				if(lambda.x != 0)
				{
					x[index.x+1] += lambda.x*values[i].x;
					countx[index.x+1] += (lambda.x);;
				}
				county[index.y] += (1-lambda.y);
				y[index.y] += (1-lambda.y)*values[i].y;
				if(lambda.y != 0)
				{
					y[index.y+1] += lambda.y*values[i].y;
					county[index.y+1] += (lambda.y);
				}
			}
			for(int i = 0; i < x.size(); i++)
			{
				if(countx[i]>0)
					x[i]/=countx[i];
				if(county[i]>0)
					y[i]/=county[i];
			}
		}
		void set_points_from_grid(const std::vector<glm::detail::tvec2<T> >& positions, std::vector<glm::detail::tvec2<T> >& values)
		{
			for(int i = 0; i < positions.size(); i++)
			{
				glm::detail::tvec2<T> index;
				glm::detail::tvec2<T> lambda;
				get_lambdas(positions[i],lambda,index);
				values[i].x = (1-lambda.x)*x[index.x];
				if(lambda.x != 0)
					values[i].x += (lambda.x)*x[index.x+1];
				values[i].y = (1-lambda.y)*y[index.y];
				if(lambda.y != 0)
					values[i].y += (lambda.y)*y[index.y+1];
			}
			//no fancy normalization needed in this case
		}
		template<class Archive>
		void serialize(Archive & ar, const unsigned int version)
		{
			ar & BOOST_SERIALIZATION_NVP(M) & BOOST_SERIALIZATION_NVP(N);
			ar & BOOST_SERIALIZATION_NVP(x) & BOOST_SERIALIZATION_NVP(y) & BOOST_SERIALIZATION_NVP(p);
		}
	};
}
}
namespace RU
{

	class ComponentInterpolationGrid : public ComponentRenderableBase
	{
		friend class boost::serialization::access;
		shared_ptr<GLVectorField> mField;
		shared_ptr<GLVectorsInSpace> mPoints;
		shared_ptr<MATH::MacGrid<float> > mGrid;
	public:
		static std::string name(){return "ComponentInterpolationGrid";}
		void derived_setup()
		{
			ComponentRenderableBase::derived_setup();
			auto_connect_always_render(this);
		}
		void on_create()
		{
			int num_points = 50;
			int grid_x_size = 10;
			int grid_y_size = 10;
			//this will be 640x480 centered at the middle fo the screen
			mField = shared_ptr<GLVectorField>(new GLVectorField(grid_x_size,grid_y_size,640,480));
			mPoints = shared_ptr<GLVectorsInSpace>(new GLVectorsInSpace(num_points));
			mGrid = shared_ptr<MATH::MacGrid<float> >(new MATH::MacGrid<float>(grid_x_size,grid_y_size));

			//all our points are from 0 to 1 (to make our grid happy, we scale during drawing instead..)
			std::vector<vec2> start; start.resize(num_points);
			std::vector<vec2> end; end.resize(num_points);
			for(int i = 0; i < num_points; i++)
			{
				start[i] = glm::compRand2<float>(0,1);
				end[i] = glm::compRand2<float>(-1/(float)grid_x_size,1/(float)grid_x_size);
			}
			for(unsigned i = 0; i < num_points; i++)
				mPoints->set_vector(i,vec3(start[i].x,start[i].y,0),vec3(end[i].x,end[i].y,0));
			mGrid->set_grid_from_points(start,end);

			
			for(int i = 0; i < mGrid->M; i++)
				for(int j = 0; j < mGrid->N; j++)
					mField->set_vector(i,j,vec3(mGrid->get_x(i,j),0,0));
					
		}
		void render()
		{
		}
		void always_render(const ScreenSpatialPosition& position)
		{ 
			//update stuff here for now

			//rendering stuff
			mat4x4 mat = position.get_spatial_position_with_screen_matrix();
			mField->render(mat);
			mat = glm::translate<float>(vec3(-1,-1,0))*glm::scale<float>(640,480,1)*mat;
			mPoints->render(mat);
		}
		template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
			ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(ComponentRenderableBase);
			ar & BOOST_SERIALIZATION_NVP(mField);
			ar & BOOST_SERIALIZATION_NVP(mPoints);
			ar & BOOST_SERIALIZATION_NVP(mGrid);
        }
	};
}

BOOST_CLASS_EXPORT(RU::ComponentInterpolationGrid);
namespace RU
{
	int __main_helper()
	{
        std::string filename("serialization_test");
		std::cout << "Math Interpolation" << std::endl;
		shared_ptr<Primary> game = create_engine(EngineParameters());
     
		//interpolation grid entity
		//weak_ptr<EntityBase> e = game->get_world_entity_container().create_entity();
		//weak_ptr<ComponentInterpolationGrid> c = e.lock()->get_component_manager().add_component<ComponentInterpolationGrid>();
        //e.lock()->get_component_manager().add_component<ComponentJoke>();
        
		//physics test entity
		weak_ptr<EntityBase> ph1 = game->get_world_entity_container().create_entity();
		ph1.lock()->get_component_manager().add_component<ComponentRigidBody>();
		//ph1.lock()->get_component_manager().add_component<ComponentColliderSphere>();
		//weak_ptr<EntityBase> ph2 = game->get_world_entity_container().create_entity();
		//ph2.lock()->get_component_manager().add_component<ComponentRigidBody>();

		{
			std::ofstream ofs(filename.c_str());
			boost::archive::xml_oarchive oa(ofs,  boost::archive::no_header );
			std::cout << "writing to file " << filename << std::endl;
			oa << boost::serialization::make_nvp("gameworld",game->get_world_entity_container());
		}
         
		game->get_world_entity_container().destroy_all();
        game->get_world_entity_container().set_this_as_deserializing();
		{
			std::ifstream ifs(filename.c_str());
			boost::archive::xml_iarchive ia(ifs,  boost::archive::no_header );
			std::cout << "reading from file " << filename << std::endl;
			ia >> boost::serialization::make_nvp("gameworld",game->get_world_entity_container());
            std::cout << "deserialization succesfull" << std::endl;
			game->get_world_entity_container().set_primary(game);
            std::cout << "primary set" << std::endl;
			game->get_world_entity_container().setup_after_deserialization();
            std::cout << "post deserialization done" << std::endl;
		}        
        std::cout << "going to run the game" << std::endl;
		RU::run_engine(game);
		return 0;
	}
}

int main(int argc, char *argv[])
{
	return RU::__main_helper();
}