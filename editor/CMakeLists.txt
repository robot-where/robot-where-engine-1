cmake_minimum_required (VERSION 2.6) 
project (RU_EDITOR)


INCLUDE_DIRECTORIES(${RU_ENGINE_COMMON_INCLUDE})
INCLUDE_DIRECTORIES(${RU_ENGINE_ABS_PATH}/include)
LINK_DIRECTORIES(${RU_ENGINE_COMMON_LINK})


add_library(RU_EDITOR include/RuEditor.h source/RuEditor.cpp)

target_link_libraries(RU_EDITOR ${RU_ENGINE_COMMON_LIBRARIES})