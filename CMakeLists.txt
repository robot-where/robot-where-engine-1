cmake_minimum_required (VERSION 2.6) 
project (RU_TOTAL)

set(CMAKE_BUILD_TYPE Debug)
#set(CMAKE_BUILD_TYPE Release)


#ARRG STATIC LINKING!!!!
SET(LINK_SEARCH_END_STATIC 1)
SET(BUILD_SHARED_LIBS OFF)
#SET(CMAKE_EXE_LINKER_FLAGS ${CMAKE_EXE_LINKER_FLAGS} "-static")



SET (RU_ENGINE_ABS_PATH "${RU_TOTAL_SOURCE_DIR}/engine")
SET (RU_ENGINE_EXTRA_ABS_PATH "${RU_TOTAL_SOURCE_DIR}/engine_extras")
SET (RW_ABS_PATH  "${RU_TOTAL_SOURCE_DIR}/game")

find_package(OpenGL)
if (OPENGL_FOUND)
	MESSAGE("OpenGL Correctly Found")
	include_directories(${OPENGL_INCLUDE_DIR})
else (OPENGL_FOUND)
	MESSAGE("OpenGL environment missing")
endif (OPENGL_FOUND)

#set(Boost_USE_STATIC_LIBS ON)
#find_package(Boost COMPONENTS signals REQUIRED)

#set these as appropriate
IF(WIN32)
SET (CMAKE_EXE_LINKER_FLAGS "-static")
SET (BOOST_ROOT_DIR  "C:\\Program\ Files\ (x86)\\boost\\boost_1_51")
SET (BOOST_LINK_DIR ${BOOST_ROOT_DIR}\\lib)
SET (GLM_ROOT_DIR "C:\\market\\kitchen\\spices\\glm-0.9.3.3")
SET (BOX2D_INCLUDE_DIR "C:\\market\\kitchen\\spices\\Box2D_v2.2.1")
SET (BOX2D_BUILD_DIR 
"C:\\market\\kitchen\\spices\\Box2D_v2.2.1\\Build\\vs2010\\bin\\Debug"
"C:\\market\\kitchen\\spices\\Box2D_v2.2.1\\Build\\vs2010\\bin\\Release")
SET (ALLEGRO_INCLUDE_DIR "C:\\market\\kitchen\\spices\\allegro-5.1.2\\build_msvc\\include")
SET (ALLEGRO_ADDONS_INCLUDE_DIR
"C:\\market\\kitchen\\spices\\allegro-5.1.2\\addons\\acodec"
"C:\\market\\kitchen\\spices\\allegro-5.1.2\\addons\\audio"
"C:\\market\\kitchen\\spices\\allegro-5.1.2\\addons\\color"
"C:\\market\\kitchen\\spices\\allegro-5.1.2\\addons\\font"
"C:\\market\\kitchen\\spices\\allegro-5.1.2\\addons\\image"
"C:\\market\\kitchen\\spices\\allegro-5.1.2\\addons\\primitives")
SET (ALLEGRO_BUILD_DIR "C:\\market\\kitchen\\spices\\allegro-5.1.2\\build_msvc\\lib\\RelWithDebInfo")
SET (GLEW_INCLUDE_DIR "C:\\market\\kitchen\\spices\\glew-1.8.0\\include")
SET (GLEW_LINK_DIR "C:\\market\\kitchen\\spices\\glew-1.8.0\\lib")
SET(RU_ENGINE_COMMON_LIBRARIES Box2D glew32 libboost_serialization-vc100-mt-gd-1_51)

ELSEIF(APPLE)
SET(BOOST_ROOT_DIR  "/Users/user/kitchen/spices/boost_1_49_0")
SET(BOOST_LINK_DIR "/Users/user/kitchen/spices/boost_1_49_0/stage/lib")
SET(GLM_ROOT_DIR "/Users/user/kitchen/spices/glm-0.9.3.3")
SET(RU_ENGINE_COMMON_LIBRARIES Box2D boost_signals boost_system boost_serialization  boost_filesystem boost_timer boost_chrono ${OPENGL_LIBRARIES})

ELSEIF(UNIX)
SET(RU_ENGINE_COMMON_LIBRARIES Box2D GLEW boost_signals boost_system boost_serialization boost_filesystem boost_chrono boost_timer ${OPENGL_LIBRARIES})

ENDIF()

SET(LOCALLIBS_ROOT_DIR "${RU_TOTAL_SOURCE_DIR}/libraries/build")

SET(RU_ENGINE_COMMON_INCLUDE ${LOCALLIBS_ROOT_DIR}/include ${BOOST_ROOT_DIR} ${GLM_ROOT_DIR} ${RU_ENGINE_ABS_PATH}/include ${RU_ENGINE_EXTRA_ABS_PATH}/include ${BOX2D_INCLUDE_DIR} ${ALLEGRO_INCLUDE_DIR} ${ALLEGRO_ADDONS_INCLUDE_DIR} ${GLEW_INCLUDE_DIR})
SET(RU_ENGINE_COMMON_LINK ${LOCALLIBS_ROOT_DIR}/lib ${BOX2D_BUILD_DIR} ${ALLEGRO_BUILD_DIR} ${BOOST_LINK_DIR} ${GLEW_LINK_DIR} )

MESSAGE(STATUS "RU_TOTAL_SOURCE_DIR is ${RU_TOTAL_SOURCE_DIR}")
#add_subdirectory(engine_allegro)
add_subdirectory(engine_glut)
add_subdirectory(game)
add_subdirectory(engine)
add_subdirectory(engine_extras)
#add_subdirectory(editor)
#add_subdirectory(math_research/mac_grid_interpolation)
add_subdirectory(experiments/boost_serialize)
add_subdirectory(experiments/boost_signals)

