#pragma once
#include "RuGLBasic.h"
#include "ExtRuGL.h"
namespace RU
{
	class GLVectorField
	{
		friend class boost::serialization::access;
		unsigned M,N; //M is num rows, N is num cols
		float w,h;
		std::vector<vec3> mDraw;
		GLRenderVBO mRenderArray;
	public:
		GLVectorField(unsigned aM, unsigned aN, float aW, float aH):M(aM),N(aN),w(aW),h(aH)
		{
			mDraw.resize(M*N*2);
			//TODO position initial of all vectors
			for(int i = 0; i < M*N; i++)
			{
				int row = i/N;
				int col = i%N;
				mDraw[2*i] = vec3(col*w/(float)(M-1),row*h/(float)(N-1),0);
			}
			mRenderArray.set_mode(GL_LINES);
		}
		//draws with center of the graph at 0,0 with supplied width and height
		void render(const mat4x4& aMat)
		{
			mRenderArray.set_data(&mDraw[0],mDraw.size(),sizeof(vec3)*mDraw.size());
			mRenderArray.reset_array_attributes();
			mRenderArray.set_array_attributes(RUGL_ARRAY_VERTEX,3);
			glPushMatrix();
			glLoadMatrixf((GLfloat*)&aMat);
			glTranslatef(-w/2,-h/2,0);
			mRenderArray.draw();
			glPopMatrix();
		}
		void set_vector(unsigned x, unsigned y, vec3 val)
		{
			mDraw[2*(x*N+y)+1] = val;
		}
	private:
		GLVectorField(){}
		template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
			ar & BOOST_SERIALIZATION_NVP(w) & BOOST_SERIALIZATION_NVP(h);
			ar & BOOST_SERIALIZATION_NVP(M) & BOOST_SERIALIZATION_NVP(N);
			ar & BOOST_SERIALIZATION_NVP(mDraw);
			ar & BOOST_SERIALIZATION_NVP(mRenderArray);
        }
	};

	class GLVectorsInSpace
	{
		friend class boost::serialization::access;
		unsigned N;
		std::vector<vec3> mDraw;
		GLRenderVBO mRenderArray;
	public:
		GLVectorsInSpace(unsigned aN):N(aN)
		{
			mDraw.resize(N*2);
			mRenderArray.set_mode(GL_LINES);
		}
		void set_vector(unsigned aIndex, vec3 start, vec3 end)
		{
			mDraw[aIndex*2] = start;
			mDraw[aIndex*2+1] = start+end;
		}
		void get_vector(unsigned aIndex, vec3& start, vec3& end)
		{
			start = mDraw[aIndex*2];
			end = mDraw[aIndex*2+1]-start;
		}
		void render(const mat4x4& aMat)
		{
			
			mRenderArray.set_data(&mDraw[0],mDraw.size(),sizeof(vec3)*mDraw.size());
			mRenderArray.reset_array_attributes();
			mRenderArray.set_array_attributes(RUGL_ARRAY_VERTEX,3);
			glPushMatrix();
			glLoadMatrixf((GLfloat*)&aMat);
			mRenderArray.draw();
			glPopMatrix();
		}
	private:
		GLVectorsInSpace(){}
		template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
			ar & BOOST_SERIALIZATION_NVP(N);
			ar & BOOST_SERIALIZATION_NVP(mDraw);
			ar & BOOST_SERIALIZATION_NVP(mRenderArray);
        }
	};
}