#pragma once
#include "RuTypes.h"
#include "RuMathTypes.h"
#include "RuMouse.h" 
#include "RuComponentRenderableBase.h"
namespace RU
{
	class UIDrawingBatch
	{
	};

	class UIDrawingStencil
	{
	}; 

	class UIElementBase
	{
		vec2 mMinDim;
	public:
		UIElementBase():mMinDim(0,0){}
		//----------
		//related to element rect
		//----------
		virtual rect get_element_rect(){return rect(0,0,0,0);}
		bool is_point_in_element_rect(const vec2& point){return get_element_rect().contains_point(point);}
		virtual bool is_point_in_element(const vec2& point){return is_point_in_element_rect(point);}


		//----------
		//related to sizeability
		//----------
		virtual bool is_sizeable(){return false;}
		void set_min_pixel_dimensions(const vec2& aDim){mMinDim=aDim;}
		vec2 get_min_pixel_dimensions(){return mMinDim;}
		void set_preferred_pixel_dimensions(const vec2& aDim){}//TODO

	};

	class UIElementContainer
	{
		typedef std::list<shared_ptr<UIElementBase> > ElementContainerType;
		ElementContainerType mElements;
	public:
		void add_element(shared_ptr<UIElementBase> aElement){mElements.push_back(aElement);}
		const ElementContainerType& get_elements(){return mElements;}
	};

	class UIContainerElementBase : public UIElementBase
	{
		UIElementContainer mContainer;
	public: 
	};
}