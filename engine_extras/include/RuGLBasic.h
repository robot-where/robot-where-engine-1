#pragma once

#ifdef _WIN32
#include <windows.h>
#define GL_GLEXT_PROTOTYPES
#else
#define __stdcall 
#endif

#ifdef __APPLE__
#include "OpenGl/gl.h"
#include <GLUT/glut.h> 
#else
#define USE_GLEW
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glut.h>
#endif


#include <iostream>


namespace RU
{
	//NOTE, this must be inlined otherwise there il a duplicate symbol error due to the way we link things...
	//should be fixable but I don't know what to call this sort of linker problem...
    inline void initialize_gl_extras_basic()
    {
#ifdef USE_GLEW
		std::cout << "initializing GLEW" << std::endl;
		GLenum err = glewInit();
		if (GLEW_OK != err)
			std::cout << "ERROR glew initialization failed" << std::endl;//TODO throw
#endif
    }
}