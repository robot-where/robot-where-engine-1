#include "RuGLBasic.h"
#include "ExtRuGL.h"
#include <string.h> //for memcpy



namespace RU
{

	GLenum GLArrayAttributeClientStateMap[4]=
	{
		GL_VERTEX_ARRAY,
		GL_COLOR_ARRAY,
		GL_NORMAL_ARRAY,
        GL_TEXTURE_COORD_ARRAY  
	};
	//GLArrayAttributeSignature GLArrayAttributeMap[RUGL_ARRAY_USER_START] = 
	void (__stdcall *GLArrayAttributeMap[]) (GLint,GLenum,GLsizei,const GLvoid*)=
	{
		glVertexPointer,
		glColorPointer,
		NULL,
		glTexCoordPointer
	};



	void GLArrayAttributeState::reset_array_attributes()
	{
		for(int i = 0; i < RUGL_ARRAY_USER_START; i++)
			mArrays[i].use = false;
	}
	void GLArrayAttributeState::set_array_attributes(GLArrayAttributes mAttrib, GLint size, GLint type, GLsizei stride, GLsizei offset) //offset in bytes
	{
		mArrays[(unsigned)mAttrib].use = true;
		mArrays[(unsigned)mAttrib].size = size;
		mArrays[(unsigned)mAttrib].type = type;
		mArrays[(unsigned)mAttrib].stride = stride;
		mArrays[(unsigned)mAttrib].offset = offset;
	}
	void GLArrayAttributeState::set_attribute_pointer_interleaved(const void* pointer)
	{
		for(int i = 0; i < RUGL_ARRAY_USER_START; i++)
		{
			GLvoid* offset = ((char*)pointer+mArrays[i].offset);
			if(!pointer) offset =  (GLvoid*)mArrays[i].offset;
			if(mArrays[i].use)
			{
				glEnableClientState(GLArrayAttributeClientStateMap[i]);
				if(i == RUGL_ARRAY_NORMAL)
					glNormalPointer(mArrays[i].type,mArrays[i].stride,offset);
				else
					GLArrayAttributeMap[i](mArrays[i].size,mArrays[i].type,mArrays[i].stride,offset);
			}
		}
	}
	void GLArrayAttributeState::enable_client_states()
	{
		for(int i = 0; i < RUGL_ARRAY_USER_START; i++)
			if(mArrays[i].use)
				glEnableClientState(GLArrayAttributeClientStateMap[i]);
	}
	void GLArrayAttributeState::disable_all_client_states()
	{
		for(int i = 0; i < RUGL_ARRAY_USER_START; i++)
			glDisableClientState(GLArrayAttributeClientStateMap[i]);
	}
	void GLArrayAttributeState::set_attribute_pointer(const void* pointers[RUGL_ARRAY_USER_START])
	{
		//TODO
	}



	GlRenderArray::~GlRenderArray(){};
	void GlRenderArray::reset_array_attributes()
	{mArrayState.reset_array_attributes();}
	void GlRenderArray::set_array_attributes(GLArrayAttributes mAttrib, GLint size, GLint type, GLsizei stride, GLsizei offset) //offset in bytes
	{mArrayState.set_array_attributes(mAttrib,size,type,stride,offset);}
	void GlRenderArray::set_mode(GLenum aMode){mMode=aMode;}
	

	//TODO test me
	//TODO NOT WORKING, fix me
	void GLRenderVertexArray::set_data(void* data, unsigned elementCount, unsigned byteCount)
	{	
		mData = data;
		mElementCount = elementCount;
		mByteCount = byteCount;
	}
	void GLRenderVertexArray::draw()
	{
		mArrayState.disable_all_client_states();
		mArrayState.set_attribute_pointer_interleaved(mData);
		glDrawArrays(mMode,0,mCount);
	}


	GLRenderVBO::GLRenderVBO():mVBOId(0)
	{
	}
	GLRenderVBO::~GLRenderVBO()
	{
		cleanup();
	}
	void GLRenderVBO::cleanup()
	{
		if(mVBOId)
			glDeleteBuffers(1,&mVBOId);
		mVBOId = 0;
	} 
	void GLRenderVBO::set_data(void* data, unsigned elementCount, unsigned byteCount)
	{	
		if(!mVBOId)
			glGenBuffers(1,&mVBOId); //TODO error check here
		mCount = elementCount;
		glBindBuffer(GL_ARRAY_BUFFER,mVBOId);
		glBufferData(GL_ARRAY_BUFFER,byteCount,data,GL_STATIC_DRAW);
	}
	void GLRenderVBO::draw()
	{
		glBindBuffer(GL_ARRAY_BUFFER,mVBOId);
		mArrayState.disable_all_client_states();
		mArrayState.set_attribute_pointer_interleaved(0);
		glDrawArrays(mMode,0,mCount);
		//glDrawElements(mMode,mCount,GL_UNSIGNED_INT,0); //TODO needs to point to reals inidces...
		//mArrayState.disable_all_client_states();
	}

	static GLfloat HOLLOW_RECT_COORDS[5][3] = {-0.5f,0.5f,0, -0.5f,-0.5f,0, 0.5f,-0.5f,0, 0.5f,0.5f,0, -0.5f,0.5f,0};
	//TODO test this stuff

	struct CD
	{
		vec3 pos;
		color4 color;
	};
	void GLPrimitives::render_box(const rect& aRect, const color4& color)
	{

		//TODO This should call the other render_box
		glPushMatrix();
		glLoadIdentity();
		glTranslatef(aRect.get_center_2d().x,aRect.get_center_2d().y,0);
		glScalef(aRect.w,aRect.h,1);

		std::vector<CD> data;
		data.resize(5);
		memcpy(&data[0],HOLLOW_RECT_COORDS,sizeof(GLfloat)*5*3);
		vec3* fakeData = reinterpret_cast<vec3*>((void*)&data[0]);
		data[4].pos = fakeData[4];
		data[3].pos = fakeData[3];
		data[2].pos = fakeData[2];
		data[1].pos = fakeData[1];
		for(int i=0; i < data.size(); i++)
			data[i].color = color;
		GLRenderVBO render;
		render.set_mode(GL_LINE_STRIP);
		render.set_data(&data[0],5,sizeof(CD)*5);
		render.set_array_attributes(RUGL_ARRAY_VERTEX,3,GL_FLOAT,sizeof(CD));
		render.set_array_attributes(RUGL_ARRAY_COLOR,4,GL_FLOAT,sizeof(CD),sizeof(vec3));
		render.draw();
		glPopMatrix();
	}

	void GLPrimitives::render_box(const mat4x4& aMat, const color4& color)
	{
		glPushMatrix();
		glLoadMatrixf((GLfloat*)&aMat);

		std::vector<CD> data;
		data.resize(5);
		memcpy(&data[0],HOLLOW_RECT_COORDS,sizeof(GLfloat)*5*3);
		vec3* fakeData = reinterpret_cast<vec3*>((void*)&data[0]);
		data[4].pos = fakeData[4];
		data[3].pos = fakeData[3];
		data[2].pos = fakeData[2];
		data[1].pos = fakeData[1];
		for(int i=0; i < data.size(); i++)
			data[i].color = color;
		GLRenderVBO render;
		render.set_mode(GL_LINE_STRIP);
		render.set_data(&data[0],5,sizeof(CD)*5);
		render.set_array_attributes(RUGL_ARRAY_VERTEX,3,GL_FLOAT,sizeof(CD));
		render.set_array_attributes(RUGL_ARRAY_COLOR,4,GL_FLOAT,sizeof(CD),sizeof(vec3));
		render.draw();

		glPopMatrix();
	}
	
	void GLPrimitives::render_oval(const vec2& aPoint, const vec2& radii, const color4& color, const int& res)
	{
		//TODO This should call the other render_oval
		glPushMatrix();
		glLoadIdentity();
		std::vector<CD> data;
		float rp1 = res+1;
		CD templ; templ.color = color;
		data.resize(rp1,templ);
		for(int i = 0; i < rp1; i++)
		{
			data[i].pos.x = aPoint.x+radii.x*sin(i/float(res)*3.1415*2);
			data[i].pos.y = aPoint.y+radii.y*cos(i/float(res)*3.1415*2);
			data[i].pos.z = 0;
		}
		GLRenderVBO render;
		render.set_mode(GL_LINE_STRIP);
		render.set_data(&data[0],rp1,sizeof(CD)*rp1);
		render.reset_array_attributes();
		render.set_array_attributes(RUGL_ARRAY_VERTEX,3,GL_FLOAT,sizeof(CD));
		render.set_array_attributes(RUGL_ARRAY_COLOR,4,GL_FLOAT,sizeof(CD),sizeof(vec3));
		render.draw();
		glPopMatrix();
	}

	void GLPrimitives::render_oval(const mat4x4& aMat, const color4& color, const int& res)
	{
		glPushMatrix();
		glLoadMatrixf((GLfloat*)&aMat);


		std::vector<CD> data;
		float rp1 = res+1;
		CD templ; templ.color = color;
		data.resize(rp1,templ);
		for(int i = 0; i < rp1; i++)
		{
			data[i].pos.x = 0.5f*sin(i/float(res)*3.1415*2);
			data[i].pos.y = 0.5f*cos(i/float(res)*3.1415*2);
			data[i].pos.z = 0;
		}
		GLRenderVBO render;
		render.set_mode(GL_LINE_STRIP);
		render.set_data(&data[0],rp1,sizeof(CD)*rp1);
		render.reset_array_attributes();
		render.set_array_attributes(RUGL_ARRAY_VERTEX,3,GL_FLOAT,sizeof(CD));
		render.set_array_attributes(RUGL_ARRAY_COLOR,4,GL_FLOAT,sizeof(CD),sizeof(vec3));
		render.draw();
		glPopMatrix();
	}

	void GLPrimitives::render_poly(const std::vector<vec2>& points, const color4& color)
	{
		render_poly(mat4x4(),points,color);
	}
	void GLPrimitives::render_poly(const mat4x4& aMat, const std::vector<vec2>& points, const color4& color)
	{
		
		glPushMatrix();
		glLoadMatrixf((GLfloat*)&aMat);

		std::vector<CD> data;
		CD templ; templ.color = color;
		data.resize(points.size(),templ);
		for(int i = 0; i < data.size(); i++)
		{
			data[i].pos.x = points[i].x;
			data[i].pos.y = points[i].y;
		}
		GLRenderVBO render;
		render.set_mode(GL_LINE_STRIP);
		render.set_data(&data[0],data.size(),sizeof(CD)*data.size());
		render.reset_array_attributes();
		render.set_array_attributes(RUGL_ARRAY_VERTEX,3,GL_FLOAT,sizeof(CD));
		render.set_array_attributes(RUGL_ARRAY_COLOR,4,GL_FLOAT,sizeof(CD),sizeof(vec3));
		render.draw();
		glPopMatrix();
	}
}