#include "include.h"
#include <iostream>
#include <boost/signal.hpp>


void dummy_function()
{
    std::cout << "I'm printed!" << std::endl;
}

int main(int argc, char *argv[])
{
    boost::signal<void()>* signal = new  boost::signal<void()>();
    boost::signals::connection connection;
    connection = (*signal).connect(&dummy_function);
    (*signal)();
    connection.disconnect();
    delete signal;
    connection.disconnect();
    
    
	return 0;
}