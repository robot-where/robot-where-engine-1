#include "include.h"
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/foreach.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/list.hpp>
#include <boost/uuid/uuid_io.hpp> 
#include <boost/uuid/uuid_serialize.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/nil_generator.hpp>
#include <boost/uuid/random_generator.hpp>
#include <iostream>
#include <fstream>

boost::uuids::random_generator gen;
class Derived_X
{
	friend class boost::serialization::access;
    boost::uuids::uuid mId;
    Derived_X* mThis;
public:
    Derived_X():mId(),mThis(this){}
	virtual void print()
	{
        std::cout << "Derived_X: " << mId << " fake: " << mThis << " actual: " << (Derived_X*)this << std::endl;
	}
private:
	template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
        ar << BOOST_SERIALIZATION_NVP(mId);
    }
    template<class Archive>
    void load(Archive & ar, const unsigned int version) 
    {
        ar >> BOOST_SERIALIZATION_NVP(mId);
        mThis = this;
    }
    BOOST_SERIALIZATION_SPLIT_MEMBER()
};

class VectorWrapper
{
    friend class boost::serialization::access;
    std::vector<Derived_X> mVector;
    std::list<Derived_X> mList;
    std::vector<boost::uuids::uuid> mUUIDVector;
    Derived_X mDerived;
public:
    VectorWrapper()
    {
        mVector.push_back(Derived_X());
        mList.push_back(Derived_X());
        mUUIDVector.push_back(boost::uuids::nil_uuid());
    }
    void print()
    {
        std::cout << "VectorWrapper: vector: ";
        for(int i = 0; i < mVector.size(); i++)
            mVector[i].print();
        std::cout << "list: ";
        mList.begin()->print();
        std::cout << "mUUIDVector: " << mUUIDVector[0] << std::endl;
        mDerived.print();
    }
private:
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
        ar & BOOST_SERIALIZATION_NVP(mVector);
        ar & BOOST_SERIALIZATION_NVP(mList);
        ar & BOOST_SERIALIZATION_NVP(mUUIDVector);
        ar & BOOST_SERIALIZATION_NVP(mDerived);
    }
};
void vector_test()
{
    VectorWrapper v1;
    int v2 = 12;
    v1.print();
    std::string filename("serialization_test");
	{
		std::ofstream ofs(filename.c_str());
		boost::archive::xml_oarchive oa(ofs,  boost::archive::no_header );
        //boost::archive::text_oarchive oa(ofs,  boost::archive::no_header );
		std::cout << "writing to file... ";
        oa << BOOST_SERIALIZATION_NVP(v2);
		oa << BOOST_SERIALIZATION_NVP(v1);
		std::cout << "done." << std::endl;
	} 
    
	{
		std::ifstream ifs(filename.c_str());
		boost::archive::xml_iarchive ia(ifs,  boost::archive::no_header );
        //boost::archive::text_iarchive ia(ifs,  boost::archive::no_header );
		std::cout << "reading from file... ";
        
		ia >> BOOST_SERIALIZATION_NVP(v2);
        ia >> BOOST_SERIALIZATION_NVP(v1);
		std::cout << "done." << std::endl;
	}
    v1.print();
}