#include "include.h"
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/foreach.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/list.hpp>
#include <boost/uuid/uuid_io.hpp> 
#include <boost/uuid/uuid_serialize.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/nil_generator.hpp>
#include <boost/uuid/random_generator.hpp>
#include <iostream>
#include <fstream>

class ClassX
{
	friend class boost::serialization::access;
    ClassX* mThis;
public:
    ClassX(){mThis = this;}
	virtual void print()
	{
        std::cout << "ClassX: fake: " << mThis << " actual: " << (ClassX*)this << std::endl;
	}
private:
	template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
    }
    template<class Archive>
    void load(Archive & ar, const unsigned int version) 
    {
        mThis = this;
    }
    BOOST_SERIALIZATION_SPLIT_MEMBER()
};

class VectorWrapper
{
    friend class boost::serialization::access;
    std::vector<ClassX> mVector;
    ClassX mDerived;
public:
    VectorWrapper()
    {
        mVector.push_back(ClassX());
    }
    void print()
    {
        std::cout << "VectorWrapper: vector: ";
        for(int i = 0; i < mVector.size(); i++)
            mVector[i].print();
        mDerived.print();
        std::cout << std::endl;
    }
private:
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
        ar & BOOST_SERIALIZATION_NVP(mVector);
        ar & BOOST_SERIALIZATION_NVP(mDerived);
    }
};
void this_test()
{
    VectorWrapper v1;
    v1.print();
    std::string filename("serialization_test");
	{
		std::ofstream ofs(filename.c_str());
		boost::archive::xml_oarchive oa(ofs,  boost::archive::no_header );
        //boost::archive::text_oarchive oa(ofs,  boost::archive::no_header );
		std::cout << "writing to file... ";
		oa << BOOST_SERIALIZATION_NVP(v1);
		std::cout << "done." << std::endl;
	} 
    
	{
		std::ifstream ifs(filename.c_str());
		boost::archive::xml_iarchive ia(ifs,  boost::archive::no_header );
        //boost::archive::text_iarchive ia(ifs,  boost::archive::no_header );
		std::cout << "reading from file... ";
        ia >> BOOST_SERIALIZATION_NVP(v1);
		std::cout << "done." << std::endl;
	}
    v1.print();
}