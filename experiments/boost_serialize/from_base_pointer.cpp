#include <boost/serialization/serialization.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/foreach.hpp>
#include <iostream>
#include <fstream>
class Base_X
{
    friend class boost::serialization::access;
    int x;
private:
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
    }
};
class Base_A
{
	friend class boost::serialization::access;
public:
	int a;
	std::map<int,int> mMap;
    
	void print_map()
	{
		typedef std::map<int,int> MapType;
		std::cout << "\n";
		BOOST_FOREACH(const  MapType::value_type& pair, mMap )
		{
			std::cout << "map pair: " <<  pair.first << " " << pair.second << "\n";
		}
	}
	virtual void print()
	{
		std::cout << "Base_A: " << a << " ";
		print_map();
		std::cout << std::endl;
		
	}
private:
	template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
		ar & boost::serialization::make_nvp("a",a);
		ar & boost::serialization::make_nvp("mMap",mMap);
    }
};

struct Base_C
{
	int c;
	template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
		ar & boost::serialization::make_nvp("c",c);
    }
};
class Base_B
{
	friend class boost::serialization::access;
public:
	int b;
	boost::shared_ptr<Base_C> mC;
	virtual void print()
	{
		std::cout << "Base_B: " << b << " " << mC->c << std::endl;
	}
private:
	template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
		ar & boost::serialization::make_nvp("b",b);
		ar & boost::serialization::make_nvp("mC",mC);
    }
};

class Derived_AB : public Base_A, public Base_B, public Base_X
{
	friend class boost::serialization::access;
public:
	int ab;
	virtual void print()
	{
		std::cout << "Derived_AB: " << a << " " << b << " " << ab << " ";
		print_map();
		std::cout << mC->c << std::endl;
	}
private:
	template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
		ar & boost::serialization::make_nvp("Base_A",boost::serialization::base_object<Base_A>(*this));
		ar & boost::serialization::make_nvp("Base_B",boost::serialization::base_object<Base_B>(*this));
        ar & boost::serialization::make_nvp("Base_X",boost::serialization::base_object<Base_X>(*this));
		ar & boost::serialization::make_nvp("ab",ab);
    }
};

struct FakeInt
{
	int n;
	template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
		ar & BOOST_SERIALIZATION_NVP(n);
    }
};
struct SharedTest
{
	FakeInt mInt;
	boost::shared_ptr<FakeInt> mShared;
	SharedTest()
	{
		mShared = boost::shared_ptr<FakeInt>(&mInt,boost::serialization::null_deleter());
	}
	template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
		ar & BOOST_SERIALIZATION_NVP(mInt);
		ar & BOOST_SERIALIZATION_NVP(mShared);
    }
};

struct Combo
{
	Base_A mA;
	Base_B mB;
	Derived_AB mAB;
	template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
		ar &  boost::serialization::make_nvp("mA",mA);
		ar &  boost::serialization::make_nvp("mB",mB);	
		ar &  boost::serialization::make_nvp("mAB",mAB);
    }
	void print()
	{
		mA.print(); mB.print(); mAB.print();
	}
	void set_random_values()
	{
		mA.a = rand();
		mA.mMap[rand()] = rand();
		mA.mMap[rand()] = rand();
		mA.mMap[rand()] = rand();
		mB.b = rand();
		mB.mC = boost::shared_ptr<Base_C>(new Base_C());
		mB.mC->c = rand();
		mAB.a = rand();
		mAB.b = rand();
		mAB.ab = rand();
		mAB.mMap[rand()] = rand();
		mAB.mMap[rand()] = rand();
		mAB.mC = boost::shared_ptr<Base_C>(new Base_C());
		mAB.mC->c = rand();
	}
};

void test_text_archive()
{
	Combo combo; 
	combo.set_random_values();
	std::cout << "printing pre serialize" << std::endl;
	combo.print();
	
	std::string filename("serialization_test");
	{
		std::ofstream ofs(filename.c_str());
		boost::archive::text_oarchive oa(ofs);
		std::cout << "writing to file " << filename << std::endl;
		oa << combo;
		std::cout << "written" << std::endl;
	} 
	combo.set_random_values();
	//combo.print();
	{
		std::ifstream ifs(filename.c_str());
		boost::archive::text_iarchive ia(ifs);
		std::cout << "reading from file " << filename << std::endl;
		ia >> combo;
		std::cout << "printing post serialize" << std::endl;
	}
	combo.print();
    
}

void test_xml_archive()
{
	Combo combo; 
	combo.set_random_values();
	std::cout << "printing pre serialize" << std::endl;
	combo.print();
	
	std::string filename("serialization_test");
	{
		std::ofstream ofs(filename.c_str());
		boost::archive::xml_oarchive oa(ofs,  boost::archive::no_header );
		std::cout << "writing to file " << filename << std::endl;
		oa << BOOST_SERIALIZATION_NVP(combo);
		std::cout << "written" << std::endl;
	} 
	combo.set_random_values();
	//combo.print();
	{
		std::ifstream ifs(filename.c_str());
		boost::archive::xml_iarchive ia(ifs,  boost::archive::no_header );
		std::cout << "reading from file " << filename << std::endl;
		ia >> BOOST_SERIALIZATION_NVP(combo);
		std::cout << "printing post serialize" << std::endl;
	}
	combo.print();
	
}

void test_shared()
{
	SharedTest test;
	test.mInt.n = 5;
	std::cout << test.mInt.n << " " << test.mShared->n << std::endl;
	std::string filename("serialization_test");
	{
		std::ofstream ofs(filename.c_str());
		boost::archive::text_oarchive oa(ofs);
		std::cout << "writing to file " << filename << std::endl;
		oa << test;
		std::cout << "written" << std::endl;
	} 
	
	{
		std::ifstream ifs(filename.c_str());
		boost::archive::text_iarchive ia(ifs);
		std::cout << "reading from file " << filename << std::endl;
		ia >> test;
		std::cout << "printing post serialize" << std::endl;
	}
	std::cout << test.mInt.n << " " << test.mShared->n << std::endl;
    
}