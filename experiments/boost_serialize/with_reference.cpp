#include <boost/serialization/serialization.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/foreach.hpp>
#include <iostream>
#include <fstream>


struct ClassA
{
    int num;
    void print()
    {
        std::cout << "ClassA: " << num << std::endl;
    }
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
        ar & BOOST_SERIALIZATION_NVP(num);
    }
};

struct ClassB
{
    friend class boost::serialization::access;
    int num;
    ClassA& mRef;
    ClassB(ClassA& aRef):mRef(aRef)
    {
        std::cout << "creating B: "; 
        mRef.print();
    }
    void print()
    {
        std::cout << "ClassB: " << num << " ";
        mRef.print();
    }
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
        ar & BOOST_SERIALIZATION_NVP(num);
    }
  
};

namespace boost{ namespace serialization{
template<class Archive>
void save_construct_data(Archive & ar, const ClassB * t, const unsigned int file_version)
{
    ClassA* ptr = &(t->mRef);
    ar << boost::serialization::make_nvp("mRef",ptr);
}

template<class Archive>
void load_construct_data(Archive & ar, ClassB * t, const unsigned int file_version)
{
    ClassA * optr;
    ar >> boost::serialization::make_nvp("mRef",optr);
    ::new(t)ClassB(*optr);
}
}}

void reference_test()
{
    ClassA cRef;
    ClassB* cSer = new ClassB(cRef); 
    cSer->print();
    
    
    std::string filename("serialization_test");
	{
		std::ofstream ofs(filename.c_str());
		boost::archive::xml_oarchive oa(ofs,  boost::archive::no_header );
		std::cout << "writing to file... ";
		oa << BOOST_SERIALIZATION_NVP(cSer);
		std::cout << "done." << std::endl;
	} 
    
    ClassB* dSer;
	{
		std::ifstream ifs(filename.c_str());
		boost::archive::xml_iarchive ia(ifs,  boost::archive::no_header );
		std::cout << "reading from file... ";
		ia >> BOOST_SERIALIZATION_NVP(dSer);
		std::cout << "done." << std::endl;
	}
    dSer->print();
	
}