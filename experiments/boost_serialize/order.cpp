#include "include.h"
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/foreach.hpp>
#include <iostream>
#include <fstream>

class A
{
    friend class boost::serialization::access;
public:
    int a, b, c, d, e, f;
    char h;
private:
    template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
        ar << BOOST_SERIALIZATION_NVP(a);
        ar << BOOST_SERIALIZATION_NVP(b);
        ar << BOOST_SERIALIZATION_NVP(c);
        ar << BOOST_SERIALIZATION_NVP(d);
        ar << BOOST_SERIALIZATION_NVP(e);
        ar << BOOST_SERIALIZATION_NVP(f);
        ar << BOOST_SERIALIZATION_NVP(h);
    }
    template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
        ar >> BOOST_SERIALIZATION_NVP(h);
		ar >> BOOST_SERIALIZATION_NVP(f);
        ar >> BOOST_SERIALIZATION_NVP(e);
        ar >> BOOST_SERIALIZATION_NVP(d);
        ar >> BOOST_SERIALIZATION_NVP(c);
        ar >> BOOST_SERIALIZATION_NVP(b);
        ar >> BOOST_SERIALIZATION_NVP(a);
    }
    BOOST_SERIALIZATION_SPLIT_MEMBER()
};

void order_test()
{
    int a, b, c, d, e, f;
    a = 1;
    b = 2;
    c = 3;
    d = 4;
    e = 5;
    f = 6;
    char h = '7';
    
    std::cout << a << " " << b << " " << c << " " << d << " " << e << " " << f << " " << h << std::endl;
    
    std::string filename("serialization_test");
	{
		std::ofstream ofs(filename.c_str());
		//boost::archive::xml_oarchive oa(ofs,  boost::archive::no_header );
        boost::archive::text_oarchive oa(ofs,  boost::archive::no_header );
		std::cout << "writing to file... ";
		oa << BOOST_SERIALIZATION_NVP(a);
        oa << BOOST_SERIALIZATION_NVP(b);
        oa << BOOST_SERIALIZATION_NVP(c);
        oa << BOOST_SERIALIZATION_NVP(d);
        oa << BOOST_SERIALIZATION_NVP(e);
        oa << BOOST_SERIALIZATION_NVP(f);
        oa << BOOST_SERIALIZATION_NVP(h);
		std::cout << "done." << std::endl;
	} 
    
	{
		std::ifstream ifs(filename.c_str());
		//boost::archive::xml_iarchive ia(ifs,  boost::archive::no_header );
        boost::archive::text_iarchive ia(ifs,  boost::archive::no_header );
		std::cout << "reading from file... ";
        ia >> BOOST_SERIALIZATION_NVP(h);
		ia >> BOOST_SERIALIZATION_NVP(f);
        ia >> BOOST_SERIALIZATION_NVP(e);
        ia >> BOOST_SERIALIZATION_NVP(d);
        ia >> BOOST_SERIALIZATION_NVP(c);
        ia >> BOOST_SERIALIZATION_NVP(b);
        ia >> BOOST_SERIALIZATION_NVP(a);
		std::cout << "done." << std::endl;
	}
    std::cout << a << " " << b << " " << c << " " << d << " " << e << " " << f << " " << h << std::endl;

}