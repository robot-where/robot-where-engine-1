#pragma once
#include "RuComponentRenderableBase.h" 
namespace RU
{
	class WorldPhysics;
	class PhysicsIntermediary;
	class ComponentPhysicsBase : public ComponentRenderableBase
	{
		friend class boost::serialization::access;
		friend class WorldPhysics;
		friend class PhysicsIntermediary;

		//DO NOT serialize this
		PhysicsIdType mPhysicsId;
        PhysicsIntermediary* mIntermediary;
        bool mDirty;//this means that physics properties other than position (which is controlled by ComponentTransform) has changedh
                    //TODO rename this to be more descriptive Peter
	private:
        void set_intermediary(PhysicsIntermediary* aIntermediary);
        void set_clean(){mDirty=false;} //this should only be called from physics intermediary's compute_positions function...
		//TODO these need to call setup functions on renderablebase onyl if we want renderable physics entities...
	protected:
        SpatialPosition mLastBoxPosition;
		void derived_setup();
		void derived_cleanup();
		ComponentPhysicsBase():mPhysicsId(NULL_PHYSICS_ID),mIntermediary(NULL),mDirty(true){}	
		virtual void detach_from_physics()=0; //actually I think this needs to be virtual too...
		virtual void attach_to_physics()=0;
		PhysicsIdType get_physics_id(){return mPhysicsId;}
        WorldPhysics& get_world_physics();
        PhysicsIntermediary* get_intermediary();
        void set_dirty(){mDirty=true;}
        bool is_dirty(){return mDirty;}
	public:
		virtual ~ComponentPhysicsBase();
		virtual ComponentInfoType get_component_info() const;
		virtual rect get_render_AABB();
	private:
		template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
			ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(ComponentRenderableBase);
        }
	};
    
    class ComponentJoke : public ComponentBase
    {
		friend class boost::serialization::access;
    public:
		~ComponentJoke();
		virtual ComponentInfoType get_component_info() const;
        static std::string name(){return "ComponentJoke";}
    private:
		template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
			ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(ComponentBase);
        }
    };
}
BOOST_CLASS_EXPORT_KEY(RU::ComponentPhysicsBase)
BOOST_CLASS_EXPORT_KEY(RU::ComponentJoke)