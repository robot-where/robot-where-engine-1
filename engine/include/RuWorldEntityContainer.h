#pragma once
#include "RuEntityBase.h"
#include <boost/bimap.hpp>
#include <vector>
#include <map>
#include <algorithm>
namespace RU
{
	class ComponentCamera;
	class ComponentRenderBase;
	class Primary;
    //this contains all entity containers as understood by RuMain
    //this contains all add/remove functions for entities
    class WorldEntityContainer
    {
        //-----------------
        //deserialization of entity references
        //-----------------
        typedef std::set<EntityReference*> EntityReferenceContainerType;
		static EntityReferenceContainerType sDeserializingReferences;
        static void clear_deserializing_references();
    public:
        static void register_entity_reference_for_deserialization(EntityReference* aRef);
		static void swap_entity_reference_for_deserialization(const EntityReference* aOld, EntityReference* aNew); //during boost::deserialization, constructors may copy references, this updates the address
        
        //deserialization pipeline
        //deserialize WorldEntityContainer(WEC)
            //during deserialization, entities register themselves
            //during deserialization, entities may be copy constructed, but when this happens it will update address using swap_entity_reference_for_deserialization
        //call set_primary on WEC, determise which Primary is associated with this WEC
        //call setup_after_deserialization on WEC, this reconnects all references
        
        //another idea, use a special serializing reference class for deserializing all referenced objects and use additional information to construct true reference type. This way, we avoid the template problem because no templated class is ever deserialized
            //one problem here btw, is that component references now must be searched for directly and not via its entity base class :(((
            //perhaps if all referencable objects are owned by entities?? 
        
        
        
	public:
        weak_ptr<EntityBase> get_last_created_entity(); //this is mainly for convenience
	private:
		friend class Primary;
		friend class boost::serialization::access;
        typedef std::map<ObjectIdType,shared_ptr<EntityBase> > EntityContainerType;
		//TO BE serialized
        EntityContainerType mGlobalEntityContainer;

		//NOT to be serialized
		typedef std::list<ComponentCamera* > CameraContainerType;
		CameraContainerType mCameraContainer; 
		weak_ptr<Primary> mPrimary; //TODO should be reference maybe?
        weak_ptr<EntityBase> mLastCreatedEntity;
    public: //TODO should be private
		void set_primary(weak_ptr<Primary> aPrimary){mPrimary = aPrimary;}
		void destroy_all();
		void setup_after_deserialization();
    public:
		//------------
		//render related
		//------------
        CameraContainerType::iterator find_camera_component(ComponentCamera* aCamera);
        void update_camera_component_depth(ComponentCamera* aCamera);
		void register_camera_component(ComponentCamera* aCamera);
        void deregister_camera_component(ComponentCamera* aCamera);
		void render_all_cameras(ScreenSpatialPosition aScreen);
        
    

		//------------
		//general
		//------------
        weak_ptr<EntityBase> get_entity(const ObjectIdType& aId) const
        {
            EntityContainerType::const_iterator it = mGlobalEntityContainer.find(aId);
			if(it != mGlobalEntityContainer.end()) 
                return it->second;
            return weak_ptr<EntityBase>();
        }
        weak_ptr<EntityBase> create_entity(std::string aName="")
        {
            shared_ptr<EntityBase> ent(new EntityBase());
            ent->set_name(aName);
			ObjectIdType eid = get_new_object_id();
			mGlobalEntityContainer[eid] = ent;
			ent->setup_entity(eid,ent,mPrimary);
            ent->setup_entity_contents();
            mLastCreatedEntity = ent;
            return ent;
        }
        void destroy_entity(const ObjectIdType& aId)
        {	
            //TODO other stuff???
			EntityContainerType::iterator it = mGlobalEntityContainer.find(aId);
			if(it != mGlobalEntityContainer.end())
            {
				it->second->base_destroy();

            }
			//RU_ASSERT(it->second.use_count()==1);
            mGlobalEntityContainer.erase(aId);
        }
		//------------
		//debug
		//------------
		void print_entities()
		{
			std::cout << "GlobalEntityContainer:\n";
			std::cout << "# of entities: " << mGlobalEntityContainer.size() << std::endl;
            for(EntityContainerType::const_iterator it = mGlobalEntityContainer.begin(); it != mGlobalEntityContainer.end(); it++)
            {
                if(it->second->is_top_level())
                    it->second->print_hierarchy_recursive();
            }

		}
	private:
		template<class Archive>
		void save(Archive & ar, const unsigned int version) const
		{
			//TODO prune out the entities we said not to serialize!
			ar << BOOST_SERIALIZATION_NVP(mGlobalEntityContainer);
		}

		template<class Archive>
		void load(Archive & ar, const unsigned int version)
		{
			ar >> BOOST_SERIALIZATION_NVP(mGlobalEntityContainer);
		}
		template<class Archive>
		void serialize(Archive & ar,const unsigned int file_version )
		{
			boost::serialization::split_member(ar, *this, file_version);
		}
    };
}