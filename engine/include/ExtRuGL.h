#pragma once

//because I'm lazy and used some gl types in here... you should replace these with their matching compatible types
#ifdef __APPLE__
#include "OpenGl/gl.h"
#else
#ifdef _WIN32
#include <windows.h>
#endif
#include <GL/gl.h>
#endif
#include "RuTypesSerialization.h"

namespace RU
{
	enum GLArrayAttributes
	{
		RUGL_ARRAY_VERTEX = 0,
		RUGL_ARRAY_COLOR,
		RUGL_ARRAY_NORMAL,
		RUGL_ARRAY_TEXTURE,
		RUGL_ARRAY_USER_START //start user array attributes here
	};

	class GLArrayAttributeState
	{
		friend class boost::serialization::access;
		struct GLArrayPointerInternal
		{
			friend class boost::serialization::access;
			bool use;
			GLint size;
			GLint type;
			GLsizei stride;
			GLsizei offset;
			GLArrayPointerInternal():use(false){}
		private:
			template<class Archive>
			void serialize(Archive & ar, const unsigned int version)
			{
				ar & BOOST_SERIALIZATION_NVP(use);
				ar & BOOST_SERIALIZATION_NVP(size);
				ar & BOOST_SERIALIZATION_NVP(type);
				ar & BOOST_SERIALIZATION_NVP(stride);
				ar & BOOST_SERIALIZATION_NVP(offset);
			}
		};
		GLArrayPointerInternal mArrays[RUGL_ARRAY_USER_START];
	public:
		void reset_array_attributes();
		void set_array_attributes(GLArrayAttributes mAttrib, GLint size, GLint type = GL_FLOAT, GLsizei stride = 0, GLsizei offset = 0);
		void set_attribute_pointer_interleaved(const void* pointer);
		void enable_client_states();
		void disable_all_client_states();
		void set_attribute_pointer(const void* pointers[RUGL_ARRAY_USER_START]);
	private: 
		template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
			ar & BOOST_SERIALIZATION_NVP(mArrays);
        }
	};
	class GlRenderArray
	{
		friend class boost::serialization::access;
	protected:
		GLArrayAttributeState mArrayState;
		GLenum mMode;
		GLsizei mCount;
	public:
		//TODO setting index array...
		virtual ~GlRenderArray();
		void reset_array_attributes();
		void set_array_attributes(GLArrayAttributes mAttrib, GLint size, GLint type = GL_FLOAT, GLsizei stride = 0, GLsizei offset = 0);
		void set_mode(GLenum aMode);
	private:
		template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
			ar & BOOST_SERIALIZATION_NVP(mArrayState);
			ar & BOOST_SERIALIZATION_NVP(mMode);
			ar & BOOST_SERIALIZATION_NVP(mCount);
        }
	};
	class GLRenderVertexArray : public GlRenderArray
	{
		friend class boost::serialization::access;
		void* mData;
		unsigned mElementCount;
		unsigned mByteCount;
	public:
		void set_data(void* data, unsigned elementCount, unsigned byteCount);
		void draw();
	private:
		template<class Archive>
		void save(Archive & ar, const unsigned int version) const
		{
			ar << BOOST_SERIALIZATION_BASE_OBJECT_NVP(GlRenderArray);
		}

		template<class Archive>
		void load(Archive & ar, const unsigned int version)
		{
			ar >> BOOST_SERIALIZATION_BASE_OBJECT_NVP(GlRenderArray);
			mData = NULL;
			mElementCount = 0;
			mByteCount = 0;
		}
		template<class Archive>
		void serialize(Archive & ar,const unsigned int file_version )
		{
			boost::serialization::split_member(ar, *this, file_version);
		}
	};
	class GLRenderVBO : public GlRenderArray
	{
		friend class boost::serialization::access;
		GLuint mVBOId;
	public:
		GLRenderVBO();
		~GLRenderVBO();
		void cleanup();
		void set_data(void* data, unsigned elementCount, unsigned byteCount);
		void draw();
	private:
		template<class Archive>
		void save(Archive & ar, const unsigned int version) const
		{
			ar << BOOST_SERIALIZATION_BASE_OBJECT_NVP(GlRenderArray);
		}

		template<class Archive>
		void load(Archive & ar, const unsigned int version)
		{
			ar >> BOOST_SERIALIZATION_BASE_OBJECT_NVP(GlRenderArray);
			mVBOId = 0;
		}
		template<class Archive>
		void serialize(Archive & ar,const unsigned int file_version )
		{
			boost::serialization::split_member(ar, *this, file_version);
		}
	};	

	struct GLPrimitives
	{
		static void render_box(const rect& aRect, const color4& color = color4(1,1,1,1));
		static void render_box(const mat4x4& aMat, const color4& color = color4(1,1,1,1)); //if aMat = identity, than this renders a 1x1 box centered at the origin of a 2x2 screen
		static void render_oval(const vec2& aPoint, const vec2& radii, const color4& color = color4(1,1,1,1), const int& res = 10);
		static void render_oval(const mat4x4& aMat, const color4& color = color4(1,1,1,1), const int& res = 10);
		static void render_poly(const std::vector<vec2>& points, const color4& color = color4(1,1,1,1));
		static void render_poly(const mat4x4& aMat, const std::vector<vec2>& points, const color4& color = color4(1,1,1,1));
	};
}