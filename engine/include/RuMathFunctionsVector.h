#pragma once
#include "RuMathTypes.h"
#include <glm/gtx/norm.hpp>
namespace RU
{
    namespace MATH
    {
        
        
        //------------
        //conversion operations
        //------------
		template <typename T>
        glm::detail::tvec2<T> vec3_to_vec2(const glm::detail::tvec3<T>& aVec)
        {
            return glm::detail::tvec2<T>(aVec.x,aVec.y);
        }
        template <typename T>
        glm::detail::tvec3<T> vec2_to_vec3(const glm::detail::tvec2<T>& aVec)
        {
            return glm::detail::tvec3<T>(aVec.x,aVec.y,0);
        }
        template <typename T>
        glm::detail::tvec3<T> vec4_homogeneous_to_vec3(const glm::detail::tvec4<T>& aVec)
        {
            if(aVec.w == 0) return glm::detail::tvec3<T>();
            return glm::detail::tvec3<T>(aVec.x/aVec.w,aVec.y/aVec.w,aVec.z/aVec.w);
        }
        template <typename T>
        glm::detail::tvec4<T> vec3_to_vec4_homogeneous(const glm::detail::tvec3<T>& aVec)
        {
            return glm::detail::tvec4<T>(aVec.x,aVec.y,aVec.z,1);
        }
        
        //-------------
        //length operations
        //-------------
        template<typename T>
        T vec3_length(const glm::detail::tvec3<T>& aVec)
        {
            return glm::l2Norm(aVec);
        }
        template<typename T>
        T vec4_homogeneous_length(const glm::detail::tvec4<T>& aVec)
        {
            return glm::l2Norm(vec4_homogeneous_to_vec3(aVec));
        }
        
        //-------------
        //other operations
        //-------------
        template<typename T>
        glm::detail::tvec4<T> vec4_homogeneous_invert(const glm::detail::tvec4<T>& aVec)
        {
            return glm::detail::tvec3<T>(-aVec.x,-aVec.y,-aVec.z,aVec.w); 
        }
    }
}