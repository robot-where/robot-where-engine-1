#pragma once
#include "RuPhysicsIntermediary.h"
#include <vector>
namespace RU
{
    class Primary;
    class WorldPhysics
    {
		friend class ComponentPhysicsBase;
		Primary& mPrimary;
		unsigned mVelocityIterations, mPositionIterations;
		shared_ptr<b2World> mBoxWorld;
		PhysicsObjectsContainerType mInteractingObjects;

		PhysicsIdType create_new_physics_id();
		PhysicsObjectsContainerType::iterator create_physics_intermediary(PhysicsIdType aId);
		void get_obtainable_colliders_in_children(EntityBase& aBase, std::vector<ComponentColliderBase* >& output);
		//this function is also used for setting up physics callbacks
		ComponentRigidBody* get_parent_rigid_body(EntityBase& aBase);
		ComponentColliderBase* get_collider(EntityBase& aBase);
	public:
		WorldPhysics(Primary& aPrimary);
		void print_entities();
		//TODO delete, I don't htink I use this...
		PhysicsIntermediary* get_physics_object(PhysicsIdType aId);
		void update_physics(float aDt);
		void add_component(ComponentRigidBody*  aRigidBody);
		void add_component(ComponentColliderBase*  aCollider);
        void add_component(ComponentJointBase*  aCollider);
		void remove_component(ComponentRigidBody*  aRigidBody);
		void remove_component(ComponentColliderBase*  aCollider);
        void remove_component(ComponentJointBase*  aCollider);
    };
}