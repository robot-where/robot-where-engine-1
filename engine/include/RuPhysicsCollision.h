#pragma once

namespace RU 
{
    class ComponentColliderBase;
    struct CollisionInfo
    {
        ComponentColliderBase* c1;
        ComponentColliderBase* c2;
        //additional info
        
    };
}