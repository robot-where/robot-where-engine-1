#pragma once
#include <map>
#include <Box2D/Box2D.h>
#include <boost/signal.hpp>
#include "RuPhysicsCommon.h"
#include "RuTypes.h"
#include "RuComponentRigidBody.h"
#include "RuComponentCollidable.h"
#include "RuComponentPhysicsJoint.h"
#include "RuException.h"

namespace RU
{
    //TODO worldphyiscs should not be a friend class and physics intermediary should have all the public 
    class WorldPhysics;
    typedef std::map<PhysicsIdType,PhysicsIntermediary> PhysicsObjectsContainerType;
    class PhysicsIntermediary
    {
        friend class WorldPhysics;
        friend class ComponentPhysicsBase;
        friend class ComponentColliderBase;
        friend class ComponentRigidBody;
        
        //box2d core related
        weak_ptr<b2World> mBoxWorld;
        b2Body* mBoxBody;
        PhysicsIdType mPhysicsId;
        ComponentRigidBody*  mRigidBody;
        
        //collider related 
        typedef std::map<ComponentColliderBase*, b2Fixture*> ColliderContainerType; //should be FLAT MAP
        ColliderContainerType mColliders;
        
        //joint related
        typedef std::map<ComponentJointBase*, b2Joint*> JointContainerType; //should be FLAT MAP
        JointContainerType mJoints;
        
        SpatialPosition mLastAbsoluteSpatialPosition;
    public:
        PhysicsIntermediary():mBoxBody(NULL),mPhysicsId(NULL_PHYSICS_ID){}
        ~PhysicsIntermediary();
        void add_component(ComponentRigidBody* aRigidBody);
        void add_component(ComponentColliderBase* aCollider);
        void add_component(ComponentJointBase* aJoint);
        void remove_component(ComponentColliderBase* aCollider);
        void remove_component(ComponentJointBase* aJoint);
        SpatialPosition get_body_position();
        void compute_positions(const SpatialPosition& aSp);
        unsigned get_collider_count(){return mColliders.size();}
        bool is_fake(){return !mRigidBody->is_fake();}
    private:
        void set_physics_id(PhysicsIdType aId){mPhysicsId=aId;}
        void set_world(weak_ptr<b2World> aWorld){mBoxWorld=aWorld;}
        void set_last_box_spatial_position(const SpatialPosition& aSp);
        void set_mass(const float& aMass);
        void create_body();
        void print();

    };
}