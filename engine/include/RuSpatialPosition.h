#pragma once
#include "RuTypesSerialization.h"
#include "RuMathFunctionsQuaternion.h"
#include <RuMathTypesStream.h>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>

namespace RU
{
    //SCALE ROTATE THEN TRANSLATE i.e mat = T * S * R
    //LEFT is LOCAL and RIGHT is GLOBAL
    struct SpatialPosition
    {
        vec3 position;
        vec3 scale;
        quat rotation;
        SpatialPosition():position(0,0,0),scale(1,1,1),rotation(){}
		SpatialPosition(const vec3& aPosition):position(aPosition),scale(1,1,1),rotation(){}
		SpatialPosition(const mat4x4& aMat);
        
        vec2 get_2d_position() const {return vec2(position.x,position.y);}
        vec2 get_2d_scale() const {return vec2(scale.x,scale.y);}
		float get_2d_rotation(){return MATH::to_radians(get_2d_rotation_degrees());}//TODO is this correct?? (should be for 2d objects...)
		float get_2d_rotation_degrees(){return glm::eulerAngles(rotation).z;}
        void set_2d_position(const vec2& aPos){position.x=aPos.x;position.y=aPos.y;}
        void set_2d_scale(const vec2& aScale){scale.x=aScale.x;scale.y=aScale.y;}
        void set_2d_rotation(const float& aRot){rotation=quat(vec3(0,0,aRot));}//TODO 
        void set_2d_rotation_degrees(const float& aRot){set_2d_rotation(MATH::to_radians(aRot));}
        void set_position(const vec3& aPos);
        void set_scale(const vec3& aScale);
        void set_rotation(const quat& aRot);
        void left_2d_scale(const vec2& aScale);
        void left_2d_rotate(const float& aRot);
        void left_2d_translate(const vec2& aTrans);
        void right_2d_scale(const vec2& aScale);
        void right_2d_rotate(const float& aRot);
        void right_2d_translate(const vec2& aTrans);
        void left_scale(const vec3& aScale);
        void left_rotate(const quat& aRot);
        void left_translate(const vec3& aTrans);
        void right_scale(const vec3& aScale);
        void right_rotate(const quat& aRot);
        void right_translate(const vec3& aTrans);
        
        rect to_rect() const {return rect(position.x,position.y,position.z,0,0,0);} //????wtf is this for again
		mat4x4 get_matrix() const;
		SpatialPosition get_inverse() const;
    };
    
    //computes spatial position of A relative to B
    inline SpatialPosition operator/(const SpatialPosition& A, const SpatialPosition& B)
    {
        //return SpatialPosition(glm::inverse(B.get_matrix())*A.get_matrix());
        SpatialPosition r;
        r.position = glm::inverse(B.rotation)*((A.position-B.position)*B.scale);
        r.scale = A.scale/B.scale;
        r.rotation = A.rotation*glm::inverse(B.rotation);
        return r;
    }
    
    //NOT commutative, merely inverts the above operation
	inline SpatialPosition operator*(const SpatialPosition& A, const SpatialPosition& B)
	{
        //return SpatialPosition(B.get_matrix()*A.get_matrix());
		SpatialPosition r;
        r.position = B.rotation*(A.position)/B.scale+B.position;
        r.scale = A.scale*B.scale;
        r.rotation = A.rotation*B.rotation;
        return r;
	}

	inline vec3 operator*(const SpatialPosition& A, const vec3& B)
	{
		vec3 r(B.x*A.scale.x, B.y*A.scale.y, B.z*A.scale.z);
		r = A.rotation*r + A.position;
		return r;
		//check against this
		//return MATH::vec4_homogeneous_to_vec3(A.get_matrix()*MATH::vec3_to_vec4_homogeneous(B));
	}

    
    inline bool operator==(const SpatialPosition& A, const SpatialPosition& B)
	{
        return A.position == B.position && A.rotation == B.rotation && A.scale == B.scale;
    }
    inline bool operator!=(const SpatialPosition& A, const SpatialPosition& B)
	{
        return !(A==B);
    }	
    
	//this includes information to go between our game coordinates, and standard gl coordinates
    struct ScreenSpatialPosition
    {
        SpatialPosition sp;
        float screenWidth, screenHeight;
        mat4x4 get_spatial_position_matrix() const;
		mat4x4 get_screen_matrix() const; //use this to convert from game coordinates to gl coordinates
        mat4x4 get_spatial_position_with_screen_matrix() const;
		rect get_rectangle() const;
    };
}

inline std::ostream& operator<< (std::ostream &out, const RU::SpatialPosition& sp)
{
	out << "position: " << sp.position << "\n";
	out << "scale: " << sp.scale << "\n";
	out << "rotation: " << sp.rotation << "\n";
	return out;
}
inline std::ostream& operator<< (std::ostream &out, const RU::ScreenSpatialPosition& sp)
{
	out << sp.sp << "\n";
	out << "width: " << sp.screenWidth << "\n";
	out << "height: " << sp.screenHeight << "\n";
	return out;
}



namespace boost {
    namespace serialization {
        template<class Archive>
        void serialize(Archive & ar, RU::SpatialPosition & o, const unsigned int version)
        {
            ar & BOOST_SERIALIZATION_NVP(o.position);
            ar & BOOST_SERIALIZATION_NVP(o.scale);
            ar & BOOST_SERIALIZATION_NVP(o.rotation);
        }
    }
}