#pragma once
//used for serializationt
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/archive/xml_oarchive.hpp>

#include <boost/serialization/export.hpp>     


#include <boost/serialization/map.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/string.hpp> 
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/weak_ptr.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/access.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/uuid/uuid_io.hpp> 
#include <boost/uuid/uuid_serialize.hpp>
#include "RuMathTypesSerialization.h"


#include <boost/config.hpp> 
#include <boost/filesystem/path.hpp> 
#include <boost/serialization/level.hpp> 

namespace boost { namespace serialization { 
    
    //I hope this is right
    template<class Archive>
    void serialize(Archive& ar, boost::filesystem::path& p, 
                   const unsigned int version) 
    { 
        std::string s; 
        if(Archive::is_saving::value) 
            s = p.string(); 
        ar & boost::serialization::make_nvp("string", s); 
        if(Archive::is_loading::value) 
            p = s; 
    } 
    
}} 