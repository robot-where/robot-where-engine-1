#pragma once
#include "RuTypesSerialization.h"
#include "RuTypes.h"
#include "RuComponentBase.h"
#include <map>

namespace RU
{
	class EntityBase;
    class ComponentManager
    {
		friend class boost::serialization::access;
        friend class EntityBase;
		//using string for now to test out serialization
		//but eventually you'll want to make some awesome tiered map that does exactly what you want!
		typedef std::map<std::string,shared_ptr<ComponentBase> > ComponentMapType;
        ComponentMapType mComponentMap;

		//DO NOT serialize
		//consider making this a raw pointer since componentmanager is owned by EntityBase hence would always be valid
		EntityBase& mOwner;
	private:	
		void setup_all();

		//internal
		void setup_component(shared_ptr<ComponentBase> aComponent);
		template<typename T>
		std::string get_component_map_key();
		template<typename T>
        shared_ptr<T> add_existing_component(shared_ptr<T> aComponent);
    public:
		ComponentManager(EntityBase& aOwner):mOwner(aOwner){}
		~ComponentManager(){}
        
        //---------
        //component operations
        //---------
        template<typename T>
        weak_ptr<T> add_component();
        template<typename T>
        void remove_component();
        template<typename T>
        weak_ptr<T> get_component(); //TODO maket his const
        void remove_all_components();
        
        
    private:
		template<class Archive>
		void save(Archive & ar, const unsigned int version) const
		{
			ComponentMapType& non_const_component_map = const_cast<ComponentMapType&>(mComponentMap);
			ComponentMapType no_serial_map;
			for(ComponentMapType::iterator it = non_const_component_map.begin(); it != non_const_component_map.end(); it++)
				if(!it->second->do_serialize())
					no_serial_map[it->first] = it->second;
			for(ComponentMapType::iterator it = no_serial_map.begin(); it != no_serial_map.end(); it++)
				non_const_component_map.erase(it->first);
			ar << BOOST_SERIALIZATION_NVP(mComponentMap);
			for(ComponentMapType::iterator it = no_serial_map.begin(); it != no_serial_map.end(); it++)
				non_const_component_map[it->first] = it->second;
		}

		template<class Archive>
		void load(Archive & ar, const unsigned int version)
		{
			ar >> BOOST_SERIALIZATION_NVP(mComponentMap);
		}
		BOOST_SERIALIZATION_SPLIT_MEMBER()
    };
	template<typename T>
	std::string ComponentManager::get_component_map_key(){return T::name();}
	template<typename T>
    shared_ptr<T> ComponentManager::add_existing_component(shared_ptr<T> aComponent)
	{
		remove_component<T>();
		mComponentMap[get_component_map_key<T>()] = aComponent;
		setup_component(aComponent);
		return aComponent;
	}
    //---------
    //component operations
    //---------
    template<typename T>
    weak_ptr<T> ComponentManager::add_component()
	{
		shared_ptr<T> r = shared_ptr<T>(new T());
		add_existing_component(r);
		r->on_create();
		return r;
	}
    template<typename T>
    void ComponentManager::remove_component()
	{
		ComponentMapType::iterator it = mComponentMap.find(get_component_map_key<T>());
		if(it != mComponentMap.end())
		{
			it->second->on_destroy();
			it->second->derived_cleanup();
			it->second->base_cleanup();
			mComponentMap.erase(it);
		}
	}
    template<typename T>
    weak_ptr<T> ComponentManager::get_component()
	{
		ComponentMapType::iterator it = mComponentMap.find(get_component_map_key<T>());
		if(it == mComponentMap.end()) return weak_ptr<T>();
		return static_pointer_cast<T>(it->second);
	}
}