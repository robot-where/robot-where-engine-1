#pragma once
#include "RuMathTypes.h"
const float PHSYICS_CONVERSION_RATIO = 30;
namespace RU
{
	struct PhysicsSettings
	{        
        static float world_to_physics(float len){return len*(1/PHSYICS_CONVERSION_RATIO);}
        static vec2 world_to_physics(const vec2& vec){return vec*(1/PHSYICS_CONVERSION_RATIO);}
        static float world_rotation_to_physics(float rot){return rot;}
        static float physics_to_world(float len){return len*PHSYICS_CONVERSION_RATIO;}
        static vec2 physics_to_world(const vec2& vec){return vec*PHSYICS_CONVERSION_RATIO;}
        static float physics_rotation_to_world(float rot){return rot;}
	};
    
	struct CollisionInfoStruct
	{
		
	};
    
	struct CollisionSignalStruct
	{
		//TODO something like this
		//typedef void (CollisionEventSignature)(ComponentColliderBase* );
	};
}