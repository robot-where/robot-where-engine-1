#pragma once
#include "RuTypes.h"
#include "RuComponentBase.h"
#include "RuSpatialPosition.h"
namespace RU
{
	class ComponentCamera : public ComponentBase
	{
		friend class boost::serialization::access;
		float mDepth;
		rect mNormalizedViewportRect; //lowerleft coner to upper right
		void derived_setup();
		void derived_cleanup();
	public:
		ComponentCamera();
		static std::string name(){return "ComponentRenderImage";}
		float get_depth() const;
		void set_depth(float aDepth);
		ScreenSpatialPosition get_camera_screen(ScreenSpatialPosition aScreen);
	private:
		template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
			ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(ComponentBase);
        }
	};
}
BOOST_CLASS_EXPORT_KEY(RU::ComponentCamera);