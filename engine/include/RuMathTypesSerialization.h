#pragma once
#include <boost/serialization/nvp.hpp>
#include "RuMathTypes.h"


namespace boost 
{
    namespace serialization 
    {
		//vec2
		template<class Archive, typename T>
        void serialize(Archive & ar, glm::detail::tvec2<T> & m, const unsigned int version)
        {
            ar & BOOST_SERIALIZATION_NVP(m.x);
			ar & BOOST_SERIALIZATION_NVP(m.y);
        }
		//vec3
        template<class Archive, typename T>
        void serialize(Archive & ar, glm::detail::tvec3<T> & m, const unsigned int version)
        {
            ar & BOOST_SERIALIZATION_NVP(m.x);
			ar & BOOST_SERIALIZATION_NVP(m.y);
			ar & BOOST_SERIALIZATION_NVP(m.z);
        }
		//vec4
		template<class Archive, typename T>
        void serialize(Archive & ar, glm::detail::tvec4<T> & m, const unsigned int version)
        {
            ar & BOOST_SERIALIZATION_NVP(m.x);
			ar & BOOST_SERIALIZATION_NVP(m.y);
			ar & BOOST_SERIALIZATION_NVP(m.z);
			ar & BOOST_SERIALIZATION_NVP(m.w);
        }
		//quat
		template<class Archive, typename T>
        void serialize(Archive & ar, glm::detail::tquat<T> & m, const unsigned int version)
        {
            ar & BOOST_SERIALIZATION_NVP(m.x);
			ar & BOOST_SERIALIZATION_NVP(m.y);
			ar & BOOST_SERIALIZATION_NVP(m.z);
			ar & BOOST_SERIALIZATION_NVP(m.w);
        }
		//mat4x4
		template<class Archive, typename T>
        void serialize(Archive & ar, glm::detail::tmat4x4<T> & m, const unsigned int version)
        {
            ar & BOOST_SERIALIZATION_NVP(m.value);
        }
		//rect
		template<class Archive, typename T>
        void serialize(Archive & ar, RU::trect<T> & m, const unsigned int version)
        {
            ar & BOOST_SERIALIZATION_NVP(m.x);
			ar & BOOST_SERIALIZATION_NVP(m.y);
			ar & BOOST_SERIALIZATION_NVP(m.z);
			ar & BOOST_SERIALIZATION_NVP(m.w);
			ar & BOOST_SERIALIZATION_NVP(m.h);
			ar & BOOST_SERIALIZATION_NVP(m.d);
        }
        //rect2
		template<class Archive, typename T>
        void serialize(Archive & ar, RU::trect2<T> & m, const unsigned int version)
        {
            ar & BOOST_SERIALIZATION_NVP(m.x);
			ar & BOOST_SERIALIZATION_NVP(m.y);
			ar & BOOST_SERIALIZATION_NVP(m.w);
			ar & BOOST_SERIALIZATION_NVP(m.h);
        }

    } 
}