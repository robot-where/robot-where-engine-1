#pragma once
#include "RuComponentBase.h"
#include "RuSpatialPosition.h"
#include "RuTypes.h"
#include <set>

namespace RU
{
    class EntityBase;
    class Transform : public ComponentBase
    {
		friend class boost::serialization::access;
		friend class EntityBase;
        SpatialPosition mAbsoluteSpatialPosition;
		SpatialPosition mRelativeSpatialPosition;
		bool mDirty;
	private:
		void flag_children_dirty();
    public:
		Transform():mDirty(true){}
		static std::string name();
		bool do_serialize() const {return false;} //NOTE Transfrom is normally serialized, just not through the component manager
		SpatialPosition get_absolute();
		SpatialPosition get_relative() const;
		void set_absolute(const SpatialPosition& aSP);
		void set_relative(const SpatialPosition& aSP);
        SpatialPosition& get_relative_ref();
		void clean();
	private:
		template<class Archive>
		void save(Archive & ar, const unsigned int version) const
		{
			ar << BOOST_SERIALIZATION_BASE_OBJECT_NVP(ComponentBase);
			ar << BOOST_SERIALIZATION_NVP(mRelativeSpatialPosition);
		}

		template<class Archive>
		void load(Archive & ar, const unsigned int version)
		{
			ar >> BOOST_SERIALIZATION_BASE_OBJECT_NVP(ComponentBase);
			ar >> BOOST_SERIALIZATION_NVP(mRelativeSpatialPosition);
			mDirty = true;
		}
		template<class Archive>
		void serialize(Archive & ar,const unsigned int file_version )
		{
			boost::serialization::split_member(ar, *this, file_version);
		}
    };
}
BOOST_CLASS_EXPORT_KEY(RU::Transform)