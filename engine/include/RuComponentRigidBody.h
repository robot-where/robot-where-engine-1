#pragma once
#include "RuComponentBase.h" 
#include "RuPhysicsBase.h"
#include "RuPhysicsCollision.h"
namespace RU
{
    
    enum PhysicsRigidBodyTypes
    {
        RIGIDBODY_STATIC = 0,
        RIGIDBODY_KINEMATIC,
        RIGIDBODY_DYNAMIC
    };
    
	class WorldPhysics;
	class ComponentFakeRigidBody;
    class ComponentColliderBase;
	class ComponentRigidBody : public ComponentPhysicsBase
	{
		friend class ComponentFakeRigidBody;
		friend class boost::serialization::access;
		friend class WorldPhysics;
		float mass;
        PhysicsRigidBodyTypes mBodyType;
	private:
		void data_updated(); //this function updates boxbody in physics intermediary with componentrigidbody parameters
	public:
		ComponentRigidBody();
		~ComponentRigidBody();
		virtual ComponentInfoType get_component_info() const;
        static std::string name();
        virtual bool is_fake() const {return false;}
		bool is_visible() const {return true;}
        
		void attach_to_physics();
		void detach_from_physics();
		void derived_setup();     
        
        void set_body_type(PhysicsRigidBodyTypes aType);
        PhysicsRigidBodyTypes get_body_type();
		void set_mass(float aMass);
        float get_mass();
        
        bool has_spatial_position_changed(const SpatialPosition& aSp);

		rect get_render_AABB();
		void render(const ScreenSpatialPosition& position);
        
        //collision callback related
        void on_collision(ComponentColliderBase* A, ComponentColliderBase* B); //Collider A should always belong to this RigidBody
        boost::signal<void(CollisionInfo&)> mEventCollision; //NOTE, as a component event, this can be destroyed and the listeners will still be there. This should in general not be an issue.
	private:
		template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
			ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(ComponentPhysicsBase);
			ar & BOOST_SERIALIZATION_NVP(mass);
            ar & BOOST_SERIALIZATION_NVP(mBodyType);
        }
	};

	class ComponentFakeRigidBody : public ComponentRigidBody
	{
		friend class boost::serialization::access;
	public:
        bool is_fake() const {return true;}
		bool is_visible() const {return false;}
		bool do_serialize() const {return false;}
		void derived_setup(){ComponentRigidBody::derived_setup(); set_body_type(RIGIDBODY_STATIC);}
	private:
		template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
			ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(ComponentRigidBody);
        }
	};
}
BOOST_CLASS_EXPORT_KEY(RU::ComponentFakeRigidBody)
BOOST_CLASS_EXPORT_KEY(RU::ComponentRigidBody)