#pragma once
#include "RuComponentBase.h"
namespace RU
{
	
	class ComponentRenderableBase : public ComponentBase
	{
		friend class boost::serialization::access;
	public: 
		~ComponentRenderableBase();
		virtual ComponentInfoType get_component_info() const;
		virtual void derived_setup()
		{
			get_world_event_distributor().mRenderBroadPhaseContainer.add_screen_entity(this);
		}
		virtual void derived_cleanup()
		{
			get_world_event_distributor().mRenderBroadPhaseContainer.remove_screen_entity(this);
		}
		virtual rect get_render_AABB(){return rect(0,0,0,0);}
		virtual void render(const ScreenSpatialPosition& position){}
		//virtual void update_if_on_screen(const EngineTime&){} //TODO, one needs to do multiple passes in WorldScreenEventDistributor
	private:
		template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
			ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(ComponentBase);
        }
	};
}
BOOST_CLASS_EXPORT_KEY(RU::ComponentRenderableBase)