#pragma once
#include "RuTypesSerialization.h"
#include "RuComponentManager.h"
#include "RuTransform.h"
#include "UtilCRTreeBase.h"
#include "RuObjectReference.h"

namespace RU
{
 
    class EntityBase;
    class EntityTreeUtility
    {
        friend class EntityBase;
        friend class boost::serialization::access;
	public:
        typedef std::list<EntityReference> ChildContainerType;
	private:
        ChildContainerType mChildren; 
        EntityReference mParent;	
        EntityBase& mOwner;
    public:
        EntityTreeUtility(EntityBase& aOwner);
        EntityReference get_parent();
        void set_to_top_level();
        void add_child(EntityReference aChild);
        void remove_from_tree(); //NOTE, better not to call this function while iterating thought it's probably okay on account of the fact we're using a linked list
        unsigned get_number_children();
        ChildContainerType& get_children();
        template<typename F>
		void for_each_child(F f);
		//this recurses 
		template<typename F> 
		void for_every_child(F f);

    private:
        template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
            ar & BOOST_SERIALIZATION_NVP(mChildren);
            ar & BOOST_SERIALIZATION_NVP(mParent);
        }
    };
 
    class WorldEntityContainer;
	class Primary;
	class Transform;
    class EntityBase
    {
        friend class boost::serialization::access;
		friend class WorldEntityContainer;
		friend class Transform;
		weak_ptr<Primary> mPrimary;
		EntityReference mThis;
    protected:
        ComponentManager mComponentManager;
        Transform mTransform;
        ObjectIdType mEntityId;
		EntityTreeUtility mHierarchy;
		bool mDoSerialize;
        std::string mName;
    private:
		#pragma warning( push )
		#pragma warning( disable : 4355) //we need to set references back to parent object and this is the only way to do it
		EntityBase():mEntityId(NULL_OBJECT_ID),mComponentManager(*this),mHierarchy(*this),mName("__untitled__"),mDoSerialize(1){}
		EntityBase(std::string aName):mEntityId(NULL_OBJECT_ID),mComponentManager(*this),mHierarchy(*this),mName(aName){}
		#pragma warning( pop )
		//NOTE these functions are to called ONLY from the world entity container
		void setup_entity(ObjectIdType aId, weak_ptr<EntityBase> aThis,  weak_ptr<Primary> aPrimary);
        void setup_entity_contents();
		//we could in fact use the destructor, but since we need to abstract the constructor, we may as well do it for the destructor as well...
		void base_destroy()
		{
			mComponentManager.remove_all_components();
            mHierarchy.remove_from_tree();
		}
		void flag_transform_dirty(){mTransform.mDirty=true;}
        static void parent_hierarchy_changed(const EntityReference& aEntity)
        {
            aEntity->mHierarchy.for_each_child(EntityBase::parent_hierarchy_changed);
            //TODO trigger callbacks
            //TODO colliders need to listen here to see if parent rigid bodies were removed (only)
        }
        static void child_hierarchy_changed(const EntityReference& aEntity)
        {
            if(!aEntity->mHierarchy.mParent.is_empty())
                child_hierarchy_changed(aEntity->mHierarchy.mParent);
            //TODO trigger callbacks
            //TODO rigid body needs to listen here to see if new colliders were added
        }
    public:
		//------
		//core accessors
		//------
		Primary& get_primary(){return *mPrimary.lock();}
		EntityReference get_entity_reference() const {return mThis;}
        ObjectIdType get_entity_id(){return mEntityId;}
        ComponentManager& get_component_manager(){return mComponentManager;}
        Transform& get_transform(){return mTransform;}
		EntityTreeUtility& get_hierarchy(){return mHierarchy;} 
        
        
        //------
        //other accessors/modifiers
        //------
        void set_name(const std::string& aName){mName=aName;}
        std::string get_name(){return mName;}
        
        //------
        //hierarchy related
        //------
        bool is_top_level(){return mHierarchy.mParent.is_empty();}
        void print_hierarchy_recursive(int level = 0) const
        {
            static_print_hierarchy_recursive(get_entity_reference(),level);
        }
        static void static_print_hierarchy_recursive(const EntityReference& aRef, int level = 0)
        {
            for(int i = 0; i < level; i++)
                std::cout << "    ";
            std::cout << "Entity " << aRef->get_name() << " @ " << aRef->get_transform().get_absolute().position << std::endl;
            aRef->mHierarchy.for_each_child(boost::bind(&EntityBase::static_print_hierarchy_recursive,_1,level+1));
        }
		
        //------
        //serialization
        //------
        bool do_serialize() const {return mDoSerialize;}
		bool set_serialize(bool aSerialize){mDoSerialize=aSerialize;}
    private:        
        template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
            ar & BOOST_SERIALIZATION_NVP(mName);
            ar & BOOST_SERIALIZATION_NVP(mHierarchy);
            ar & BOOST_SERIALIZATION_NVP(mTransform);
            ar & BOOST_SERIALIZATION_NVP(mEntityId);
			ar & BOOST_SERIALIZATION_NVP(mDoSerialize);
            ar & BOOST_SERIALIZATION_NVP(mComponentManager);
            
            if (!Archive::is_saving::value) 
                std::cout << "Deserializing entity: " << mName << std::endl;
        }
    };


    //TODO this appears to not be working
	template<typename F>
	void EntityTreeUtility::for_each_child(F f)
	{
		std::for_each(mChildren.begin(),mChildren.end(),f);
	}
	//this recurses 
	template<typename F> 
	void EntityTreeUtility::for_every_child(F f)
	{
		for(typename ChildContainerType::iterator it = mChildren.begin(); it != mChildren.end(); it++)
		{
			f(it->get());
            //TODO
			//(*it)->for_every_child(f);
		}
	}
}

BOOST_CLASS_EXPORT_KEY(RU::EntityBase)