#pragma once
#include "RuTypes.h"
#include "RuMathTypes.h"
#include "RuObjectReference.h"
#include <boost/bimap.hpp>
#include <Box2D/Box2D.h>
#include <boost/signal.hpp>


namespace RU
{
	class ComponentRenderableBase;
	struct ScreenEntityIntermediary
	{
		EntityReference entity;
		ComponentRenderableBase* component; //TODO component reference???
		bool flagged_for_update;
		bool operator<(const ScreenEntityIntermediary& o) const;
		ScreenEntityIntermediary(ComponentRenderableBase* aComponent);
	};
    class BroadPhaseContainer
	{
	private:
        typedef ScreenEntityIntermediary VolumeType;
        typedef boost::bimap<int32,VolumeType> BimapType; //maps broadphase id to entity
	public:
		typedef std::vector<VolumeType> QueryReturnType;
	private:
        BimapType mProxyIdVolumeMap;
        b2BroadPhase mBroadPhase;
        QueryReturnType mReturnBuffer;
        b2AABB to_b2AABB(rect aRect)
        {
            b2AABB aabb;
            aabb.lowerBound = b2Vec2(aRect.x,aRect.y);
            aabb.upperBound = b2Vec2(aRect.x + aRect.w, aRect.y + aRect.h);
            return aabb;
        }
    public:
        //don't call me
		bool QueryCallback(boost::int32_t nodeid)
        {
            mReturnBuffer.push_back(mProxyIdVolumeMap.left.find(nodeid)->second);
            return true;
        }
	public:
        void add_screen_entity(ComponentRenderableBase* aComponent);
		void remove_screen_entity(ComponentRenderableBase* aComponent);
        void update_rect(rect aRect, VolumeType aVolume);
		void update_all_aabb();
        QueryReturnType& query(rect aRect);
    };

}