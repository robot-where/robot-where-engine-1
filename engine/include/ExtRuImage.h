#pragma once
#include "RuFileId.h"
#include "RuResourceBase.h"
#include "RuResourceManager.h"
#include "RuMathTypes.h"
namespace RU
{
    
    class ResourceImage : public ResourceBase
    {
		class __ImageData;
        friend class __ImageData;
        boost::scoped_ptr<__ImageData> data;
    public:
        ResourceImage();
        ResourceImage(const FileId& aFile);
        ~ResourceImage();

		bool is_loaded();
		bool has_FileId(){return true;}
		FileId get_FileId();

		void unload();

        void load_image(const FileId& aFile);
        void render_image(int x, int y); //clumsy rendering...
		void render_image(vec2 aPos, vec2 aScale, float angle);
        void render_image(mat4x4 aMat);
    };

	template<>
	inline shared_ptr<ResourceImage> ResourceManager::load_resource(const FileId& aFile)
	{
		shared_ptr<ResourceImage> r(new ResourceImage());
		r->load_image(aFile);
		return r;
	}
}