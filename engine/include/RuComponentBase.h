#pragma once
#include "RuTypesSerialization.h"
#include <string>
#include "RuTypes.h"
#include "RuMathTypes.h"
#include "RuWorldEventDistributor.h"
#include "RuEventConnectionTracker.h"
#include "RuObjectReference.h"




namespace RU
{
	class Transform;
	class Primary;
	class EntityBase;
	class ComponentManager;
	class WorldEntityContainer;
	class WorldPhysics;

    class ComponentBase
    {
		friend class boost::serialization::access;
		friend class EntityBase;
		friend class ComponentManager;
		//internal setup functions
		void base_setup(const EntityReference& aEntity)
		{
			mOwner = aEntity; 
			mEventConnectionTracker = shared_ptr<EventConnectionTracker>(new EventConnectionTracker());
		}
		void base_cleanup()
		{
			mEventConnectionTracker.reset();
		} 
		virtual void derived_setup(){}
		virtual void derived_cleanup(){} //NOTE there is no difference between this and on_destroy other than call order

		virtual bool do_serialize() const {return true;} //ComponentManager will not serialize if this retuns false (TODO)
		virtual bool is_visible() const {return true;} //Editor will not display these if this return false
	protected:
		EntityReference mOwner;
		//helpers
		shared_ptr<EventConnectionTracker> mEventConnectionTracker; //this induces uneeded overhead for eventless components but oh well
		Primary& get_primary();
		Transform& get_transform();
		WorldEventDistributor& get_world_event_distributor();
		WorldEntityContainer& get_world_entity_container();
		WorldPhysics& get_world_physics();
    public:
		ComponentBase(){}
		virtual ~ComponentBase();
		virtual ComponentInfoType get_component_info() const;
        static std::string name();
        EntityReference get_owner_ref(){return mOwner;}
		EntityBase& get_owner(){return *mOwner;}
        virtual void on_create(){}
        virtual void on_destroy(){}
        
        //---------------
        //Editor related
        //---------------
        //TODO figure out how you want to do this
        //I'm thinking it should hook up a type by string with get and set functors
        //note the editor class could then do the prefab checking thing
        //void expose_editor_variables(EditorClassType);
        

		
		//---------------
		//helper event connection routines
		//---------------
	protected:
		/*template<typename T>
		void connect_render(T* aComponent)
		{
			mEventConnectionTracker->add_connection(get_world_event_distributor().mEventRender.connect(boost::bind(&T::render,aComponent)));

		}*/
		template<typename T>
		void auto_connect_always_render (T* aComponent)
		{
			mEventConnectionTracker->add_connection(get_world_event_distributor().mEvent_always_render.connect(boost::bind(&T::always_render,aComponent,_1)));
		}
		template<typename T>
		void auto_connect_update (T* aComponent)
		{
			mEventConnectionTracker->add_connection(get_world_event_distributor().mEvent_update.connect(boost::bind(&T::update,aComponent,_1)));
		}

		//---------------
		//serialization
		//---------------
	private:
        template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
			//mEventConnectionTracker are set up in base_setup
			//mOwner is setup by Entity's setup routines so we need to do nothing here
        }
    };
}

BOOST_SERIALIZATION_ASSUME_ABSTRACT(RU::ComponentBase)
BOOST_CLASS_EXPORT_KEY(RU::ComponentBase)

