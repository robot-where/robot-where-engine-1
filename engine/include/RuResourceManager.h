#pragma once
#include "RuResourceBase.h"
#include "RuTypes.h"
#include <map>
namespace RU
{
	//keeps track of all FILE BASED resources
	class ResourceManager
	{
		typedef std::map<FileId,shared_ptr<ResourceBase> > ResourceMapType;
		ResourceMapType mResourceMap;
	public:
		template<typename T>
		shared_ptr<T> load_resource(const FileId& aFile);
		template<typename T>
		shared_ptr<T> get_loaded_resource(const FileId& aFile);
		void unload_resource(const FileId& aFile){mResourceMap[aFile]->unload();}
		void unload_all_resources()
		{
			for(ResourceMapType::iterator it = mResourceMap.begin(); it != mResourceMap.end(); it++)
				it->second->unload();
		}
	};

	/* this causes linker error with our template specialization.... how do we prevent this from happening?
	template<typename T>
	shared_ptr<T> load_resource(const FileId& aFile)
	{
		throw SimpleException("unknown resource type!");
	}*/

	template<typename T>
	shared_ptr<T> ResourceManager::get_loaded_resource(const FileId& aFile)
	{
		ResourceMapType::iterator it = mResourceMap.find(aFile);
		if(it == mResourceMap.end()) return shared_ptr<T>();
		return dynamic_pointer_cast<T>(it->second);
	}
}
