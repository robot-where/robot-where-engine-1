#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/core/func_common.hpp>
//use this for random stuff
//#include <glm/gtc/random.hpp>

#undef max

namespace RU
{
	//we put any routines that may have conflicting names in here
	namespace MATH
	{
		using glm::max;

	}

    using glm::vec2;
    using glm::vec3;
	using glm::vec4;
    using glm::quat;
	using glm::mat4x4;

	typedef vec3 color3;
	typedef vec4 color4;
    
    //x,y,z is in lower left back
    template<typename T>
    struct trect
    {
        T x,y,z;
        T w,h,d;
		trect():x(0),y(0),z(0),w(0),h(0),d(0){}
        trect(T ax, T ay, T az, T aw, T ah, T ad):x(ax),y(ay),z(az),w(aw),h(ah),d(ad){}
        trect(T ax, T ay, T aw, T ah):x(ax),y(ay),w(aw),h(ah),z(0),d(0){}
		void set_center(T ax, T ay, T az){x = ax-w/2; y = ay-h/2; z = az-d/2;}
		void set_center(T ax, T ay){set_center(ax,ay,z+d/2);}
		void set_center(const vec2& aCenter){set_center(aCenter.x,aCenter.y,z+d/2);}
		vec2 get_center_2d() const {return vec2(x+w/2.0f,y+h/2.0f);}
		bool contains_point(const vec3& aPoint)
		{
			return 
				(aPoint.x >= x) && (aPoint.x <= x+w) && 
				(aPoint.y >= y) && (aPoint.y <= y+h) && 
				(aPoint.z >= z) && (aPoint.z <= z+d);
		}
		bool contains_point(const vec2& aPoint){return contains_point(vec3(aPoint.x,aPoint.y,z));}
    };
    
    template<typename T>
    struct trect2
    {
        T x,y;
        T w,h;
		trect2():x(0),y(0),w(0),h(0){}
        trect2(T ax, T ay, T aw, T ah):x(ax),y(ay),w(aw),h(ah){}
		void set_center(T ax, T ay){x = ax-w/2; y = ay-h/2;}
		vec2 get_center() const {return vec2(x+w/2.0f,y+h/2.0f);}
		bool contains_point(const vec2& aPoint)
		{
			return 
            (aPoint.x >= x) && (aPoint.x <= x+w) && 
            (aPoint.y >= y) && (aPoint.y <= y+h);
		}
    };
    
    typedef trect<float> rect; //TODO rename this to rect3
    typedef trect2<float> rect2;
}


