#pragma once
#include "RuTypesSerialization.h"

#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/cstdint.hpp>
#include <boost/mem_fn.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/nil_generator.hpp>
#include <assert.h>




#define RU_ASSERT(x) std::assert(x)

namespace RU
{



    using boost::shared_ptr;
    using boost::weak_ptr;
    using boost::enable_shared_from_this;
	using boost::dynamic_pointer_cast;
	using boost::static_pointer_cast;

	using boost::serialization::null_deleter;

	typedef boost::uuids::uuid ObjectIdType;
	typedef int MouseIdType;
    typedef boost::uint32_t WorldIndexType;
	typedef boost::int32_t PhysicsIdType;
	typedef std::string ComponentInfoType;

	const ObjectIdType NULL_OBJECT_ID = boost::uuids::nil_uuid();
	const PhysicsIdType NULL_PHYSICS_ID = -1;
	typedef void (*VoidVoidFuncType)(void);
    
    //TODO move this to a diferent file yo
    template<typename T>
    bool is_weak_ptr_null(weak_ptr<T> wp)
    {
        weak_ptr<T> empty ;
        // operator < claims to establish a strict weak order, so this
        // should always work.
        return (!(wp < empty) && !(empty < wp)) ;
    }

}
