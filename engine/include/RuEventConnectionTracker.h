#pragma once
#include <boost/signal.hpp>
#include <list>

//TODO rename this file...
namespace RU
{
	class EventConnectionTracker
	{
		typedef std::list<boost::signals::connection> ConnectionContainerType;
		ConnectionContainerType mConnections;
	public:
		EventConnectionTracker(){}
		~EventConnectionTracker()
		{
			for(ConnectionContainerType::iterator it = mConnections.begin(); it != mConnections.end(); it++)
             it->disconnect();
		}
		void add_connection(boost::signals::connection aConnection) //is it okay to copy construct these?
		{
			mConnections.push_back(aConnection);
		}
		void remove_connection(boost::signals::connection aConnection) //is it okay to copy construct these?
		{
			//slow
			for(ConnectionContainerType::iterator it = mConnections.begin(); it != mConnections.end(); it++)
				if(*it == aConnection)
				{
					it->disconnect();
					mConnections.erase(it);
					break;
				}
		} 
	};
	
}