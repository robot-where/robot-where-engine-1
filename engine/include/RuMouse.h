#pragma once
#include "RuSpatialPosition.h"
#include "RuTypes.h"
#include <map>
namespace RU
{
	
	struct MousePosition
	{
		vec2 mScreenDim;
		vec2 mAbsolute;

		vec2 get_absolute(){return mAbsolute;}
		vec2 get_absolute_relative_to_center(){return mAbsolute-mScreenDim*0.5f;}
		vec2 get_relative(){return get_absolute()/mScreenDim;}
		vec2 get_relative_relative_to_center(){return get_absolute_relative_to_center()/mScreenDim;}
		SpatialPosition get_spatial_position()
		{
			return SpatialPosition(vec3(mAbsolute.x,mAbsolute.y,0));
		}
		SpatialPosition get_spatial_position(SpatialPosition aCameraSp)
		{
			return aCameraSp*get_spatial_position();
		}
	};

	class MouseButtonState
	{
		bool mFancy;
		MouseIdType mMouseId;
		MousePosition mStart;
		MousePosition mEnd;
		//TODO time variables
		//TODO fancy version w/ signal class
	public:
		MouseButtonState(MouseIdType aMouseId, bool aFancy = false):mMouseId(aMouseId),mFancy(aFancy)
		{
		}
	};

	class MultiMouseButtonState
	{
		typedef std::map<int,MouseButtonState> MouseContainerType; //TODO flatmap
		MouseContainerType mMice;
		//TODO
	};
}