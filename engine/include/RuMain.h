#pragma once
#include "RuTypes.h"
#include "ExtRu.h"
#include "RuStatus.h"
#include "RuMathTypes.h"
#include "RuKeys.h"
#include "UtilMemory.h"
#include "RuTime.h"
#include "RuWorldEntityContainer.h"
#include "RuWorldEventDistributor.h"
#include "RuResourceManager.h"
#include "RuPhysics.h"
#include "RuMouse.h"
#include <bitset>
namespace RU 
{
    struct EngineParameters
    {
        float targetFramerate;
        std::string windowName;
        EngineParameters()
        {
            targetFramerate = 1/30.0f;
        }
    };

    class Primary : public enable_shared_from_this<Primary>
    {
	public:
        friend shared_ptr<Primary> create_engine(const EngineParameters& aParam);
        friend OperationStatus run_engine(shared_ptr<Primary> aGame);
        friend class EngineRunner;
		friend int __main_helper();

        
        //-----
        //internal state vars
        //-----
        bool mQuit;
        EngineTime mEngineTime;
        std::bitset<KEYCODE_ENUM_SIZE> mKeyStates;
        std::bitset<KEYCODE_ENUM_SIZE> mKeyChangedThisFrame;
        vec2 mScreenSize;
        
        //-----
        //world state vars
        //-----
		ResourceManager mResourceManager;
        WorldEntityContainer mWorldEntityContainer;
        WorldEventDistributor mWorldEventDistributor;
		WorldPhysics mWorldPhysics;
        
    private:
        Primary(const EngineParameters& aParam);
        void initialize();

        //-----
        //sytem data modifiers
        //-----
        void set_screen_size(vec2);
    
        //-----
        //system callbacks
        //-----
        bool does_request_quit();
        void update(float aDeltaTime);
        void render();
		void destroy();
        
        //-----
        //key/mouse/touch related
        //-----
        void key_pressed(KeyCode aKey);
        void key_released(KeyCode aKey);
        
		void mouse_position_update(MouseIdType aId, MousePosition aMouse);
		void mouse_pressed(MouseIdType aId, MousePosition aMouse);
		void mouse_released(MouseIdType aId, MousePosition aMouse);
        
        
        
        
    public:
        ~Primary();
        //-----
        //key/mouse/touch accessors
        //-----
        bool was_key_pressed(KeyCode aKey);
        bool was_key_released(KeyCode aKey);
        bool get_key_state(KeyCode aKey);
        
        //-----
        //internal state accessors
        //-----
        const EngineTime& get_time();
        vec2 get_screen_size();
        
        //-----
        //world state accessors
        //-----
		ResourceManager& get_resource_manager();
        WorldEntityContainer& get_world_entity_container();
		WorldEventDistributor& get_world_event_distributor();
		WorldPhysics& get_world_physics();


		//TODO SERIALIZATION ROUTINES HERE
		//NOTE should run in their own thread maybe???
		//i.e. deserialization world entity container in its own thread
		//swap the two, and run startup functions (which sadly we can't run in its own thread)
    };
    
}