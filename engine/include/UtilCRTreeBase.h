#pragma once
#include <list>
#include <algorithm>
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/indexed_by.hpp>
#include <boost/multi_index/sequenced_index.hpp>
#include "RuTypes.h"
namespace RUTIL
{
	//this class owns no data
	template<typename T>
	class CRTreeBase
	{
	public:
		//this is a serializeable dropin for std::list... obviously
		//typedef boost::multi_index_container<T*,boost::multi_index::indexed_by<boost::multi_index::sequenced<> > > ChildContainerType;
        typedef std::list<T*> ChildContainerType;
	private:
		friend class boost::serialization::access;
		T* parent;
		ChildContainerType children;
		//typename ChildContainerType::iterator position;
	private:
		void disconnect_parent()
		{
			parent = NULL;
			//position = ChildContainerType::iterator();
		}
	public:
		CRTreeBase():parent(NULL)
		{
		}
		/*T* get_next() const
		{
			if(!parent) 
				return NULL;
			typename ChildContainerType::iterator r = position;
			if(++r != parent->children.end())
				return **r;
			return NULL;
		}*/
		T* get_parent() const {return parent;}
		void add_child(T* aChild)
		{
			aChild->position = children.insert(aChild);
			aChild->parent = this;
		}
		void add_child(RU::shared_ptr<T> aChild){add_child(aChild.get());}
		void add_child(const T& aChild){add_child(&aChild);}
		void destroy()//removes this node from the tree
		{
			if(parent)
				for(typename ChildContainerType::iterator it = children.begin(); it != children.end(); it++)
					parent->add_child(**it);
			else
				for(typename ChildContainerType::iterator it = children.begin(); it != children.end(); it++)
					it->disconnect_parent();
			children.clear();
		}
		ChildContainerType& get_children(){return children;}
		template<typename F>
		void for_each_child(F f)
		{
			std::for_each(children.begin(),children.end(),f);
		}
		//this recurses 
		template<typename F> 
		void for_every_child(F f)
		{
			for(typename ChildContainerType::iterator it = children.begin(); it != children.end(); it++)
			{
				f(*it);
				(*it)->for_every_child(f);
			}
		}
        //serialization
    private:
        template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
            ar & BOOST_SERIALIZATION_NVP(parent);
            ar & BOOST_SERIALIZATION_NVP(children);
			//ar & position;
        }
	};
}