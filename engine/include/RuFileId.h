#pragma once
#include <string>
#include <boost/filesystem.hpp>
namespace RU
{
    typedef boost::filesystem::path FileId;
	//TODO stuff for getting a so called "root directory" identifier

	inline bool is_file_null(FileId aFile)
	{
		return aFile == boost::filesystem::path();
	}
}