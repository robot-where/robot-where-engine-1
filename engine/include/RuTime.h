#pragma once
namespace RU
{
    //contains time statistics for the negine
    class Primary;
    class EngineTime
    {
        typedef float PrecType;
        friend class Primary;    
        
        PrecType mTargetDeltaTime;        
        PrecType mLastDeltaTime;
        PrecType mTotalTime;
        unsigned mTotalFrames;
    private:
        EngineTime(PrecType aTargetDeltaTime):mTargetDeltaTime(aTargetDeltaTime),mLastDeltaTime(0),mTotalTime(0),mTotalFrames(0){}
        void advance_frame(PrecType aDeltaTime){mLastDeltaTime = aDeltaTime; mTotalFrames++; mTotalTime+=aDeltaTime;}
    public:
        PrecType get_last_delta_time() const {return mLastDeltaTime;}
        PrecType get_target_delta_time() const {return mTargetDeltaTime;}
        PrecType get_total_time() const {return mTotalTime;}
        unsigned get_total_frames() const {return mTotalFrames;}
    };
}