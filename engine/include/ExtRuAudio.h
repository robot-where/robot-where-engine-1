#pragma once
#include "RuFileId.h"
#include "RuResourceBase.h"
#include "RuResourceManager.h"
#include "RuMathTypes.h"
namespace RU
{
    
    class ResourceAudio : public ResourceBase
    {
		class __AudioData
        friend class __AudioData
        boost::scoped_ptr<__AUdioData> data;
    public:
        ResourceAudio();
        ResourceAudio(const FileId& aFile);
        ResourceAudio();
        
		bool is_loaded();
		bool has_FileId(){return true;}
		FileId get_FileId();
        
		void unload();
        
        void load_sound(const FileId& aFile);
    };
    
	template<>
	inline shared_ptr<ResourceAudio> ResourceManager::load_resource(const FileId& aFile)
	{
		shared_ptr<ResourceAudio> r(new ResourceImage());
		r->load_audio(aFile);
		return r;
	}
}