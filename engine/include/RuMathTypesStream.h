#pragma once
#include "RuMathTypes.h"
#include <ostream>
inline std::ostream& operator<< (std::ostream &out, const RU::mat4x4& mat)
{
	for(int i = 0; i < 4; i++)
	{
		for(int j = 0; j < 4; j++)
			out << mat[i][j];
		out << "\n";
	}
	return out;
}
inline std::ostream& operator<< (std::ostream &out, const RU::vec2& vec)
{
	out << vec.x << " " << vec.y;
	return out;
}
inline std::ostream& operator<< (std::ostream &out, const RU::vec3& vec)
{
	out << vec.x << " " << vec.y << " " << vec.z;
	return out;
}
inline std::ostream& operator<< (std::ostream &out, const RU::quat& vec)
{
	out << vec.x << " " << vec.y << " " << vec.z << " " << vec.w;
	return out;
}