#pragma once
#include "RuMathTypes.h"
#include <glm/gtx/norm.hpp>
namespace RU
{
    namespace MATH
    {
        /*
        
        
        //obviously not written by me...
        template <typename T> 
        T matrix_determinant(const glm::detail::tmat4x4<T>& mat) const {
#define MINOR(m, r0, r1, r2, c0, c1, c2) \
((m).rows[r0][c0] * ((m).rows[r1][c1] * (m).rows[r2][c2] - (m).rows[r2][c1] * (m).rows[r1][c2]) - \
(m).rows[r0][c1] * ((m).rows[r1][c0] * (m).rows[r2][c2] - (m).rows[r2][c0] * (m).rows[r1][c2]) + \
(m).rows[r0][c2] * ((m).rows[r1][c0] * (m).rows[r2][c1] - (m).rows[r2][c0] * (m).rows[r1][c1]))
            
            return this->rows[0][0] * MINOR(*this, 1, 2, 3, 1, 2, 3) -
            this->rows[0][1] * MINOR(*this, 1, 2, 3, 0, 2, 3) +
            this->rows[0][2] * MINOR(*this, 1, 2, 3, 0, 1, 3) -
            this->rows[0][3] * MINOR(*this, 1, 2, 3, 0, 1, 2);
            
#undef MINOR
        }
        
        template <typename T> 
        bool matrix_is_affine(const glm::detail::tmat4x4<T>& mat) const {
            // First make sure the bottom row meets the condition that it is (0, 0, 0, 1)
            if (!Vector4D<T>::Equals(this->GetRow(3), Vector4D<T>(0.0, 0.0, 0.0, 1.0))) {
                return false;
            }
            
            // Get the inverse of this matrix:
            // Make sure the matrix is invertible to begin with...
            if (fabs(this->Determinant()) <= get_error_epsilon<T>()) {
                return false;
            }
            
            // Calculate the inverse and seperate the inverse translation component 
            // and the top 3x3 part of the inverse matrix
            Matrix4x4<T> inv4x4Matrix = Matrix4x4<T>::Inverse(*this);
            Vector3D<T> inv4x4Translation(inv4x4Matrix.rows[0][3],
                                          inv4x4Matrix.rows[1][3],
                                          inv4x4Matrix.rows[2][3]);
            Matrix3x3<T> inv4x4Top3x3 = Matrix4x4<T>::ToMatrix3x3(inv4x4Matrix);
            
            // Grab just the top 3x3 matrix
            Matrix3x3<T> top3x3Matrix     = Matrix4x4<T>::ToMatrix3x3(*this);
            Matrix3x3<T> invTop3x3Matrix  = Matrix3x3<T>::Inverse(top3x3Matrix);
            Vector3D<T> inv3x3Translation = -(invTop3x3Matrix * this->GetTranslation());
            
            // Make sure we adhere to the conditions of a 4x4 invertible affine transform matrix
            if (!Matrix3x3<T>::Equals(inv4x4Top3x3, invTop3x3Matrix)) {
                return false;
            }
            if (!Vector3D<T>::Equals(inv4x4Translation, inv3x3Translation)) {
                return false;
            }
            
            return true;
        }
        
        template <typename T> 
        void Decompose(const glm::detail::tmat4x4<T>& m,  //input
                                     glm::detail::tvec3& translation, //output
                                     glm::detail::tmat4x4<T>& rotation, 
                                    glm::detail::tvec3<T>& scale) {
            // Copy the matrix first - we'll use this to break down each component
            Matrix4x4<T> mCopy(m);
            
            // Start by extracting the translation (and/or any projection) from the given matrix
            translation = mCopy.GetTranslation();
            for (int i = 0; i < 3; i++) {
                mCopy.rows[i][3] = mCopy.rows[3][i] = 0.0;
            }
            mCopy.rows[3][3] = 1.0;
            
            // Extract the rotation component - this is done using polar decompostion, where
            // we successively average the matrix with its inverse transpose until there is
            // no/a very small difference between successive averages
            T norm;
            int count = 0;
            rotation = mCopy;
            do {
                Matrix4x4<T> nextRotation;
                Matrix4x4<T> currInvTranspose = 
                Matrix4x4<T>::Inverse(Matrix4x4<T>::Transpose(rotation));
                
                // Go through every component in the matrices and find the next matrix
                for (int i = 0; i < 4; i++) {
                    for (int j = 0; j < 4; j++) {
                        nextRotation.rows[i][j] = static_cast<T>(0.5 * 
                                                                 (rotation.rows[i][j] + currInvTranspose.rows[i][j]));
                    }
                }
                
                norm = 0.0;
                for (int i = 0; i < 3; i++) {
                    float n = static_cast<float>(
                                                 fabs(rotation.rows[i][0] - nextRotation.rows[i][0]) +
                                                 fabs(rotation.rows[i][1] - nextRotation.rows[i][1]) +
                                                 fabs(rotation.rows[i][2] - nextRotation.rows[i][2]));
                    norm = std::max<T>(norm, n);
                }
                rotation = nextRotation;
            } while (count < 100 && norm > blackbox::bbmath::get_error_epsilon<T>());
            
            // The scale is simply the removal of the rotation from the non-translated matrix
            Matrix4x4<T> scaleMatrix = Matrix4x4<T>::Inverse(rotation) * mCopy;
            scale = Vector3D<T>(scaleMatrix.rows[0][0],
                                scaleMatrix.rows[1][1],
                                scaleMatrix.rows[2][2]);
            
            // Calculate the normalized rotation matrix and take its determinant to determine whether
            // it had a negative scale or not...
            Vector3D<T> row1(mCopy.rows[0][0], mCopy.rows[0][1], mCopy.rows[0][2]);
            Vector3D<T> row2(mCopy.rows[1][0], mCopy.rows[1][1], mCopy.rows[1][2]);
            Vector3D<T> row3(mCopy.rows[2][0], mCopy.rows[2][1], mCopy.rows[2][2]);
            row1.Normalize();
            row2.Normalize();
            row3.Normalize();
            Matrix3x3<T> nRotation(row1, row2, row3);
            
            // Special consideration: if there's a single negative scale 
            // (all other combinations of negative scales will
            // be part of the rotation matrix), the determinant of the 
            // normalized rotation matrix will be < 0. 
            // If this is the case we apply an arbitrary negative to one 
            // of the component of the scale.
            T determinant = nRotation.Determinant();
            if (determinant < 0.0) {
                scale.SetX(scale.GetX() * -1.0);
            }
        }
        */
    }
}