#pragma once
#include "RuComponentBase.h"
#include "RuPhysicsBase.h"
#include "ExtRuGL.h"
#include "RuPhysicsCollision.h"
namespace RU
{
    class ComponentRigidBody;
	class ComponentColliderBase : public ComponentPhysicsBase
	{
		friend class boost::serialization::access;
        //do NOT serialize
        SpatialPosition mLastRelativeSpatialPosition;
	public:
        ComponentColliderBase(){}
		virtual ~ComponentColliderBase();
		ComponentInfoType get_component_info() const;
		void attach_to_physics();
		void detach_from_physics();
        void set_last_relative_spatial_position(const SpatialPosition& aSp); //this is relative to the attached rigidbody's transform
        virtual bool has_spatial_position_changed(const SpatialPosition& aSp);
		virtual b2Shape* get_box_shape()=0;
        virtual float get_volume();
		static std::string name(){return "ComponentColliderBase";}
        
        //collision callback related
        void on_collision(ComponentColliderBase* other);
        boost::signal<void(CollisionInfo&)> mEventCollision; //NOTE, as a component event, this can be destroyed and the listeners will still be there. This should in general not be an issue.
        
    protected:
        ComponentRigidBody* get_attached_rigid_body();
	private:
		template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
			ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(ComponentPhysicsBase);
        }
	};

	
	class ComponentColliderSphere : public ComponentColliderBase
	{
		friend class boost::serialization::access;
		b2CircleShape mShape;
		vec2 mCenter;
		vec2 mAbsCenter;
		float mRadius;
		float mAbsRadius;
	public:
        ComponentColliderSphere();
		~ComponentColliderSphere();
		ComponentInfoType get_component_info() const;
		void set_radius(float aRadius);
		void set_center(vec2 aCenter);
        void derived_setup();
		void render(const ScreenSpatialPosition& position);
		static std::string name(){return "ComponentColliderSphere";}
		void compute_positions();
		virtual b2Shape* get_box_shape();
	private:
		template<class Archive>
		void serialize(Archive & ar, const unsigned int version)
		{
			ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(ComponentColliderBase);
			ar & BOOST_SERIALIZATION_NVP(mCenter);
			ar & BOOST_SERIALIZATION_NVP(mRadius);
		}
	};

	class ComponentColliderBox : public ComponentColliderBase
	{
		friend class boost::serialization::access;
		b2PolygonShape mShape;
		rect mRect;
		rect mAbsRect;
		float mRot;
		float mAbsRot;
	public:
        ComponentColliderBox();
		~ComponentColliderBox();
		ComponentInfoType get_component_info() const;
        void derived_setup();
		void render(const ScreenSpatialPosition& position);
		void compute_positions();
		void set_size(vec2 aSize);
		void set_position(vec2 aPos);
		static std::string name(){return "ComponentColliderBox";}
		virtual b2Shape* get_box_shape();
	private:
		template<class Archive>
		void serialize(Archive & ar, const unsigned int version)
		{
			ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(ComponentColliderBase);
			ar & BOOST_SERIALIZATION_NVP(mRect);
			ar & BOOST_SERIALIZATION_NVP(mRot);
		}
	};

	class ComponentColliderPoly : public ComponentColliderBase
	{
		//TODO should store vertictes internally so we don't need to serialize b2PolygonShape (though we could do that if we wanted to)
		friend class boost::serialization::access;
		b2PolygonShape mShape;
		typedef std::vector<vec2> PolygonPositionArrayType;
		PolygonPositionArrayType mPolygonPositions;
		PolygonPositionArrayType mAbsolutePolygonPositions;
	public:
        ComponentColliderPoly();
		~ComponentColliderPoly();
		ComponentInfoType get_component_info() const;
		void set_positions(PolygonPositionArrayType aPositions);
        void derived_setup();
		void render(const ScreenSpatialPosition& position);
		void compute_positions();
		static std::string name(){return "ComponentColliderPoly";}
		virtual b2Shape* get_box_shape();
	private:
		template<class Archive>
		void serialize(Archive & ar, const unsigned int version)
		{
			ar &  BOOST_SERIALIZATION_BASE_OBJECT_NVP(ComponentColliderBase);
			//TODO
		}
	};

	class ComponentColliderChain : public ComponentColliderBase
	{
		friend class boost::serialization::access;
		b2ChainShape mShape;
		typedef std::vector<vec2> ChainPositionArrayType;
		ChainPositionArrayType mChainPositions;
		ChainPositionArrayType mAbsoluteChainPositions;
	public:
        ComponentColliderChain();
		~ComponentColliderChain();
		ComponentInfoType get_component_info() const;
		void set_positions(ChainPositionArrayType aChain);
		void set_positions_loop(ChainPositionArrayType aChain);
        void derived_setup();
		void render(const ScreenSpatialPosition& position);
		void compute_positions();
		static std::string name(){return "ComponentColliderPoly";}
		virtual b2Shape* get_box_shape();
	private:
		template<class Archive>
		void serialize(Archive & ar, const unsigned int version)
		{
			ar &  BOOST_SERIALIZATION_BASE_OBJECT_NVP(ComponentColliderBase);
			//TODO
		}
	};
}

BOOST_CLASS_EXPORT_KEY(RU::ComponentColliderBase)
BOOST_CLASS_EXPORT_KEY(RU::ComponentColliderSphere)
BOOST_CLASS_EXPORT_KEY(RU::ComponentColliderBox)
BOOST_CLASS_EXPORT_KEY(RU::ComponentColliderPoly)
BOOST_CLASS_EXPORT_KEY(RU::ComponentColliderChain)
