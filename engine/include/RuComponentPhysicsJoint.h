#pragma once
#include "RuPhysicsBase.h"
#include "RuComponentRigidBody.h"
namespace RU
{
	class ComponentJointBase : public ComponentPhysicsBase
	{
        bool mCollideConnected;
        ComponentRigidBody* mAttachedRigidBody; //TODO this should be entity reference
        ComponentRigidBody* mTargetRigidBody; 

    public:
        ComponentJointBase();
        void attach_to_physics();
		void detach_from_physics();
        void derived_setup();
        ComponentRigidBody* get_other(ComponentRigidBody* rb);
        void set_attached_rigid_body(EntityReference o);
    };
}