#pragma once
#include <boost/exception/exception.hpp>
#include <boost/throw_exception.hpp>
#include <string>
#include <exception>
namespace RU
{
    //TODO should inherit from std::exception or boost exception bah
    class SimpleException
    {
        std::string mMsg;
    public:
        explicit SimpleException(std::string aMsg) throw() :mMsg(aMsg){}
        SimpleException() throw() {}
        const char* what() const throw() { return mMsg.c_str(); }
    };
}