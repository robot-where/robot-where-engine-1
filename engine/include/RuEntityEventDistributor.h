#pragma once
#include <boost/signal.hpp>
#include "RuObjectReference.h"
namespace RU
{
	struct EntityEventDistributor
	{
		boost::signal<void(EntityReference&)> mEvent_destroy;
	};
}