#pragma once
#include "RuComponentRenderableBase.h" 
#include "ExtRuImage.h"
#include "RuTypesSerialization.h"
namespace RU
{
    class ComponentRenderImage : public ComponentRenderableBase
	{
		friend class boost::serialization::access;
		ResourceImage mImage;
		FileId mFileId;

		void derived_setup();
		void derived_cleanup();
		void load_image_internal()
		{
			std::cout << "loading image " << mFileId << std::endl;
			mImage.load_image(mFileId);
		}
	public:
		static std::string name(){return "ComponentRenderImage";}
		void render(const ScreenSpatialPosition& position)
		{
		}
		void always_render(const ScreenSpatialPosition& position)
		{ 
			mImage.render_image(0,0);
		}
		void set_image(const FileId& aFileId)
		{
			mFileId = aFileId;
			load_image_internal();
		}
	private:
		template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
			ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(ComponentRenderableBase);
			ar & BOOST_SERIALIZATION_NVP(mFileId);
			//mImage is set in derived_setup
        }
	};
}

BOOST_CLASS_EXPORT_KEY(RU::ComponentRenderImage);