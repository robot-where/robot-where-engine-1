#pragma once
#include <boost/signal.hpp>
#include <Box2D/Box2D.h>
#include "RuMathTypes.h"
#include "RuSpatialPosition.h"
#include "RuTime.h"
#include "RuWorldScreenEventDistributor.h"


namespace RU
{

    struct WorldEventDistributor
    {
		struct SignalStruct //TODO what is this??? delete me
		{
			boost::signals::connection render;
			boost::signals::connection update;
		};

		BroadPhaseContainer mRenderBroadPhaseContainer;
		//this is clumsy, we may as well store the signal directly in the broadphasecontainer....

        boost::signal<void(const ScreenSpatialPosition&)> mEvent_always_render;
		boost::signal<void(const EngineTime&)> mEvent_update;
    };

#define SIGNAL_CONNECT_0(signal_name,signal_type,function_name) template<typename T> \
	boost::signals::connection connect_##signal_name(boost::signal<signal_type>& aSignal, T& aConnecting) \
	{ return aSignal.connect(boost::bind(&T::function_name,aConnecting)); }

	SIGNAL_CONNECT_0(mEvent_always_render, void(), always_render)

}