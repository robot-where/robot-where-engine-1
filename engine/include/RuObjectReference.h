#pragma once
#include "RuTypesSerialization.h"
#include "RuTypes.h"
#include <boost/uuid/uuid_generators.hpp>
namespace RU
{
	inline ObjectIdType get_new_object_id()
	{
		boost::uuids::random_generator gen;
		return gen();
	}

	class ReferenceBase
	{
		friend class boost::serialization::access;
	protected:
		ObjectIdType mId;
	public:
		ReferenceBase():mId(boost::uuids::nil_uuid()){}
		bool is_empty() const {return mId == boost::uuids::nil_uuid();}
		bool operator<(const ReferenceBase& o) const
		{
			return this->mId < o.mId;
		}
	private:
		template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
			ar & BOOST_SERIALIZATION_NVP(mId);
        }
	};

	class EntityBase;
	class WorldEntityContainer;
	class EntityReference : public ReferenceBase
	{
        static EntityBase* const DESERIALIZING_FLAG_VALUE;
		friend class boost::serialization::access;
		weak_ptr<EntityBase> mEntity;
        EntityBase* mEntityRaw;
    private:
        bool is_deserializing(); //used only by deserialization to update pointer in WEC when copy constructor is called
	public:
        EntityReference();
        EntityReference(const EntityReference& o);
        void lookup(const WorldEntityContainer& aContainer);
		weak_ptr<EntityBase> get_safe() const;
		EntityBase* get() const;
		void set(EntityBase* aEntity);
        void set(weak_ptr<EntityBase> aEntity);
        void set(const ObjectIdType& aId);
		EntityBase* operator->() const;
		EntityBase& operator*() const;
        bool operator==(const EntityReference& o) const;
        bool operator!=(const EntityReference& o) const;
		bool operator<(const EntityReference& o) const
		{
			return *((ReferenceBase*)this) < (ReferenceBase&)o;
		}
		bool is_set() const;   //checks if mEntity and mEntityRaw have been set, note, returns false if this is empty
		bool is_valid() const; //checks if entity still exists, this can throw
	private:
		//since we can't inline this due to cross referenec issue
		void register_this_in_world_entity_container();
		template<class Archive>
		void save(Archive & ar, const unsigned int version) const
		{
			ar << BOOST_SERIALIZATION_BASE_OBJECT_NVP(ReferenceBase);
		}

		template<class Archive>
		void load(Archive & ar, const unsigned int version)
		{ 
            ar >> BOOST_SERIALIZATION_BASE_OBJECT_NVP(ReferenceBase);
            if(!is_empty())
                register_this_in_world_entity_container();
		}
		template<class Archive>
		void serialize(Archive & ar,const unsigned int file_version )
		{
			boost::serialization::split_member(ar, *this, file_version);
		}
	};

	//does not derive from reference base since components do not have UUIDs 
	//we might eventually derive this from referencebase if we want to give components UUIDs (i.e. for multiple components)	
    /*
	template<typename T>
	class ComponentReference
	{
		friend class boost::serialization::access;
		EntityReference mEntity;
		weak_ptr<T> mComponent;
		T* mComponentRaw;
		//TODO component identifier
	public:
        ComponentReference():mComponentRaw(NULL)
        {            
        }
        ComponentReference(EntityReference aEntity):mComponentRaw(NULL)
        {            
            set(aEntity);
        }

		void lookup(const WorldEntityContainer& aContainer)
		{
			//TODO
		}
		const EntityReference& get_parent() const
		{return mEntity;}
		weak_ptr<T> get_safe() const
		{return mComponent.lock().get();}
		T* get() const
		{return mComponentRaw;}
        void set(EntityReference aEntity)
        {
            mEntity = aEntity;
            mComponent = mEntity->get_component_manager().get_component<T>();
            if(mComponent.lock())
                mComponentRaw = mComponent.lock().get();
        }
		T* operator->() const
		{return get();}
		T& operator*() const
		{return *get_safe();}
	private:
		template<class Archive>
		void serialize(Archive & ar,const unsigned int file_version )
		{
			ar & BOOST_SERIALIZATION_NVP(mEntity);
		}
	};*/
}

BOOST_SERIALIZATION_ASSUME_ABSTRACT(RU::ComponentReferenceBase);
BOOST_CLASS_EXPORT_KEY(RU::EntityReference);