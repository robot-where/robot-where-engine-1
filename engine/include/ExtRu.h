#pragma once
#include "RuStatus.h"
#include "RuTypes.h"
namespace RU
{
    class EngineRunner;
	class Primary;
	struct EngineParameters;
    shared_ptr<Primary> create_engine(const EngineParameters& aParam);
    OperationStatus run_engine(shared_ptr<Primary> aGame);
	int __main_helper(); //has access to Primary private members, also lives in RU namespace
}