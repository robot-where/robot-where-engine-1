#pragma once
#include "RuFileId.h"
#include "RuTypes.h"
namespace RU
{
	class ResourceBase
	{
	public:
        virtual ~ResourceBase(){}
		virtual bool is_loaded(){return false;}
		virtual bool is_ready(){return is_loaded();}

		virtual bool has_FileId(){return false;} //this means this resource comes from a file... probbaly always true???
		virtual FileId get_FileId(){return FileId();}

		virtual bool can_duplicate(){return false;}
		virtual shared_ptr<ResourceBase> duplicate(){return shared_ptr<ResourceBase>();}
		template<typename T> 
		shared_ptr<T> duplicate_typed(){return shared_ptr<T>(duplicate());}

		virtual void unload(){} //TODO maybe give this a return type so we can check for errors...
	};
}