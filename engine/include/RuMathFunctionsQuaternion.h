#pragma once
namespace RU
{
    namespace MATH
    {
#define LOCALPI 3.1415926535897932384626433832795028841971693993751058209749445923078164062

        inline float to_radians(const float& aDeg){return aDeg*float(LOCALPI)/float(180);}
        inline float to_degrees(const float& aRad){return aRad*float(180)/float(LOCALPI);}
        inline vec2 rotate_vec2(const vec2& aVec, float aRad)
        {
            float cs = glm::cos(aRad);
            float sn = glm::sin(aRad);
            return vec2(aVec.x*cs - aVec.y*sn, aVec.x*sn + aVec.y*cs);
        }
        inline vec2 rotate_vec2(const vec2& aVec,const quat& aQuat){return rotate_vec2(aVec,to_radians(glm::eulerAngles(aQuat).z));}
        
#undef LOCALPI
    }
}