#pragma once
namespace RUTIL
{
 
    //TODO rewrite me...
    template<typename T>
    struct Timer
    {
        Timer(T aTarget = 0,T aStart = 0):mStart(aStart),mCurrent(aStart),mTarget(aTarget){}
        Timer() {}
        void update(T amnt = 1)
        {
            mCurrent += amnt;
        }
        void reset()
        {
            mCurrent = mStart;
        }
        
        bool isSet()
        {
            return mTarget != mStart;
        }
        Timer& operator++(int unused)
        {
            update();
            return *this;
        }
        Timer& operator--(int unused)
        {
			mCurrent-=1;
            return *this;
        }
        void setTarget(T aTarget)
        {
            mTarget = aTarget;
        }
        void setTargetAndReset(T aTarget)
        {
            setTarget(aTarget);
            reset();
        }
        void expire()
        {
            mCurrent = mTarget;
        }
        void clamp()
        {
            if(isExpired())
                expire();
            if(getTimeSinceStart() < 0)
                reset();
        }
        //timer expires when mCurrent is >= mTarget
        bool isExpired()
        {
            return mCurrent >= mTarget;
        }
        T getTimeSinceStart()
        {
            return mCurrent-mStart;
        }
        T getTimeUntilEnd()
        {
            return mTarget-mCurrent;
        }
        T getTimeTotal()
        {
            return mTarget-mStart;
        }
        float getLinear()
        {
            if(isExpired())
                return 1;
            return (float)(mCurrent-mStart)/(float)(mTarget-mStart);
        }
        float getLinear(float l, float r)
        {
            return getLinear()*(r-l)+l;
        }
        float getSquareRoot()
        {
            return sqrt(getLinear());
        }
        float getSquare()
        {
            float r = getLinear();
            return r*r;
        }
        float getCubed(float l = 0, float r = 1)
        {
            float v = getLinear();
            return v*v*v*(r-l)+l;
        }
        //0 and 0 and 1, max at 0.5
        float getUpsidedownParabola()
        {
            float x = getLinear();
            return (-(x-0.5)*(x-0.5) + 0.25)*4;
        }
        //1 at 0 and 1, 0 at 0.5
        float getParabola(float l = 0, float r = 1)
        {
            return (1-getUpsidedownParabola())*(r-l)+l;
        }
    protected:
        T mStart,mCurrent,mTarget;
    };
    
}