#include "RuComponentBase.h"
#include "RuEntityBase.h"
#include "RuObjectReference.h"
#include "RuMain.h"
namespace RU
{

	ComponentBase::~ComponentBase()
	{
	}
	ComponentInfoType ComponentBase::get_component_info() const
	{
		return "ComponentBase";
	}
	Primary& ComponentBase::get_primary()
	{
		return mOwner->get_primary();
	}
	Transform& ComponentBase::get_transform()
	{
		return mOwner->get_transform();
	}
	WorldEventDistributor& ComponentBase::get_world_event_distributor()
	{
		return mOwner->get_primary().get_world_event_distributor();
	}
	WorldEntityContainer& ComponentBase::get_world_entity_container()
	{
		return mOwner->get_primary().get_world_entity_container();
	}
	WorldPhysics& ComponentBase::get_world_physics()
	{
		return mOwner->get_primary().get_world_physics();
	}
}
BOOST_CLASS_EXPORT_IMPLEMENT(RU::ComponentBase); //TODO do I need to do this or not??
