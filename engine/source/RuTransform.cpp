#include "RuTransform.h"
#include "RuEntityBase.h"
namespace RU
{
	std::string Transform::name()
	{
		return "Transform";
	}
	SpatialPosition Transform::get_absolute()
	{
		if(mDirty) clean();
		return mAbsoluteSpatialPosition;
	}
    SpatialPosition& Transform::get_relative_ref()
    {
        flag_children_dirty();
        return mRelativeSpatialPosition;
    }
	SpatialPosition Transform::get_relative() const
	{
		return mRelativeSpatialPosition;
	}
	void Transform::set_absolute(const SpatialPosition& aSP)
	{
		mAbsoluteSpatialPosition = aSP;
		EntityReference parent = get_owner().get_hierarchy().get_parent();
		if(!parent.is_empty())
			mRelativeSpatialPosition = mAbsoluteSpatialPosition/parent->get_transform().get_absolute();
        else mRelativeSpatialPosition = mAbsoluteSpatialPosition;
		flag_children_dirty();
	}
	void Transform::set_relative(const SpatialPosition& aSP)
	{
        mDirty = true;
		flag_children_dirty();
		mRelativeSpatialPosition = aSP;
	}
	void Transform::flag_children_dirty()
	{
		mOwner->get_hierarchy().for_every_child(boost::mem_fn(&EntityBase::flag_transform_dirty));
	}
	void Transform::clean()
	{
		shared_ptr<EntityBase> operating = mOwner->get_hierarchy().get_parent().get_safe().lock();
		if(operating && operating.get()->mTransform.mDirty)
		{
			operating->mTransform.clean();
			mAbsoluteSpatialPosition = mRelativeSpatialPosition*operating->mTransform.get_absolute();
		}
        else if(!operating)
            mAbsoluteSpatialPosition = mRelativeSpatialPosition;
	}
}
BOOST_CLASS_EXPORT_IMPLEMENT(RU::Transform);