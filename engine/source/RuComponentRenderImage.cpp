#include "RuComponentRenderImage.h"
#include "RuMain.h"
#include <boost/bind.hpp>
namespace RU
{
	void ComponentRenderImage::derived_setup()
	{
		ComponentRenderableBase::derived_setup();
		auto_connect_always_render(this);
		//other setup
		if(mFileId != FileId())
			load_image_internal();
	}
	void ComponentRenderImage::derived_cleanup()
	{
		ComponentRenderableBase::derived_cleanup();
		//let resource manager know we are done with the image
	}
}
BOOST_CLASS_EXPORT_IMPLEMENT(RU::ComponentRenderImage);