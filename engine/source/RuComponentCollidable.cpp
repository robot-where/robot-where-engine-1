#include "RuComponentCollidable.h"
#include "RuEntityBase.h"
#include "RuPhysics.h"
#include "RuMathFunctionsQuaternion.h"
#include "RuMathFunctionsVector.h"
namespace RU
{
	ComponentColliderBase::~ComponentColliderBase()
	{
	}
	ComponentInfoType ComponentColliderBase::get_component_info() const
	{
		return "ComponentColliderBase";
	}
	void ComponentColliderBase::attach_to_physics()
	{
		get_world_physics().add_component(this);
	}
	void ComponentColliderBase::detach_from_physics()
	{
		get_world_physics().remove_component(this);
	}
    void ComponentColliderBase::set_last_relative_spatial_position(const SpatialPosition& aSp)
    {
        mLastRelativeSpatialPosition = aSp;
    }
    float ComponentColliderBase::get_volume()
    {
        b2MassData output;
        get_box_shape()->ComputeMass(&output,1);
        return output.mass;
    }
    ComponentRigidBody* ComponentColliderBase::get_attached_rigid_body()
    {
        return get_intermediary()->mRigidBody;
    }
    bool ComponentColliderBase::has_spatial_position_changed(const SpatialPosition& aSp)
    {
        if(is_dirty())
            return true;
        SpatialPosition spNew = (aSp/get_transform().get_absolute());
        bool r = spNew != mLastRelativeSpatialPosition;
        return r;
    }
    void ComponentColliderBase::on_collision(ComponentColliderBase* other)
    {
        CollisionInfo info;
        info.c1 = this;
        info.c2 = other;
        mEventCollision(info);
    }
    ComponentColliderSphere::ComponentColliderSphere()
    {
        mRadius = 0.5f;
    }
	ComponentColliderSphere::~ComponentColliderSphere()
	{
	}
	ComponentInfoType ComponentColliderSphere::get_component_info() const
	{
		return "ComponentColliderSphere";
	}
    void ComponentColliderSphere::set_radius(float aRadius) //TODO Decide if this is relative to world scale or local scale
    {
        mRadius=aRadius;
        set_dirty();
    } 
    void ComponentColliderSphere::set_center(vec2 aCenter) //TODO Decide if this is relative to world scale or local scale
    {
        mCenter=aCenter;
        set_dirty();
    }    
    void ComponentColliderSphere::derived_setup()
	{
		ComponentPhysicsBase::derived_setup();
		//mEventConnectionTracker->add_connection(get_world_event_distributor().mEvent_always_render.connect(boost::bind(&ComponentColliderSphere::render,this,_1)));
	}
	void ComponentColliderSphere::render(const ScreenSpatialPosition& position)
	{
		compute_positions();
		SpatialPosition sp;
		sp.set_2d_position(mAbsCenter);
		sp.set_2d_scale(vec2(mAbsRadius,mAbsRadius)*2.0f);
        sp.set_2d_rotation(get_transform().get_absolute().get_2d_rotation());
		GLPrimitives::render_oval(position.get_spatial_position_with_screen_matrix()*sp.get_matrix(),color4(1,1,1,1),10);
	}
	void ComponentColliderSphere::compute_positions()
	{
        //TODO possibly needs to do this relative to body.position
		SpatialPosition sp = get_transform().get_absolute();
        mAbsCenter = sp.get_2d_position() + MATH::rotate_vec2(mCenter,sp.rotation);
		vec2 s = sp.get_2d_scale();
		//TODO get this to compile on windows fuck..
		mAbsRadius = mRadius * s.x;//RU::MATH::max(s.x,s.y);
	}
    b2Shape* ComponentColliderSphere::get_box_shape()
    {
        compute_positions();
        SpatialPosition offset = get_intermediary()->get_body_position();
		mShape.m_radius = PhysicsSettings::world_to_physics(mAbsRadius);
        SpatialPosition center;
        center.set_2d_position(mAbsCenter);
        center = center/offset;
        vec2 pos = PhysicsSettings::world_to_physics(vec2(center.position.x,center.position.y));
		mShape.m_p.Set(pos.x,pos.y);
        return &mShape;
    }
    ComponentColliderBox::ComponentColliderBox()
    {
        mRect = rect(-0.5f,-0.5f,1,1);
        mRot = 0;
        //DELETE mShape.SetAsBox(mRect.w,mRect.h,b2Vec2(mRect.x,mRect.y),mRot);
    }
	ComponentColliderBox::~ComponentColliderBox()
	{
	}
	ComponentInfoType ComponentColliderBox::get_component_info() const
	{
		return "ComponentColliderBox";
	}
    void ComponentColliderBox::derived_setup()
	{
		ComponentPhysicsBase::derived_setup();
		//mEventConnectionTracker->add_connection(get_world_event_distributor().mEvent_always_render.connect(boost::bind(&ComponentColliderBox::render,this,_1)));
	}
	void ComponentColliderBox::render(const ScreenSpatialPosition& position)
	{
		SpatialPosition sp = get_transform().get_absolute();
		sp.set_2d_scale(sp.get_2d_scale()); 
		GLPrimitives::render_box(position.get_spatial_position_with_screen_matrix()*sp.get_matrix());
	}
	void ComponentColliderBox::compute_positions()
	{ 
		SpatialPosition sp = get_transform().get_absolute();
        vec2 s = sp.get_2d_scale();
		mAbsRect.w = mRect.w*s.x;
		mAbsRect.h = mRect.h*s.y;
		mAbsRect.set_center(sp.get_2d_position() + MATH::rotate_vec2(mRect.get_center_2d(),sp.rotation));
		mAbsRot = sp.get_2d_rotation()+mRot;
	}
    void ComponentColliderBox::set_size(vec2 aSize) //TODO Decide if this is relative to world scale or local scale
    {
        mRect.w=aSize.x; 
        mRect.h=aSize.y;
        set_dirty();
    }
    void ComponentColliderBox::set_position(vec2 aPos) //TODO Decide if this is relative to world scale or local scale
    {
        mRect.x=aPos.x; 
        mRect.y=aPos.y;
        set_dirty();
    }
    b2Shape* ComponentColliderBox::get_box_shape()
    {
        compute_positions();
        SpatialPosition offset = get_intermediary()->get_body_position();
        SpatialPosition rectCenter; 
        rectCenter.set_2d_position(mAbsRect.get_center_2d());
        rectCenter = rectCenter/offset;
        vec2 dim = PhysicsSettings::world_to_physics(vec2(mAbsRect.w,mAbsRect.h));
        vec2 shapeCenter = PhysicsSettings::world_to_physics(vec2(rectCenter.position.x,rectCenter.position.y));
        float rot = PhysicsSettings::world_rotation_to_physics(mAbsRot-rectCenter.get_2d_rotation());
		mShape.SetAsBox(dim.x/2.0f,dim.y/2.0f,b2Vec2(shapeCenter.x,shapeCenter.y),rot);
        return &mShape;
    }
    ComponentColliderPoly::ComponentColliderPoly()
    {
    
    }
	ComponentColliderPoly::~ComponentColliderPoly()
	{
	}
    void ComponentColliderPoly::derived_setup()
	{
		ComponentPhysicsBase::derived_setup();
		//mEventConnectionTracker->add_connection(get_world_event_distributor().mEvent_always_render.connect(boost::bind(&ComponentColliderPoly::render,this,_1)));
	}
	ComponentInfoType ComponentColliderPoly::get_component_info() const
	{
		return "ComponentColliderPoly";
	}
	void ComponentColliderPoly::set_positions(PolygonPositionArrayType aPositions)
	{
		mPolygonPositions = aPositions;
		set_dirty();
	}
	void ComponentColliderPoly::render(const ScreenSpatialPosition& position)
	{
		compute_positions();
		if(mAbsolutePolygonPositions.size() > 0)
		{
			mAbsolutePolygonPositions.push_back(mAbsolutePolygonPositions[0]);
			GLPrimitives::render_poly(mAbsolutePolygonPositions);
			mAbsolutePolygonPositions.pop_back();
		}
	}
	void ComponentColliderPoly::compute_positions()
	{
		SpatialPosition sp = get_transform().get_absolute();
		mAbsolutePolygonPositions.resize(mPolygonPositions.size());
		for(int i = 0; i < mPolygonPositions.size(); i++)
		{
			mAbsolutePolygonPositions[i] = MATH::vec3_to_vec2(sp*MATH::vec2_to_vec3(mPolygonPositions[i]));
		}
	}
    b2Shape* ComponentColliderPoly::get_box_shape()
    {

        //TODO probably wrong
		compute_positions();
		SpatialPosition offset = (get_intermediary()->get_body_position()).get_inverse();
		b2Vec2* vertices = new b2Vec2[mAbsolutePolygonPositions.size()];
        for(int i = 0; i < mAbsolutePolygonPositions.size(); i++)
		{
			vec2 pos = PhysicsSettings::world_to_physics(MATH::vec3_to_vec2(offset*MATH::vec2_to_vec3(mAbsolutePolygonPositions[i])));
			vertices[i] = b2Vec2(pos.x,pos.y);
		}
		mShape.Set(vertices,mAbsolutePolygonPositions.size());
		delete vertices;
        return &mShape;
    }

	ComponentColliderChain::ComponentColliderChain()
	{
	}
	ComponentColliderChain::~ComponentColliderChain()
	{
	}
	ComponentInfoType ComponentColliderChain::get_component_info() const
	{
		return "ComponentColliderChain";
	}
	void ComponentColliderChain::set_positions(ChainPositionArrayType aChain)
	{
		mChainPositions = aChain;
		set_dirty();
	}
	void ComponentColliderChain::set_positions_loop(ChainPositionArrayType aChain)
	{
		mChainPositions = aChain;
		mChainPositions.push_back(mChainPositions[0]);
		set_dirty();
	}
    void ComponentColliderChain::derived_setup()
	{
		ComponentPhysicsBase::derived_setup();
	}
	void ComponentColliderChain::render(const ScreenSpatialPosition& position)
	{
		compute_positions();
		GLPrimitives::render_poly(mAbsoluteChainPositions);
	}
	void ComponentColliderChain::compute_positions()
	{
		SpatialPosition sp = get_transform().get_absolute();
		mAbsoluteChainPositions.resize(mChainPositions.size());
		for(int i = 0; i < mChainPositions.size(); i++)
			mAbsoluteChainPositions[i] = MATH::vec3_to_vec2(sp*MATH::vec2_to_vec3(mChainPositions[i]));
	}
	b2Shape* ComponentColliderChain::get_box_shape()
	{
		//TODO probably wrong
		compute_positions();
		SpatialPosition offset = (get_intermediary()->get_body_position()).get_inverse();
		b2Vec2* vertices = new b2Vec2[mAbsoluteChainPositions.size()];
        for(int i = 0; i < mAbsoluteChainPositions.size(); i++)
		{
			vec2 pos = PhysicsSettings::world_to_physics(MATH::vec3_to_vec2(offset*MATH::vec2_to_vec3(mAbsoluteChainPositions[i])));
			vertices[i] = b2Vec2(pos.x,pos.y);
		}
		mShape.CreateChain(vertices,mAbsoluteChainPositions.size());
		delete vertices;
        return &mShape;
	}
}

BOOST_CLASS_EXPORT_IMPLEMENT(RU::ComponentColliderBase)
BOOST_CLASS_EXPORT_IMPLEMENT(RU::ComponentColliderSphere)
BOOST_CLASS_EXPORT_IMPLEMENT(RU::ComponentColliderBox)
BOOST_CLASS_EXPORT_IMPLEMENT(RU::ComponentColliderPoly)
BOOST_CLASS_EXPORT_IMPLEMENT(RU::ComponentColliderChain)