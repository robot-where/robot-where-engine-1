#include "RuObjectReference.h"
#include "RuEntityBase.h"
#include "RuWorldEntityContainer.h"
#include "RuMain.h"
namespace RU{
    EntityBase* const EntityReference::DESERIALIZING_FLAG_VALUE = (EntityBase*)(1);
    EntityReference::EntityReference():mEntityRaw(NULL)
    {
    }
    bool EntityReference::is_deserializing()
    {
        return mEntityRaw == DESERIALIZING_FLAG_VALUE && is_weak_ptr_null(mEntity);
    }
    EntityReference::EntityReference(const EntityReference& o)
    {
        mId = o.mId;
        mEntity = o.mEntity;
        mEntityRaw = o.mEntityRaw;
        if(is_deserializing())
            WorldEntityContainer::swap_entity_reference_for_deserialization(&o, this);
    }
    EntityBase* EntityReference::operator->() const 
    {
        return mEntityRaw;
    }
    EntityBase& EntityReference::operator*() const 
    {
        return *mEntityRaw;
    }
    bool EntityReference::operator==(const EntityReference& o) const
    {
        return mId == o.mId;
    }
    bool EntityReference::operator!=(const EntityReference& o) const
    {
        return mId != o.mId;
    }
    void EntityReference::lookup(const WorldEntityContainer& aContainer)
    {
        if(!is_empty())
        {
            mEntity = aContainer.get_entity(mId);
            mEntityRaw = mEntity.lock().get();
        }
    }
    weak_ptr<EntityBase> EntityReference::get_safe() const
    {
        return mEntity;
    }
	EntityBase* EntityReference::get() const
	{
		return mEntityRaw;
	}
    
    void EntityReference::set(EntityBase* aEntity)
    {
        if(aEntity)
            *this = aEntity->get_entity_reference();
        else
        {
            mId = boost::uuids::nil_uuid();
            mEntity = weak_ptr<EntityBase>();
            mEntityRaw = NULL;
        }
    }
    void EntityReference::set(weak_ptr<EntityBase> aEntity)
    {
        mId = aEntity.lock()->get_entity_id();
        mEntity = aEntity;
		mEntityRaw = aEntity.lock().get();
    }
    void EntityReference::set(const ObjectIdType& aId)
    {
        mId = aId;
    }
	void EntityReference::register_this_in_world_entity_container()
	{
        WorldEntityContainer::register_entity_reference_for_deserialization(this);
        //now that this has been registered, we need to flag this reference as culprit
        //our indicator is that there is a mismatch between the weak and raw pointer
        mEntity = weak_ptr<EntityBase>();
        mEntityRaw = DESERIALIZING_FLAG_VALUE;
	}
	bool EntityReference::is_set() const
	{
		return shared_ptr<EntityBase>(mEntity);
	}
	bool EntityReference::is_valid() const
	{
		if(!is_set())
			throw SimpleException("Using un set EntityReference");
		return !mEntity.expired();
	}
}

BOOST_CLASS_EXPORT_IMPLEMENT(RU::EntityReference)