#include "RuComponentManager.h"
#include "RuEntityBase.h"
namespace RU
{
	void ComponentManager::setup_all()
	{
        //we use a temp map because new components are possibly created during setup and we do not want to initialize those twice
        ComponentMapType tempMap = mComponentMap;
		for(ComponentMapType::iterator it = tempMap.begin(); it != tempMap.end(); it++)
		{
			it->second->base_setup(mOwner.get_entity_reference());
			it->second->derived_setup();
		}
		for(ComponentMapType::iterator it = tempMap.begin(); it != tempMap.end(); it++)
			it->second->on_create();
	}

	void ComponentManager::setup_component(shared_ptr<ComponentBase> aComponent)
	{
		aComponent->base_setup(mOwner.get_entity_reference());
		aComponent->derived_setup();
	}
    
    void ComponentManager::remove_all_components()
    {
        while(mComponentMap.size() > 0)
        {
            mComponentMap.begin()->second->on_destroy();
            mComponentMap.begin()->second->derived_cleanup();
            mComponentMap.begin()->second->base_cleanup();
            mComponentMap.erase(mComponentMap.begin());
        }
    }
}