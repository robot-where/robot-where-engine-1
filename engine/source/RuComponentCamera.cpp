#include "RuComponentCamera.h"
#include "RuTransform.h"
#include "RuWorldEntityContainer.h"
namespace RU
{
    void ComponentCamera::derived_setup()
    {
        get_world_entity_container().register_camera_component(this);
    }
    void ComponentCamera::derived_cleanup()
    {
        get_world_entity_container().deregister_camera_component(this);
    }
    ComponentCamera::ComponentCamera():mDepth(0),mNormalizedViewportRect(0,0,1,1){}
	float ComponentCamera::get_depth() const
	{
		return mDepth;
	}
	void ComponentCamera::set_depth(float aDepth)
	{
		mDepth = aDepth;
        get_world_entity_container().update_camera_component_depth(this);	
    }

	ScreenSpatialPosition ComponentCamera::get_camera_screen(ScreenSpatialPosition aScreen)
	{
		aScreen.sp = aScreen.sp*get_transform().get_absolute();
        aScreen.sp.left_2d_translate(mNormalizedViewportRect.get_center_2d());
        aScreen.screenWidth *= mNormalizedViewportRect.w;
        aScreen.screenHeight *= mNormalizedViewportRect.h;
		//TODO set viewport coordinates here
		return aScreen;
	}
}
BOOST_CLASS_EXPORT_IMPLEMENT(RU::ComponentCamera);