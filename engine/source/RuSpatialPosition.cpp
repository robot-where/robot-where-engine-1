#include "RuSpatialPosition.h"
#include "RuMathFunctionsVector.h"
#include <glm/gtx/vector_angle.hpp>
#include <glm/gtx/quaternion.hpp>
namespace RU
{
    SpatialPosition::SpatialPosition(const mat4x4& aMat)
    {
        vec4 orig(0,0,0,1);
        vec4 pos = aMat*orig;
        pos.w = 0; //needed for proper homogeneous vector addition
        vec4 origx = vec4(1,0,0,1);
        vec4 unitx = aMat*vec4(1,0,0,1)-pos;
        vec4 unity = aMat*vec4(0,1,0,1)-pos;
        vec4 unitz = aMat*vec4(0,0,1,1)-pos;
        
        position = vec3(pos.x,pos.y,pos.z);
        scale = vec3(MATH::vec4_homogeneous_length(unitx),MATH::vec4_homogeneous_length(unity),MATH::vec4_homogeneous_length(unitz));
        
        //this is criminal
        //TODO angle does not go all the way around afaik
        vec3 origx3 = glm::normalize(MATH::vec4_homogeneous_to_vec3(origx));
        vec3 unitx3 = glm::normalize(MATH::vec4_homogeneous_to_vec3(unitx));
        vec3 cross = glm::cross(origx3,unitx3);
        float angle = glm::orientedAngle<float>(origx3,unitx3,cross);
        rotation = glm::angleAxis(angle,cross);

    }
    
    void SpatialPosition::set_position(const vec3& aPos)
    {
        position = aPos;
    }
    void SpatialPosition::set_scale(const vec3& aScale)
    {
        scale = aScale;
    }
    void SpatialPosition::set_rotation(const quat& aRot)
    {
        rotation = aRot;
    }
    
    //TODO I'm doing this the lazy/slow/stupid way
    void SpatialPosition::left_2d_scale(const vec2& aScale)
    {
        SpatialPosition sp;
        sp.set_2d_scale(aScale);
        *this = sp*(*this);                
    }
    void SpatialPosition::left_2d_rotate(const float& aRot)
    {
        SpatialPosition sp;
        sp.set_2d_rotation(aRot);
        *this = sp*(*this);        
    }
    void SpatialPosition::left_2d_translate(const vec2& aTrans)
    {
        SpatialPosition sp;
        sp.set_2d_position(aTrans);
        *this = sp*(*this);
    }
    void SpatialPosition::right_2d_scale(const vec2& aScale)
    {
        SpatialPosition sp;
        sp.set_2d_position(aScale);
        *this = (*this)*sp;        
    }
    void SpatialPosition::right_2d_rotate(const float& aRot)
    {
        SpatialPosition sp;
        sp.set_2d_rotation(aRot);
        *this = (*this)*sp;              
    }
    void SpatialPosition::right_2d_translate(const vec2& aTrans)
    {
        SpatialPosition sp;
        sp.set_2d_position(aTrans);
        *this = (*this)*sp;                      
    }
    void SpatialPosition::left_scale(const vec3& aScale)
    {
        
    }
    void SpatialPosition::left_rotate(const quat& aRot)
    {
        
    }
    void SpatialPosition::left_translate(const vec3& aTrans)
    {
        
    }
    void SpatialPosition::right_scale(const vec3& aScale)
    {
        
    }
    void SpatialPosition::right_rotate(const quat& aRot)
    {
        
    }
    void SpatialPosition::right_translate(const vec3& aTrans)
    {
        
    }
    
    mat4x4 SpatialPosition::get_matrix() const
    {
        //scale, rotate, then translate
        return glm::translate<float>(position)*
            glm::toMat4(rotation)*
            glm::scale<float>(scale.x,scale.y,scale.z);
    }
    
    SpatialPosition SpatialPosition::get_inverse() const
    {

		return SpatialPosition()/ *this;
		//faster version
		//TODO I'm pretty sure this is incorrect...
        //TODO needs to error check if scale is 0
        SpatialPosition r;
        r.position = -position;
        r.scale = vec3(1/scale.x, 1/scale.y, 1/scale.z);
        r.rotation = glm::inverse(rotation);
        return r;
    }
    
    mat4x4 ScreenSpatialPosition::get_spatial_position_matrix() const
    {
        return sp.get_matrix();
    }
    //by default, our transform coordinate system assumes the screen is screenWidthxscreenHeight with origin in the middle. This function is particularly useful for converting the 2x2 gl default screen to this
    mat4x4 ScreenSpatialPosition::get_screen_matrix() const
    {
        return glm::scale<float>(2/screenWidth,2/screenHeight,1);
    }
    mat4x4 ScreenSpatialPosition::get_spatial_position_with_screen_matrix() const
    {
        return get_screen_matrix()*get_spatial_position_matrix();
    }

	rect ScreenSpatialPosition::get_rectangle() const
	{
		rect r;
        r.w = screenWidth;
        r.h = screenHeight;
        r.set_center(sp.position.x,sp.position.y);
		return r;
	}
}