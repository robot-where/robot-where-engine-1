#include "RuComponentRenderableBase.h"
namespace RU
{
	ComponentRenderableBase::~ComponentRenderableBase()
	{
	}
	ComponentInfoType ComponentRenderableBase::get_component_info() const
	{
		return "ComponentRenderableBase";
	}
}
BOOST_CLASS_EXPORT_IMPLEMENT(RU::ComponentRenderableBase);