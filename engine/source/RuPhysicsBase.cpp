#include "RuPhysicsBase.h"
#include "RuTransform.h"
#include "Rumain.h"
namespace RU
{
	ComponentPhysicsBase::~ComponentPhysicsBase()
	{
	}
    void ComponentPhysicsBase::set_intermediary(PhysicsIntermediary* aIntermediary)
    {
        mIntermediary = aIntermediary;
        if(mIntermediary)
            mPhysicsId = mIntermediary->mPhysicsId;
        else mPhysicsId = NULL_PHYSICS_ID;
    }
    PhysicsIntermediary* ComponentPhysicsBase::get_intermediary()
    {
        return mIntermediary;
    }
	ComponentInfoType ComponentPhysicsBase::get_component_info() const
	{
		return "ComponentPhysicsBase";
	}
	void ComponentPhysicsBase::derived_setup()
	{
        ComponentRenderableBase::derived_setup();
		attach_to_physics();
	} 
	void ComponentPhysicsBase::derived_cleanup()
	{
		detach_from_physics();
        ComponentRenderableBase::derived_cleanup();
	}
	rect ComponentPhysicsBase::get_render_AABB()
	{
		SpatialPosition sp = get_transform().get_absolute();
		return rect(sp.position.x,sp.position.y,0,0);
	}
    WorldPhysics& ComponentPhysicsBase::get_world_physics()
    {
        return mOwner->get_primary().get_world_physics();
    }
	ComponentJoke::~ComponentJoke()
	{
	}
	ComponentInfoType ComponentJoke::get_component_info() const
	{
		return "ComponentJoke";
	}
}
BOOST_CLASS_EXPORT_IMPLEMENT(RU::ComponentPhysicsBase);
BOOST_CLASS_EXPORT_IMPLEMENT(RU::ComponentJoke);