#include "RuEntityBase.h"
namespace RU
{
	EntityTreeUtility::EntityTreeUtility(EntityBase& aOwner):mOwner(aOwner)
	{
	}
    EntityReference EntityTreeUtility::get_parent()
	{
		return mParent;
	}
    void EntityTreeUtility::set_to_top_level()
    {
        if(!mParent.is_empty())
        {
            mOwner.get_transform().set_relative(mOwner.get_transform().get_absolute());
            mParent = EntityReference();   
        }
    }
    void EntityTreeUtility::add_child(EntityReference aChild)
	{
        aChild->get_transform().set_relative(aChild->get_transform().get_absolute()/mOwner.get_transform().get_absolute());
        //better make sure aChild does not contain mOwner as a child lol.
        aChild->get_hierarchy().mParent = mOwner.get_entity_reference();
		mChildren.push_back(aChild);
	}
    void EntityTreeUtility::remove_from_tree()
	{
		mOwner.print_hierarchy_recursive();
        if(!mParent.is_empty())
        {
            EntityTreeUtility& h = mParent->get_hierarchy();
            //remove self from parent... slow
            for(ChildContainerType::iterator it = h.mChildren.begin(); it != h.mChildren.end(); it++)
            {
                if(*it == mOwner.get_entity_reference())
                {
                    h.mChildren.erase(it);
                    break;
                }
            }
            //attach all children to parent
            for(ChildContainerType::iterator it = mChildren.begin(); it != mChildren.end(); it++)
                h.add_child(*it);
        }
        else
        {
            for(ChildContainerType::iterator it = mChildren.begin(); it != mChildren.end(); it++)
                (*it)->get_hierarchy().set_to_top_level();
        }
		set_to_top_level();
        mChildren.clear();
	}
    unsigned EntityTreeUtility::get_number_children()
    {
        return mChildren.size();
    }
    EntityTreeUtility::ChildContainerType& EntityTreeUtility::get_children()
	{
		return mChildren;
	}


    void EntityBase::setup_entity(ObjectIdType aId, weak_ptr<EntityBase> aThis, weak_ptr<Primary> aPrimary)
    {
        mEntityId = aId;
		mThis.set(aThis);
        mPrimary = aPrimary;
        mTransform.base_setup(mThis);
    }
    void EntityBase::setup_entity_contents()
    {
        mComponentManager.add_existing_component<Transform>(shared_ptr<Transform>(&mTransform,null_deleter()));
        mComponentManager.setup_all(); //NOTE Transform gets setup twice now, but that's not a huge deal. We need Transform to be setup in order for other components to be able to access it during setup        
    }
}
BOOST_CLASS_EXPORT_IMPLEMENT(RU::EntityBase);