#include "RuWorldScreenEventDistributor.h"
#include "RuComponentRenderableBase.h"
#include <boost/foreach.hpp>
namespace RU
{
	ScreenEntityIntermediary::ScreenEntityIntermediary(ComponentRenderableBase* aComponent):component(aComponent),entity(aComponent->get_owner_ref()),flagged_for_update(false)
	{
	}
	bool ScreenEntityIntermediary::operator<(const ScreenEntityIntermediary& o) const
	{
		return component < o.component;
	}
	void BroadPhaseContainer::add_screen_entity(ComponentRenderableBase* aComponent)
	{
		mProxyIdVolumeMap.insert(BimapType::value_type(mBroadPhase.CreateProxy(to_b2AABB(aComponent->get_render_AABB()),NULL),ScreenEntityIntermediary(aComponent)));
	}
	void BroadPhaseContainer::remove_screen_entity(ComponentRenderableBase* aComponent)
	{
        BimapType::right_map::iterator it = mProxyIdVolumeMap.right.find(ScreenEntityIntermediary(aComponent));
		mBroadPhase.DestroyProxy(it->second);
        mProxyIdVolumeMap.right.erase(it);
	}
    void BroadPhaseContainer::update_rect(rect aRect, VolumeType aVolume)
    {
        mBroadPhase.MoveProxy(mProxyIdVolumeMap.right.find(aVolume)->second,to_b2AABB(aRect),b2Vec2(0,0));
    }
	void BroadPhaseContainer::update_all_aabb()
	{
        for(BimapType::left_map::const_iterator it = mProxyIdVolumeMap.left.begin(); it != mProxyIdVolumeMap.left.end(); it++)
		{
			mBroadPhase.MoveProxy(it->first,to_b2AABB(it->second.component->get_render_AABB()),b2Vec2(0,0));
		}
	}
    std::vector<BroadPhaseContainer::VolumeType>& BroadPhaseContainer::query(rect aRect)
    {
        mReturnBuffer.clear();
        b2AABB aabb = to_b2AABB(aRect);
        mBroadPhase.Query(this,aabb);
        return mReturnBuffer;
    }
}