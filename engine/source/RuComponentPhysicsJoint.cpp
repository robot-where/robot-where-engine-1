#include "RuComponentPhysicsJoint.h"
#include "RuPhysics.h"
namespace RU
{
    ComponentJointBase::ComponentJointBase():mCollideConnected(false),mAttachedRigidBody(NULL),mTargetRigidBody(NULL)
    {
    
    }
    void ComponentJointBase::attach_to_physics()
    {
        get_world_physics().add_component(this);

    }
    void ComponentJointBase::detach_from_physics()
    {
        get_world_physics().remove_component(this);

    }
    void ComponentJointBase::derived_setup()
    {
        ComponentPhysicsBase::derived_setup();
    }
    ComponentRigidBody* ComponentJointBase::get_other(ComponentRigidBody* rb)
    {
        if(rb == mAttachedRigidBody)
            return mTargetRigidBody;
        else if(rb == mTargetRigidBody)
            return mAttachedRigidBody;
        else throw new SimpleException("rb is not part of this joint");
    }
}