#include "RuMain.h"
namespace RU
{
#pragma warning( push )
#pragma warning( disable : 4355) //we need to set references back to parent object and this is the only way to do it
    Primary::Primary(const EngineParameters& aParam):mEngineTime(aParam.targetFramerate),mWorldPhysics(*this)
    {
        mKeyStates.reset(); //sets all bits to 0
        mKeyChangedThisFrame.reset();
        mQuit = false;
    }
#pragma warning( pop )
	void Primary::initialize()
	{
		mWorldEntityContainer.set_primary(shared_from_this());
	}
    Primary::~Primary()
    {
		mResourceManager.unload_all_resources();
        mWorldEntityContainer.destroy_all();
    }
    
    
    bool Primary::does_request_quit()
    {
        return mQuit;
    }

	void Primary::destroy()
	{
        
	}
    
    void Primary::update(float aDeltaTime)
    {
        //advance our system timer
        mEngineTime.advance_frame(aDeltaTime);
        
        //advance physics
		mWorldPhysics.update_physics(aDeltaTime);
        
        try 
        {
            //run update events
            mWorldEventDistributor.mEvent_update(get_time());

            //run render events
            //for OGL, projection matrix is assumed to be orthographic with ratio same as screen
            ScreenSpatialPosition p;
            p.screenWidth = get_screen_size().x;
            p.screenHeight = get_screen_size().y;
            mWorldEventDistributor.mEvent_always_render(p);
            
            mWorldEntityContainer.render_all_cameras(p);
            
        } 
        catch (SimpleException e) 
        {
            std::cout << "exception caught in Primary::update "  << e.what() << std::endl;
        }
        catch ( std::exception e)
        {
            std::cout << "exception caught in Primary::update "  << e.what() << std::endl;
        }
        
        mKeyChangedThisFrame.reset();
    }
    
    void Primary::set_screen_size(vec2 aSize)
    {
        mScreenSize = aSize;
    }
    void Primary::key_pressed(KeyCode aKey)
    {
        if(aKey == KEY_ESCAPE)
            mQuit = true;
        mKeyStates[aKey] = true;
        mKeyChangedThisFrame[aKey] = true;
    }
    void Primary::key_released(KeyCode aKey)
    {
        mKeyStates[aKey] = false;
        mKeyChangedThisFrame[aKey] = true;
    }

	void Primary::mouse_position_update(MouseIdType aId, MousePosition aMouse)
	{
	}
	void Primary::mouse_pressed(MouseIdType aId, MousePosition aMouse)
	{
	}
	void Primary::mouse_released(MouseIdType aId, MousePosition aMouse)
	{
	}
    
    bool Primary::was_key_pressed(KeyCode aKey)
    {
        return mKeyChangedThisFrame[aKey] && mKeyStates[aKey];
        
    }
    bool Primary::was_key_released(KeyCode aKey)
    {
        return mKeyChangedThisFrame[aKey] && !mKeyStates[aKey];        
    }
    bool Primary::get_key_state(KeyCode aKey)
    {
        return mKeyStates[aKey];
    }

    
    
    
    const EngineTime& Primary::get_time()
    {
        return mEngineTime;
    }
    vec2 Primary::get_screen_size()
    {
        return mScreenSize;
    }
    
    
	ResourceManager& Primary::get_resource_manager()
	{
		return mResourceManager;
	}
    WorldEntityContainer& Primary::get_world_entity_container()
    {
        return mWorldEntityContainer;
    }
	WorldEventDistributor& Primary::get_world_event_distributor()
	{
		return mWorldEventDistributor;
	}

	WorldPhysics& Primary::get_world_physics()
	{
		return mWorldPhysics;
	}

}