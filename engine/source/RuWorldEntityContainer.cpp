#include "RuWorldEntityContainer.h"
#include "RuComponentCamera.h"
#include "RuMain.h"
#include "RuWorldScreenEventDistributor.h"
namespace RU
{
    WorldEntityContainer::EntityReferenceContainerType WorldEntityContainer::sDeserializingReferences = EntityReferenceContainerType();
    void WorldEntityContainer::register_entity_reference_for_deserialization(EntityReference* aRef)
    {
        sDeserializingReferences.insert(aRef);
    }
    void WorldEntityContainer::swap_entity_reference_for_deserialization(const EntityReference* aOld, EntityReference* aNew)
    {
        sDeserializingReferences.erase(const_cast<EntityReference*>(aOld));//BAD Peter
        register_entity_reference_for_deserialization(aNew);
    }
    void WorldEntityContainer::clear_deserializing_references()
    {
        sDeserializingReferences.clear();
    }
    weak_ptr<EntityBase> WorldEntityContainer::get_last_created_entity()
    {
        return mLastCreatedEntity;
    }
    WorldEntityContainer::CameraContainerType::iterator WorldEntityContainer::find_camera_component(ComponentCamera* aCamera)
    {
        //TODO could do a binary search using get_depth() you know.
        for(CameraContainerType::iterator it = mCameraContainer.begin(); it != mCameraContainer.end(); it++)
            if((*it) == aCamera)
                return it;
        return mCameraContainer.end();
    }
    void WorldEntityContainer::update_camera_component_depth(ComponentCamera* aCamera)
    {
        CameraContainerType::iterator it = find_camera_component(aCamera);
        if(it != mCameraContainer.end())
        {
            mCameraContainer.erase(it);
            register_camera_component(aCamera);
        }
        throw SimpleException("updating camera that does not exist");
    }
	void WorldEntityContainer::register_camera_component(ComponentCamera* aCamera)
	{
		bool inserted = false;
		for(CameraContainerType::iterator it = mCameraContainer.begin(); it != mCameraContainer.end(); it++)
		{
			if((*it)->get_depth() > aCamera->get_depth())
			{
				mCameraContainer.insert(it,aCamera);
				inserted = true;
				break;
			}
		}
		if(!inserted)
			mCameraContainer.push_back(aCamera);
	}
    void WorldEntityContainer::deregister_camera_component(ComponentCamera* aCamera)
    {
        CameraContainerType::iterator it = find_camera_component(aCamera);
        if(it != mCameraContainer.end())
        {
            mCameraContainer.erase(it);
            return;
        }
        throw SimpleException("deregistering camera that does not exist");
    }
	void WorldEntityContainer::render_all_cameras(ScreenSpatialPosition aScreen)
	{
        mPrimary.lock()->get_world_event_distributor().mRenderBroadPhaseContainer.update_all_aabb();
		for(CameraContainerType::reverse_iterator it = mCameraContainer.rbegin(); it != mCameraContainer.rend(); it++)
		{
			ScreenSpatialPosition cameraScreen = (*it)->get_camera_screen(aScreen);
			rect screenRect = aScreen.get_rectangle();
			BroadPhaseContainer::QueryReturnType& renderList = mPrimary.lock()->get_world_event_distributor().mRenderBroadPhaseContainer.query(screenRect);
			for(BroadPhaseContainer::QueryReturnType::iterator jt = renderList.begin(); jt != renderList.end(); jt++)
				jt->component->render(cameraScreen);
		}
	}
    void WorldEntityContainer::destroy_all()
    {
        //slow, but that's okay
        while(mGlobalEntityContainer.size() > 0)
            destroy_entity(mGlobalEntityContainer.begin()->second->get_entity_id());
    }
    void WorldEntityContainer::setup_after_deserialization()
    {
        
        for(EntityReferenceContainerType::iterator it = sDeserializingReferences.begin(); it != sDeserializingReferences.end(); it++)
        {
            (*it)->lookup(*this);
        }
        clear_deserializing_references();
        for(EntityContainerType::iterator it = mGlobalEntityContainer.begin(); it != mGlobalEntityContainer.end(); it++)
            it->second->setup_entity(it->first,it->second,mPrimary);    
        for(EntityContainerType::iterator it = mGlobalEntityContainer.begin(); it != mGlobalEntityContainer.end(); it++)
            it->second->setup_entity_contents();
    }
}