#include "RuComponentRigidBody.h"
#include "RuEntityBase.h"
#include "RuPhysics.h"
namespace RU
{
	ComponentRigidBody::ComponentRigidBody():mass(1),mBodyType(RIGIDBODY_DYNAMIC)
	{
	}
	ComponentRigidBody::~ComponentRigidBody()
	{
	}
	ComponentInfoType ComponentRigidBody::get_component_info() const
	{
		return "ComponentRigidBody";
	}
	std::string ComponentRigidBody::name()
	{
		return "ComponentRigidBody";
	}
	void ComponentRigidBody::derived_setup()
	{
		ComponentPhysicsBase::derived_setup();
		//mEventConnectionTracker->add_connection(get_world_event_distributor().mEvent_always_render.connect(boost::bind(&ComponentRigidBody::render,this,_1)));
	}
    bool ComponentRigidBody::has_spatial_position_changed(const SpatialPosition& aSp)
    {
        if(is_dirty())
            return true;
        return get_transform().get_absolute() != aSp;
    }
    void ComponentRigidBody::data_updated()
    {
        //TODO 
    }
    void ComponentRigidBody::set_body_type(PhysicsRigidBodyTypes aType)
    {
        mBodyType = aType;
        //TODO build better accessor to physic intermediary!
        //TODO also this is not safe at all
        get_world_physics().get_physics_object(get_physics_id())->mBoxBody->SetType(b2BodyType(aType));
    }
    PhysicsRigidBodyTypes ComponentRigidBody::get_body_type()
    {
        return mBodyType;
    }
    float ComponentRigidBody::get_mass()
    {
        return mass;
    }
	void ComponentRigidBody::set_mass(float aMass)
	{
		mass = aMass; 
		//TOOD write this function data_updated();
	}
	void ComponentRigidBody::attach_to_physics()
	{
		get_world_physics().add_component(this);
	}
	void ComponentRigidBody::detach_from_physics()
	{
		get_world_physics().remove_component(this);
	}
    
    void ComponentRigidBody::on_collision(ComponentColliderBase* A, ComponentColliderBase* B)
    {
        CollisionInfo info;
        info.c1 = A;
        info.c2 = B;
        mEventCollision(info);
    }
	rect ComponentRigidBody::get_render_AABB()
	{
        return ComponentPhysicsBase::get_render_AABB();
	}
	void ComponentRigidBody::render(const ScreenSpatialPosition& position)
	{
		SpatialPosition sp = get_transform().get_absolute();
		sp.set_2d_scale(vec2(5));
		GLPrimitives::render_oval(position.get_spatial_position_with_screen_matrix()*sp.get_matrix(),color4(0,0,1,1),10);
	}
}
BOOST_CLASS_EXPORT_IMPLEMENT(RU::ComponentRigidBody)
BOOST_CLASS_EXPORT_IMPLEMENT(RU::ComponentFakeRigidBody)