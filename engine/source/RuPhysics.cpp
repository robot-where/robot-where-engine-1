#include "RuPhysics.h"
#include "RuEntityBase.h"
#include "RuComponentRigidBody.h"
#include "RuMathTypesStream.h"
#include "RuMain.h"
#include <stack>
namespace RU
{
	PhysicsIdType WorldPhysics::create_new_physics_id()
	{
		if(mInteractingObjects.size() == 0) return 1;
		return mInteractingObjects.rbegin()->first+1;
	}
	PhysicsObjectsContainerType::iterator WorldPhysics::create_physics_intermediary(PhysicsIdType aId)
	{
		PhysicsObjectsContainerType::iterator it = mInteractingObjects.insert(std::pair<PhysicsIdType,PhysicsIntermediary>(aId,PhysicsIntermediary())).first;
		it->second.set_physics_id(aId);
		it->second.set_world(mBoxWorld);
		return it;
	}

	ComponentColliderBase* WorldPhysics::get_collider(EntityBase& aBase)
	{
		//lol...
		ComponentColliderBase* col = aBase.get_component_manager().get_component<ComponentColliderBox>().lock().get();
		col = col?col:aBase.get_component_manager().get_component<ComponentColliderSphere>().lock().get();
		col = col?col:aBase.get_component_manager().get_component<ComponentColliderPoly>().lock().get();
		return col;
	}

	void WorldPhysics::get_obtainable_colliders_in_children(EntityBase& aBase, std::vector<ComponentColliderBase*>& output)
	{
		output.clear();
		typedef EntityReference StackContainedType;
		typedef std::stack<StackContainedType> StackType;
		StackType operating;


		ComponentColliderBase* col = get_collider(aBase);
		if(col) output.push_back(col);
        //this function is more awesome but broken for some reason :(
        //aBase.get_hierarchy().for_each_child(boost::bind(static_cast<void (StackType::*)(const StackContainedType& )>(&StackType::push),operating,_1));
        EntityTreeUtility::ChildContainerType& children = aBase.get_hierarchy().get_children();
        for(EntityTreeUtility::ChildContainerType::iterator it = children.begin(); it != children.end(); it++)
            operating.push(*it);
		while(operating.size() > 0)
		{
			EntityBase* base = operating.top().get();
			operating.pop();
			ComponentRigidBody*  rb = base->get_component_manager().get_component<ComponentRigidBody>().lock().get();
			if(rb && !rb->is_fake()) //we do not comandeer ones owned by real rigid bodies
				continue;

			ComponentColliderBase* col = get_collider(*base);
			if(col) 
				output.push_back(col);
			base->get_hierarchy().for_each_child(boost::bind(static_cast<void (StackType::*)(const StackContainedType& )>(&StackType::push),operating,_1));
		}
	}

	//this function is also used for setting up physics callbacks
	ComponentRigidBody*  WorldPhysics::get_parent_rigid_body(EntityBase& aBase)
	{
		ComponentRigidBody*  rb = aBase.get_component_manager().get_component<ComponentRigidBody>().lock().get();
		if(rb) 
			return rb;
        //TODO consider doing this with EntityReference
		EntityBase* parent = aBase.get_hierarchy().get_parent().get();
		while(parent != NULL)
		{
			rb = parent->get_component_manager().get_component<ComponentRigidBody>().lock().get();
			if(rb) 
				return rb;
			parent = parent->get_hierarchy().get_parent().get();
		}
        return NULL;
	}


	WorldPhysics::WorldPhysics(Primary& aPrimary):mPrimary(aPrimary)
	{
		mVelocityIterations = 8;
		mPositionIterations = 3;
		mBoxWorld = shared_ptr<b2World>(new b2World(b2Vec2(0.0f, -10.0f)));
	}

	void WorldPhysics::print_entities()
	{
		for(PhysicsObjectsContainerType::iterator it=mInteractingObjects.begin(); it!= mInteractingObjects.end(); it++)
			it->second.print();
		std::cout << std::flush;
	}

	//TODO delete, I don't htink I use this...
	PhysicsIntermediary* WorldPhysics::get_physics_object(PhysicsIdType aId)
	{
		PhysicsObjectsContainerType::iterator it = mInteractingObjects.find(aId);
		if(it != mInteractingObjects.end())
			return &it->second;
		return (PhysicsIntermediary*)NULL;
	}
	void WorldPhysics::update_physics(float aDt)
	{	
        //checks for changes in physics properties and positions NOT due to physics
        for(PhysicsObjectsContainerType::iterator it = mInteractingObjects.begin(); it != mInteractingObjects.end(); it++)
            it->second.compute_positions(it->second.mLastAbsoluteSpatialPosition);
        
		mBoxWorld->Step(aDt, mVelocityIterations, mPositionIterations);
		b2Contact* contacts = mBoxWorld->GetContactList();
		while(contacts != NULL)
		{
			//TODO run collision and trigger callbacks
            //TODO I guess I need a reverse map from b2Bodies to physicsIntermediary
			contacts = contacts->GetNext();
		}
		for(PhysicsObjectsContainerType::iterator it = mInteractingObjects.begin(); it != mInteractingObjects.end(); it++)
		{
            b2Vec2 p = it->second.mBoxBody->GetPosition();
            vec2 np = PhysicsSettings::physics_to_world(vec2(p.x,p.y));
            float nr = PhysicsSettings::physics_rotation_to_world(it->second.mBoxBody->GetAngle());
            //std::cout << np << std::endl;
            if(it->second.mRigidBody) //TODO delete this, this should always be true
            {
                //computes the new spatial position according to physics and updates last spatial position to be this.
                SpatialPosition sp = it->second.mRigidBody->get_transform().get_absolute();
				sp.set_2d_position(np);
                sp.set_2d_rotation(nr);
				it->second.mRigidBody->get_transform().set_absolute(sp);
                it->second.set_last_box_spatial_position(sp);
            }
            else
                throw SimpleException("Physics intermediary has no body"); 

		}
	}

	void WorldPhysics::add_component(ComponentRigidBody*  aRigidBody)
	{
		//create an entry for the rigid body
		PhysicsObjectsContainerType::iterator physit = create_physics_intermediary(create_new_physics_id());
		physit->second.add_component(aRigidBody);

		//take or commandeer all colliders
		typedef std::vector<ComponentColliderBase*  > ColliderVector;
		ColliderVector take;
		get_obtainable_colliders_in_children(aRigidBody->get_owner(), take);
		for(ColliderVector::iterator it = take.begin(); it != take.end(); it++)
		{
			if((*it)->get_physics_id() != NULL_PHYSICS_ID)
				remove_component(*it);
			physit->second.add_component(*it);
		}
	}

	void WorldPhysics::add_component(ComponentColliderBase*  aCollider)
	{
		ComponentRigidBody* rb = get_parent_rigid_body(aCollider->get_owner());
		if(!rb)
			aCollider->mOwner->get_component_manager().add_component<ComponentFakeRigidBody>();
		else if(rb->get_physics_id() != NULL_PHYSICS_ID)
			mInteractingObjects[rb->get_physics_id()].add_component(aCollider);
	}
    
    void WorldPhysics::add_component(ComponentJointBase*  aCollider)
    {
        //TODO
    }


	void WorldPhysics::remove_component(ComponentRigidBody*  aRigidBody)
	{
		PhysicsObjectsContainerType::iterator it = mInteractingObjects.find(aRigidBody->get_physics_id());
		if(it != mInteractingObjects.end())
		{
			PhysicsIntermediary::ColliderContainerType tempColliders = it->second.mColliders;
			it->second.mColliders.clear();
			for(PhysicsIntermediary::ColliderContainerType::iterator jt=tempColliders.begin(); jt!=tempColliders.end(); jt++)
			{
                //the collider is now a free agent
				jt->first->set_intermediary(NULL);
                //if collider is attached to same object as with the rigid body being removed then we don't do anything, it will either be reattached by a real rigidbody or destroyed
                //however if it is a child to the rigidbody entity, then we will let it recreate it's own fake rigidbodies since it's not guaranteed it will be reattached by a real rigidbody
                if(aRigidBody->is_fake() && (jt->first->get_owner_ref() != aRigidBody->get_owner_ref())) 
                    add_component(jt->first);
			}
            aRigidBody->set_intermediary(NULL);
            it->second.mRigidBody = NULL;
			mInteractingObjects.erase(it->first);
		}
	}
	void WorldPhysics::remove_component(ComponentColliderBase*  aCollider)
	{
        if(aCollider->get_physics_id() == NULL_PHYSICS_ID) //this can happen, see comments in remove_component(RigidBody)
            return;
		PhysicsObjectsContainerType::iterator it = mInteractingObjects.find(aCollider->get_physics_id());
		if(it != mInteractingObjects.end())
		{
            ComponentRigidBody* rb = aCollider->mOwner->get_component_manager().get_component<ComponentRigidBody>().lock().get();
            it->second.remove_component(aCollider);
            if(rb && rb->is_fake())
                remove_component(rb);
		}
		else throw SimpleException("removing component that is not registered in physics");
	}
    
    void WorldPhysics::remove_component(ComponentJointBase*  aCollider)
    {
        //TODO
    }
}
