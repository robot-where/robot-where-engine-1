#include "RuPhysicsIntermediary.h"
#include "RuPhysics.h"
#include "RuEntityBase.h"
#include "RuComponentRigidBody.h"
namespace RU
{
    void PhysicsIntermediary::create_body()
    {
        //TODO this is for reference
        b2BodyDef body;
        body.userData = NULL;
        body.position.Set(0.0f, 0.0f);
        body.angle = 0.0f;
        body.linearVelocity.Set(0.0f, 0.0f);
        body.angularVelocity = 0.0f;
        body.type = b2_dynamicBody;
        body.fixedRotation = false;
        mBoxBody = mBoxWorld.lock()->CreateBody(&body);
    }
    
    void PhysicsIntermediary::set_mass(const float& aMass)
    {
        b2MassData data;
        data.mass = aMass;
        data.center = b2Vec2(0,0); //or do we want this to be rigid body position??
        
        if(mColliders.size() == 0)
            mBoxBody->SetMassData(&data);
        
        //TODO something else if there are colliders
    }
	void PhysicsIntermediary::add_component(ComponentRigidBody*  aRigidBody)
	{
		aRigidBody->set_intermediary(this);
		mRigidBody = aRigidBody;
		if(!mBoxBody) create_body();
		vec3 pos3 = aRigidBody->get_owner().get_transform().get_absolute().position;
		vec2 pos = PhysicsSettings::world_to_physics(vec2(pos3.x,pos3.y));
		float angle = PhysicsSettings::world_rotation_to_physics(aRigidBody->get_owner().get_transform().get_absolute().get_2d_rotation());
		mBoxBody->SetTransform(b2Vec2(pos.x,pos.y),angle);
        mBoxBody->SetType(b2BodyType(aRigidBody->get_body_type()));
        set_mass(mRigidBody->get_mass());
		//TODO set things like density, also go and recompute center of mass...
	}
	void PhysicsIntermediary::add_component(ComponentColliderBase*  aCollider)
	{
		aCollider->set_intermediary(this);
		if(!mBoxBody) 
			throw SimpleException("attempting to add collider where there is no body");
        
		//if collider did not exist in the map
		std::pair<ColliderContainerType::iterator,bool> it = mColliders.insert(std::pair<ComponentColliderBase* ,b2Fixture*>(aCollider,(b2Fixture*)NULL));
        /* this will get called anyway in compute_positions()
		if(it.second)
		{
			//TODO set denisty to something other than 1??? compute from mass of mBoxBody...
			it.first->second = mBoxBody->CreateFixture(it.first->first->get_box_shape(),0);
		}
		else
			throw SimpleException("adding collider that already exists");
         */
	}
    void PhysicsIntermediary::add_component(ComponentJointBase* aJoint)
    {
        aJoint->set_intermediary(this);
        //TODO fuck
    }
	void PhysicsIntermediary::remove_component(ComponentColliderBase*  aCollider)
	{
		aCollider->set_intermediary(NULL);
		ColliderContainerType::iterator remove = mColliders.find(aCollider);
		if(remove != mColliders.end())
		{
			if(remove->second) mBoxBody->DestroyFixture(remove->second);
			mColliders.erase(remove);
		}
		else
			throw SimpleException("removing collider that does not exist");
	}
    void PhysicsIntermediary::remove_component(ComponentJointBase* aJoint)
    {
        aJoint->set_intermediary(NULL);
        JointContainerType::iterator remove = mJoints.find(aJoint);
        if(remove != mJoints.end())
        {
            if(remove->second) mBoxWorld.lock()->DestroyJoint(remove->second);
            //now manually remove joint from the other physicsintermediary
            remove->first->get_other(mRigidBody)->get_intermediary()->mJoints.erase(aJoint);
        }
    }
    PhysicsIntermediary::~PhysicsIntermediary()
    {
        if(mBoxBody != NULL)
            mBoxWorld.lock()->DestroyBody(mBoxBody);
        if(mPhysicsId != NULL_PHYSICS_ID)
            if(mRigidBody || mColliders.size())
                throw SimpleException("destroying physics intermediary with things still attached");
    }
    
    SpatialPosition PhysicsIntermediary::get_body_position()
    {
        b2Vec2 p = mBoxBody->GetPosition();
        float rot = mBoxBody->GetAngle();
        SpatialPosition r;
        r.set_2d_position(PhysicsSettings::physics_to_world(vec2(p.x,p.y)));
        r.set_2d_rotation(PhysicsSettings::physics_rotation_to_world(rot));
        return r;
    }
    
    void PhysicsIntermediary::set_last_box_spatial_position(const SpatialPosition& aSp)
    {
        mLastAbsoluteSpatialPosition=aSp;
        for(ColliderContainerType::iterator it = mColliders.begin(); it != mColliders.end(); it++)
        {
            SpatialPosition sp = it->first->get_transform().get_absolute();
            it->first->set_last_relative_spatial_position(aSp/sp); //this is collider SP relative to entity SP
        }
    }
    
    //TODO maybe rename this to update lol
	void PhysicsIntermediary::compute_positions(const SpatialPosition& aSp)
	{
        if(mRigidBody->has_spatial_position_changed(aSp))
        {
            //set rigidbody in box2d
            //TODO move this stuff into a function in physics base
            //TODO consider making a version that only changes velocity and not position...
            vec3 pos3 = mRigidBody->get_owner().get_transform().get_absolute().position;
            vec2 pos = PhysicsSettings::world_to_physics(vec2(pos3.x,pos3.y));
            float angle = PhysicsSettings::world_rotation_to_physics(mRigidBody->get_owner().get_transform().get_absolute().get_2d_rotation());
            mBoxBody->SetTransform(b2Vec2(pos.x,pos.y),angle);
            mBoxBody->SetType(b2BodyType(mRigidBody->get_body_type()));
            mRigidBody->set_clean();
        }
        typedef std::list<ComponentColliderBase*> TempColliderContainerType;
        TempColliderContainerType tempColliders;
        for(ColliderContainerType::iterator it = mColliders.begin(); it != mColliders.end(); it++)
            if(it->first->has_spatial_position_changed(aSp))
                tempColliders.push_back(it->first);
        for(TempColliderContainerType::iterator it = tempColliders.begin(); it != tempColliders.end(); it++)
        {
            //set colliders in box2d
            //TODO also consider moving this into collider base
            //although, it might be better here because the shapes might need to be mutually aware of each other to properly distribute mass
            //TODO consider doing fixture->GetShape() and modifying that...
            ColliderContainerType::iterator colit = mColliders.find(*it);
            if(colit->second) mBoxBody->DestroyFixture(colit->second);
            b2Shape * shape = colit->first->get_box_shape();
            b2MassData massData;
            shape->ComputeMass(&massData, 1);
            colit->second = mBoxBody->CreateFixture(shape,massData.mass); //this makes all shapes have the same density
            colit->first->set_clean();
        }
        
        //TODO handle joints here
	}
    
    void PhysicsIntermediary::print()
    {
        std::string typeString;
        switch(mBoxBody->GetType())
        {
            case b2_staticBody:
                typeString = "stat";
                break;
            case b2_kinematicBody:
                typeString = "kin";
                break;
            case b2_dynamicBody:
                typeString = "dyn";
                break;
            default:
                typeString = "ERR";
        }
        std::cout << "PI: type-" << typeString << " mass-" << mBoxBody->GetMass() << " pos-(" << mBoxBody->GetPosition().x << "," << mBoxBody->GetPosition().y << ") vel-(" << mBoxBody->GetLinearVelocity().x << "," << mBoxBody->GetLinearVelocity().y << ")\n";
        std::cout << "collider count: " << mColliders.size() << "\n";
    }
}