#include <iostream>
#include "ExtRu.h"
#include "RuTypes.h"
#include "RuComponentRenderImage.h"
#include "RuEntityBase.h"
#include "RuMain.h"
#include "RuMathFunctionsQuaternion.h"
#include <fstream>
#include "ComponentTesting.h"
#include "RuComponentCamera.h"
namespace RU
{
    void physics_test(shared_ptr<Primary> game)
    {
        weak_ptr<EntityBase> e = game->get_world_entity_container().create_entity("e");
        e.lock()->get_component_manager().add_component<ComponentRigidBody>();
		e.lock()->get_component_manager().add_component<ComponentColliderSphere>();
		e.lock()->get_transform().get_relative_ref().set_2d_scale(vec2(75,75));
        e.lock()->get_transform().get_relative_ref().set_2d_position(vec2(50,0));
        e.lock()->get_component_manager().add_component<ComponentJiggle>();
		weak_ptr<ComponentRenderImage> c = e.lock()->get_component_manager().add_component<ComponentRenderImage>();
		c.lock()->set_image("cat.bmp");
        
        weak_ptr<EntityBase> e3 = game->get_world_entity_container().create_entity("e3");
        e3.lock()->get_transform().get_relative_ref().set_2d_scale(vec2(30,30));
		e3.lock()->get_transform().get_relative_ref().set_2d_position(vec2(-5,-40));
        e.lock()->get_hierarchy().add_child(e3.lock()->get_entity_reference());
        e3.lock()->get_component_manager().add_component<ComponentRigidBody>();//.lock().get()->set_body_type(RIGIDBODY_KINEMATIC);
		e3.lock()->get_component_manager().add_component<ComponentColliderBox>();
        
        e.lock()->get_transform().get_relative_ref().set_2d_rotation_degrees(42);
        
        weak_ptr<EntityBase> e2 = game->get_world_entity_container().create_entity();
         e2.lock()->get_component_manager().add_component<ComponentRigidBody>();//.lock().get()->set_body_type(RIGIDBODY_KINEMATIC);
         e2.lock()->get_component_manager().add_component<ComponentColliderBox>();
         e2.lock()->get_transform().get_relative_ref().set_2d_scale(vec2(25,25));
         e2.lock()->get_transform().get_relative_ref().set_2d_position(vec2(5,-30));
        
        weak_ptr<EntityBase> e4 = game->get_world_entity_container().create_entity("e4");
        //e4.lock()->get_component_manager().add_component<ComponentRigidBody>();
		e4.lock()->get_component_manager().add_component<ComponentColliderBox>();
		e4.lock()->get_transform().get_relative_ref().set_2d_scale(vec2(25,25));
		e4.lock()->get_transform().get_relative_ref().set_2d_position(vec2(-100,30));
		e4.lock()->get_transform().get_relative_ref().set_2d_rotation_degrees(46);
        e3.lock()->get_hierarchy().add_child(e4.lock()->get_entity_reference());
        
        weak_ptr<EntityBase> e5 = game->get_world_entity_container().create_entity("e5");
        //e5.lock()->get_component_manager().add_component<ComponentRigidBody>();
		e5.lock()->get_component_manager().add_component<ComponentColliderBox>();
		e5.lock()->get_transform().get_relative_ref().set_2d_scale(vec2(500,50));
		e5.lock()->get_transform().get_relative_ref().set_2d_position(vec2(0,-200));
        
        weak_ptr<EntityBase> camera	 = game->get_world_entity_container().create_entity("camera");
		//camera.lock()->get_component_manager().add_component<ComponentJiggle>();
        camera.lock()->get_component_manager().add_component<ComponentCamera>();
        camera.lock()->get_component_manager().add_component<ComponentKeyboardController>();
    }
    
    void sp_test(shared_ptr<Primary> game)
    {
        weak_ptr<EntityBase> adult = game->get_world_entity_container().create_entity("adult");
        weak_ptr<EntityBase> c1 = game->get_world_entity_container().create_entity("child_1");
        adult.lock()->get_hierarchy().add_child(c1.lock()->get_entity_reference());
        weak_ptr<EntityBase> c2 = game->get_world_entity_container().create_entity("child_2");
        adult.lock()->get_hierarchy().add_child(c2.lock()->get_entity_reference());
        weak_ptr<EntityBase> c11 = game->get_world_entity_container().create_entity("child_1_1");
        c1.lock()->get_hierarchy().add_child(c11.lock()->get_entity_reference());
        
        adult.lock()->get_component_manager().add_component<ComponentJiggle>();
        c1.lock()->get_component_manager().add_component<ComponentJiggle>();
        adult.lock()->get_component_manager().add_component<ComponentExistence>();
        c1.lock()->get_component_manager().add_component<ComponentExistence>();
        c2.lock()->get_component_manager().add_component<ComponentExistence>();
        c11.lock()->get_component_manager().add_component<ComponentExistence>();
        
        adult.lock()->get_transform().get_relative_ref().set_2d_position(vec2(-100,-50));
        adult.lock()->get_transform().get_relative_ref().set_2d_scale(vec2(1,1.5));
        c1.lock()->get_transform().get_relative_ref().set_2d_position(vec2(100,0));
        c2.lock()->get_transform().get_relative_ref().set_2d_position(vec2(0,100));
        c11.lock()->get_transform().get_relative_ref().set_2d_position(vec2(100,0));
        
    }
    
    void game(shared_ptr<Primary> game)
    {
        //
    }
    
	int __main_helper()
	{
		std::cout << "Hello World!" << std::endl;
		std::cout << "Robot! Where?" << std::endl;
		shared_ptr<Primary> game = create_engine(EngineParameters());
        physics_test(game);
        //sp_test(game);
		std::string filename("serialization_test");
		{
			std::ofstream ofs(filename.c_str());
			boost::archive::xml_oarchive oa(ofs,  boost::archive::no_header );
			std::cout << "writing to file " << filename << "...";
			oa << boost::serialization::make_nvp("gameworld",game->get_world_entity_container());
			std::cout << " success!" << std::endl;
		}
		game->get_world_entity_container().destroy_all();
		{
			std::ifstream ifs(filename.c_str());
            if (!ifs.good()) throw new SimpleException("ifs not good");
			boost::archive::xml_iarchive ia(ifs,  boost::archive::no_header );
			std::cout << "reading from file " << filename << "...\n";
			ia >> boost::serialization::make_nvp("gameworld",game->get_world_entity_container());
			std::cout << " success!" << std::endl;
			game->get_world_entity_container().set_primary(game);
			std::cout << "doing setup after deserialization...";
			game->get_world_entity_container().setup_after_deserialization();
			std::cout << " success!" << std::endl;
		}
        
        //game->get_world_physics().print_entities();
        //game->get_world_entity_container().print_entities();
		
		std::cout << "running game." << std::endl;
		RU::run_engine(game);
		std::cout << " quitting." << std::endl;
		return 0;
	}
}

int main(int argc, char *argv[])
{
	return RU::__main_helper();
}