#include "ComponentTesting.h"
#include "RuTransform.h"
#include "ExtRuGL.h"
#include "RuMain.h"
#include "RuKeys.h"

namespace RU
{
    void ComponentJiggle::derived_setup()
    {
        auto_connect_update(this);
        //get_primary().get_world_event_distributor().mEvent_update(get_primary().get_time());
    }
    
    void ComponentJiggle::update(const EngineTime& aTime)
    {
        get_transform().get_relative_ref().set_2d_rotation_degrees(get_transform().get_relative().get_2d_rotation_degrees()+1);
        //get_transform().get_relative_ref().set_2d_scale(vec2(1,1.0f + sin(aTime.get_total_time())/2.0f));
    }
    void ComponentExistence::always_render(const ScreenSpatialPosition& position)
    {
        SpatialPosition sp = get_transform().get_absolute();
        sp.set_2d_scale(10.0f*sp.get_2d_scale());
        GLPrimitives::render_oval(position.get_spatial_position_with_screen_matrix()*sp.get_matrix(),color4(0,1,1,1),10);
    }
    
    void ComponentExistence::derived_setup()    
    {
        auto_connect_always_render(this);
        auto_connect_update(this);
    }
    
    void ComponentKeyboardController::derived_setup()    
    {
        auto_connect_update(this);
    }
    
    void ComponentKeyboardController::update(const EngineTime& aTime)
    {
 
        if(get_primary().get_key_state(KEY_UP))
            get_transform().get_relative_ref().position += vec3(0,100,0)*aTime.get_last_delta_time();
    }
}
BOOST_CLASS_EXPORT_IMPLEMENT(RU::ComponentJiggle)
BOOST_CLASS_EXPORT_IMPLEMENT(RU::ComponentExistence)
BOOST_CLASS_EXPORT_IMPLEMENT(RU::ComponentKeyboardController)