#pragma once
#include "RuTypes.h"
#include "RuComponentRenderableBase.h"
#include "RuTime.h"

namespace RU
{
	class ComponentJiggle : public ComponentBase
	{
		friend class boost::serialization::access;
		void derived_setup();

	public:
		ComponentJiggle(){}
		static std::string name(){return "ComponentJiggle";}
        void update(const EngineTime& aTime);
	private:
		template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
            ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(ComponentBase);
        }
	};
    
    class ComponentExistence : public ComponentRenderableBase
    {
        friend class boost::serialization::access;
		void derived_setup();
	public:
		ComponentExistence(){}
		static std::string name(){return "ComponentExistence";}
        void update(const EngineTime& aTime)
		{
		}
        void always_render(const ScreenSpatialPosition& position);
	private:
		template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
            ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(ComponentRenderableBase);
        }
    };
    
    class ComponentKeyboardController: public ComponentBase
    {
        friend class boost::serialization::access;
		void derived_setup();
	public:
		ComponentKeyboardController(){}
		static std::string name(){return "ComponentKeyboardController";}
        void update(const EngineTime& aTime);
	private:
		template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
            ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(ComponentBase);
        }

    };
}
BOOST_CLASS_EXPORT_KEY(RU::ComponentKeyboardController)
BOOST_CLASS_EXPORT_KEY(RU::ComponentJiggle)
BOOST_CLASS_EXPORT_KEY(RU::ComponentExistence)